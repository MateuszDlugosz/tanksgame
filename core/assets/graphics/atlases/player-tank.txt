player-tank.png
size: 49, 49
format: RGBA8888
filter: Nearest,Nearest
repeat: none
player-down-0
  rotate: false
  xy: 1, 1
  size: 15, 15
  orig: 15, 15
  offset: 0, 0
  index: -1
player-down-1
  rotate: false
  xy: 17, 1
  size: 15, 15
  orig: 15, 15
  offset: 0, 0
  index: -1
player-left-0
  rotate: false
  xy: 33, 1
  size: 15, 15
  orig: 15, 15
  offset: 0, 0
  index: -1
player-left-1
  rotate: false
  xy: 1, 17
  size: 15, 15
  orig: 15, 15
  offset: 0, 0
  index: -1
player-right-0
  rotate: false
  xy: 17, 17
  size: 15, 15
  orig: 15, 15
  offset: 0, 0
  index: -1
player-right-1
  rotate: false
  xy: 33, 17
  size: 15, 15
  orig: 15, 15
  offset: 0, 0
  index: -1
player-up-0
  rotate: false
  xy: 1, 33
  size: 15, 15
  orig: 15, 15
  offset: 0, 0
  index: -1
player-up-1
  rotate: false
  xy: 17, 33
  size: 15, 15
  orig: 15, 15
  offset: 0, 0
  index: -1
