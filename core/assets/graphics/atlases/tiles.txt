tiles.png
size: 28, 19
format: RGBA8888
filter: Nearest,Nearest
repeat: none
bricks
  rotate: false
  xy: 1, 1
  size: 8, 8
  orig: 8, 8
  offset: 0, 0
  index: -1
concrete
  rotate: false
  xy: 10, 1
  size: 8, 8
  orig: 8, 8
  offset: 0, 0
  index: -1
grass
  rotate: false
  xy: 19, 1
  size: 8, 8
  orig: 8, 8
  offset: 0, 0
  index: -1
water-0
  rotate: false
  xy: 1, 10
  size: 8, 8
  orig: 8, 8
  offset: 0, 0
  index: -1
water-1
  rotate: false
  xy: 10, 10
  size: 8, 8
  orig: 8, 8
  offset: 0, 0
  index: -1
water-2
  rotate: false
  xy: 19, 10
  size: 8, 8
  orig: 8, 8
  offset: 0, 0
  index: -1
