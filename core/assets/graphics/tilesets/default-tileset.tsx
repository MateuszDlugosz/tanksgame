<?xml version="1.0" encoding="UTF-8"?>
<tileset name="tileset " tilewidth="8" tileheight="8" spacing="2" margin="1" tilecount="6" columns="6">
 <image source="tileset .png" width="60" height="10"/>
 <tile id="0">
  <properties>
   <property name="type" value="Obstacle"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="type" value="Obstacle"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="type" value="Grass"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="type" value="Boundaries"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="type" value="Ground"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="type" value="Water"/>
  </properties>
 </tile>
</tileset>
