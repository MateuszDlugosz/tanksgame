package tanks.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.google.common.eventbus.EventBus;
import tanks.game.assets.AssetStorage;
import tanks.game.context.Context;
import tanks.game.screens.GameScreen;
import tanks.game.screens.LevelScreen;

import java.util.HashMap;
import java.util.Map;

public class GameApplication extends Game {

	private final Context context;

	private AssetStorage assetStorage;
	private SpriteBatch spriteBatch;
	private ShapeRenderer shapeRenderer;
	private EventBus eventBus;

	private Map<Class<? extends GameScreen>, GameScreen> gameScreens;

	public GameApplication(Context context) {
		this.context = context;
		this.gameScreens = new HashMap<Class<? extends GameScreen>, GameScreen>();
	}

	@Override
	public void create() {
		assetStorage = new AssetStorage(new AssetManager());
		spriteBatch = new SpriteBatch();
		shapeRenderer = new ShapeRenderer();
		eventBus = new EventBus();

		gameScreens.put(LevelScreen.class, new LevelScreen(this, "prototypes/levels/test-level-0.xml"));
		this.setScreen(gameScreens.get(LevelScreen.class));
	}

	public Context getContext() {
		return context;
	}

	public AssetStorage getAssetStorage() {
		return assetStorage;
	}

	public ShapeRenderer getShapeRenderer() {
		return shapeRenderer;
	}

	public SpriteBatch getSpriteBatch() {
		return spriteBatch;
	}

	public EventBus getEventBus() {
		return eventBus;
	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		super.render();
	}

	@Override
	public void dispose() {
		assetStorage.dispose();
		shapeRenderer.dispose();
		spriteBatch.dispose();
	}

}
