package tanks.game.assets;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.utils.Disposable;

public class AssetStorage implements Disposable {

    private final AssetManager assetManager;

    public AssetStorage(AssetManager assetManager) {
        this.assetManager = assetManager;
    }

    public <T> T getAsset(String filename, Class<T> assetClass) {
        if (!assetManager.isLoaded(filename, assetClass)) {
            assetManager.load(filename, assetClass);
            assetManager.finishLoading();
        }

        return assetManager.get(filename, assetClass);
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }

    @Override
    public void dispose() {
        assetManager.dispose();
    }

}
