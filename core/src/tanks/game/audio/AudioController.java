package tanks.game.audio;

import com.google.common.eventbus.Subscribe;
import tanks.game.audio.events.*;
import tanks.game.audio.musics.MusicPlayer;
import tanks.game.audio.sounds.SoundPlayer;

public class AudioController {

    private final SoundPlayer soundPlayer;
    private final MusicPlayer musicPlayer;
    private final AudioOptions audioOptions;

    public AudioController(SoundPlayer soundPlayer,
                           MusicPlayer musicPlayer,
                           AudioOptions audioOptions)
    {
        this.soundPlayer = soundPlayer;
        this.musicPlayer = musicPlayer;
        this.audioOptions = audioOptions;
    }

    @Subscribe
    public void handlePlaySoundEvent(PlaySoundEvent event) {
        soundPlayer.playSound(event.getSound(), audioOptions);
    }

    @Subscribe
    public void handlePlayMusicEvent(PlayMusicEvent event) {
        musicPlayer.playMusic(event.getMusic(), audioOptions);
    }

    @Subscribe
    public void handlePauseMusicEvent(PauseMusicEvent event) {
        musicPlayer.pauseMusic();
    }

    @Subscribe
    public void handleResumeMusicEvent(ResumeMusicEvent event) {
        musicPlayer.resumeMusic();
    }

    @Subscribe
    public void handleStopMusicEvent(StopMusicEvent event) {
        musicPlayer.stopMusic();
    }



}
