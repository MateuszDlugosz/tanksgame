package tanks.game.audio;

import com.badlogic.gdx.math.MathUtils;

import java.util.Map;

public class AudioOptions {

    public enum Option {

        Volume(0, 100),
        Pan(-100, 100);

        private final int minValue;
        private final int maxValue;

        Option(int minValue, int maxValue) {
            this.minValue = minValue;
            this.maxValue = maxValue;
        }

        public int getMinValue() {
            return minValue;
        }

        public int getMaxValue() {
            return maxValue;
        }

    }

    private final Map<Option, Integer> options;

    public AudioOptions(Map<Option, Integer> options) {
        this.options = options;
    }

    public float getOptionValue(Option option) {
        return options.get(option) * 0.01f;
    }

    public void setOptionValue(Option option, int value) {
        options.put(option, MathUtils.clamp(value, option.getMinValue(), option.getMaxValue()));
    }

}
