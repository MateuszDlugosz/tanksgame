package tanks.game.audio.events;

import com.badlogic.gdx.audio.Music;

public class PlayMusicEvent {

    private final Music music;

    public PlayMusicEvent(Music music) {
        this.music = music;
    }

    public Music getMusic() {
        return music;
    }

}
