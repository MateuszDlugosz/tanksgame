package tanks.game.audio.events;

import com.badlogic.gdx.audio.Sound;

public class PlaySoundEvent {

    private final Sound sound;

    public PlaySoundEvent(Sound sound) {
        this.sound = sound;
    }

    public Sound getSound() {
        return sound;
    }

}
