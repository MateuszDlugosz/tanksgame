package tanks.game.audio.musics;

import com.badlogic.gdx.audio.Music;
import tanks.game.audio.AudioOptions;

public class MusicPlayer {

    private Music music;

    public void playMusic(Music music, AudioOptions audioOptions) {
        stopMusic();
        music.play();
        changeOptions(audioOptions);

        this.music = music;
    }

    public void stopMusic() {
        if (isPlaying())
            music.stop();
    }

    public void pauseMusic() {
        if (isPlaying())
            music.pause();
    }

    public void resumeMusic() {
        if (music != null && !music.isPlaying())
            music.play();
    }

    public void changeOptions(AudioOptions audioOptions) {
        if (isPlaying()) {
            music.setPan(
                    audioOptions.getOptionValue(AudioOptions.Option.Pan),
                    audioOptions.getOptionValue(AudioOptions.Option.Volume)
            );
        }
    }

    public boolean isPlaying() {
        return music != null && music.isPlaying() ? true : false;
    }

}
