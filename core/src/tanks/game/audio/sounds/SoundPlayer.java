package tanks.game.audio.sounds;

import com.badlogic.gdx.audio.Sound;
import tanks.game.audio.AudioOptions;

public class SoundPlayer {

    public void playSound(Sound sound, AudioOptions audioOptions) {
        sound.play(audioOptions.getOptionValue(AudioOptions.Option.Volume));
    }

}
