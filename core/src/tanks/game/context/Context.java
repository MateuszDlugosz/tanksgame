package tanks.game.context;

import java.util.Map;

public class Context {

    private final Map<Class<?>, Provider<?>> providers;

    public Context(Map<Class<?>, Provider<?>> providers) {
        this.providers = providers;
    }

    public <T> T getObject(Class<T> objectClass) {
        if (!providers.containsKey(objectClass)) {
            throw new ProviderNotFoundException(String.format("Provider of %s not found.", objectClass));
        }

        return (T) providers.get(objectClass).provide(this);
    }

}
