package tanks.game.context;

import org.reflections.Reflections;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ContextInitializer {

    private Reflections reflections;
    private ProviderMethodValidator providerMethodValidator;

    public ContextInitializer(Class<?> mainClass) {
        reflections = new Reflections(mainClass.getPackage().getName());
        providerMethodValidator = new ProviderMethodValidator();
    }

    public Context initContext() {
        return new Context(initProviders(reflections.getTypesAnnotatedWith(ProviderClass.class)));
    }

    private Map<Class<?>, Provider<?>> initProviders(Set<Class<?>> providersClasses) {
        Map<Class<?>, Provider<?>> providers = new HashMap<Class<?>, Provider<?>>();

        for (Class<?> providerClass : providersClasses) {
            try {
                Object providerObject = providerClass.newInstance();

                for (Method providerMethod : providerClass.getMethods()) {
                    if (providerMethod.getAnnotation(ProviderMethod.class) == null) continue;

                    Provider<?> provider = createProvider(providerObject, providerMethod);
                    providers.put(provider.provides(), provider);
                }
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return providers;
    }

    private Provider<?> createProvider(Object object, Method providerMethod) {
        Provider<?> provider = null;

        if (providerMethodValidator.isValid(providerMethod)) {
            Scope scope = providerMethod.getAnnotation(ProviderMethod.class).scope();

            if (scope == Scope.Singleton)
                provider = createSingletonProvider(object, providerMethod, providerMethod.getReturnType());

            if (scope == Scope.Prototype)
                provider = createPrototypeProvider(object, providerMethod, providerMethod.getReturnType());
        }

        return provider;
    }

    private <T> Provider<T> createSingletonProvider(Object object, Method method, Class<T> provides) {
        return new SingletonProvider<T>(object, method, provides);
    }

    private <T> Provider<T> createPrototypeProvider(Object object, Method method, Class<T> provides) {
        return new PrototypeProvider<T>(object, method, provides);
    }

}
