package tanks.game.context;

public class ObjectProvideException extends RuntimeException {

    public ObjectProvideException(String message) {
        super(message);
    }

}
