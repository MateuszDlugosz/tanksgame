package tanks.game.context;

public interface Provider<T> {

    Class<? extends T> provides();
    T provide(Context context);

}
