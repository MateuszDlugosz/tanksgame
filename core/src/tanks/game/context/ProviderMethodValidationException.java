package tanks.game.context;

public class ProviderMethodValidationException extends RuntimeException {

    public ProviderMethodValidationException(String message) {
        super(message);
    }

}
