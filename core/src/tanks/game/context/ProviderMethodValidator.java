package tanks.game.context;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class ProviderMethodValidator {

    public boolean isValid(Method method) {
        try {
            checkAnnotation(method);
            checkParameters(method);
            checkModifier(method);
            checkReturnType(method);
        } catch (Exception exception) {
            throw new ProviderMethodValidationException(
                    String.format(
                            "Method %s is not valid provider method. %s",
                            method.getName(),
                            exception.getMessage()
                    )
            );
        }

        return true;
    }

    private void checkAnnotation(Method method) throws Exception {
        if (method.getAnnotation(ProviderMethod.class) == null)
            throw new Exception(String.format("Annotation %s not found", ProviderMethod.class));
    }

    private void checkParameters(Method method) throws Exception {
        if (method.getParameterTypes().length != 1)
            throw new Exception(String.format("Provider method must have only one parameter."));

        if (!method.getParameterTypes()[0].equals(Context.class))
            throw new Exception(String.format("Provider method parameter must be Context.class."));
    }

    private void checkModifier(Method method) throws Exception {
        if (!Modifier.isPublic(method.getModifiers()))
            throw new Exception("Provider method must be public.");
    }

    private void checkReturnType(Method method) throws Exception {
        if (method.getReturnType().equals(Void.class))
            throw new Exception("Provider method must return Object type.");
    }

}
