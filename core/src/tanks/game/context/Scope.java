package tanks.game.context;

public enum Scope {

    Singleton,
    Prototype

}
