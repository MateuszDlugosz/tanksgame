package tanks.game.context;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class SingletonProvider<T> implements Provider<T> {

    private final Object providerObject;
    private final Method providerMethod;
    private final Class<? extends T> provides;
    private T object;

    public SingletonProvider(Object providerObject, Method providerMethod, Class<? extends T> provides) {
        this.providerObject = providerObject;
        this.providerMethod = providerMethod;
        this.provides = provides;
    }

    @Override
    public Class<? extends T> provides() {
        return provides;
    }

    @Override
    public T provide(Context context) {
        if (object == null) {
            try {
                object = (T) providerMethod.invoke(providerObject, context);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        return object;
    }

}
