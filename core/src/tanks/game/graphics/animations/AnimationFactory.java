package tanks.game.graphics.animations;

import com.badlogic.gdx.graphics.g2d.Animation;
import tanks.game.assets.AssetStorage;
import tanks.game.utils.scales.Scale;

import java.util.HashMap;
import java.util.Map;

public class AnimationFactory {

    private final Map<Class<? extends AnimationPrototype>, ConcreteAnimationFactory> innerFactories;

    public AnimationFactory(ConcreteAnimationFactory[] factories) {
        innerFactories = new HashMap<Class<? extends AnimationPrototype>, ConcreteAnimationFactory>();

        for (ConcreteAnimationFactory factory : factories) {
            RegistrableAnimationFactory annotation = factory.getClass().getAnnotation(RegistrableAnimationFactory.class);
            if (annotation == null) {
                throw new AnnotationNotFoundException(String.format("Annotation %s not found in class %s.",
                        RegistrableAnimationFactory.class, factory.getClass()));
            }

            innerFactories.put(annotation.registerAs(), factory);
        }
    }

    public Animation createAnimation(AnimationPrototype prototype, AssetStorage assetStorage, Scale scale) {
        Class<? extends AnimationPrototype> type = prototype.getClass();

        if (innerFactories.containsKey(type)) {
            return innerFactories.get(type).createAnimation(prototype, assetStorage, scale);
        }
        else {
            throw new FactoryNotFoundException(String.format("Factory of type %s not found.", type));
        }
    }

}
