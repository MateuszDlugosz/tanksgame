package tanks.game.graphics.animations;

import com.badlogic.gdx.utils.XmlReader;

import java.util.HashMap;
import java.util.Map;

public class AnimationPrototypeXmlLoader {

    public static final String ANIMATIONS_ELEMENT = "animations";
    public static final String ANIMATION_ELEMENT = "animation";
    public static final String TYPE_ATTRIBUTE = "type";

    private final Map<String, ConcreteAnimationPrototypeXmlLoader> innerLoaders;

    public AnimationPrototypeXmlLoader(ConcreteAnimationPrototypeXmlLoader[] loaders) {
        innerLoaders = new HashMap<String, ConcreteAnimationPrototypeXmlLoader>();

        for (ConcreteAnimationPrototypeXmlLoader loader : loaders) {
            RegistrableAnimationPrototypeXmlLoader annotation
                    = loader.getClass().getAnnotation(RegistrableAnimationPrototypeXmlLoader.class);

            if (annotation == null) {
                throw new AnnotationNotFoundException(String.format("Annotation %s not found in class %s.",
                        RegistrableAnimationPrototypeXmlLoader.class, loader.getClass()));
            }

            innerLoaders.put(annotation.registerAs(), loader);
        }
    }

    public AnimationPrototype loadAnimationPrototype(XmlReader.Element element) {
        String type = element.getAttribute(TYPE_ATTRIBUTE);

        if (innerLoaders.containsKey(type)) {
            return innerLoaders.get(type).loadAnimationPrototype(element);
        }
        else {
            throw new LoaderNotFoundException(String.format("Prototype loader of type %s not found.", type));
        }
    }

}
