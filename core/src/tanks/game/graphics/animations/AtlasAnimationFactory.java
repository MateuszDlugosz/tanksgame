package tanks.game.graphics.animations;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Array;
import tanks.game.assets.AssetStorage;
import tanks.game.utils.scales.Scale;

import java.util.List;

@RegistrableAnimationFactory(registerAs = AtlasAnimationPrototype.class)
public class AtlasAnimationFactory implements ConcreteAnimationFactory<AtlasAnimationPrototype> {

    @Override
    public Animation createAnimation(AtlasAnimationPrototype prototype, AssetStorage assetStorage, Scale scale) {
        return new Animation(
                prototype.getFrameDuration(),
                createFrames(prototype.getAtlasFilename(), prototype.getRegionNames(), assetStorage, scale),
                prototype.getPlayMode()
        );
    }

    private Array<Sprite> createFrames(String atlasFilename, List<String> regionNames, AssetStorage assetStorage, Scale scale) {
        Array<Sprite> frames = new Array<Sprite>();
        TextureAtlas atlas = assetStorage.getAsset(atlasFilename, TextureAtlas.class);

        for (String regionName : regionNames) {
            Sprite sprite = atlas.createSprite(regionName);
            sprite.setSize(
                    scale.scaleValue(sprite.getWidth()),
                    scale.scaleValue(sprite.getHeight())
            );

            frames.add(sprite);
        }

        return frames;
    }

}
