package tanks.game.graphics.animations;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.google.common.base.MoreObjects;

import java.util.List;

public class AtlasAnimationPrototype implements AnimationPrototype {

    private String atlasFilename;
    private List<String> regionNames;
    private float frameDuration;
    private Animation.PlayMode playMode;

    public String getAtlasFilename() {
        return atlasFilename;
    }

    public void setAtlasFilename(String atlasFilename) {
        this.atlasFilename = atlasFilename;
    }

    public List<String> getRegionNames() {
        return regionNames;
    }

    public void setRegionNames(List<String> regionNames) {
        this.regionNames = regionNames;
    }

    public float getFrameDuration() {
        return frameDuration;
    }

    public void setFrameDuration(float frameDuration) {
        this.frameDuration = frameDuration;
    }

    public Animation.PlayMode getPlayMode() {
        return playMode;
    }

    public void setPlayMode(Animation.PlayMode playMode) {
        this.playMode = playMode;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("atlasFilename", atlasFilename)
                .add("regionNames", regionNames)
                .add("frameDuration", frameDuration)
                .add("playMode", playMode)
                .toString();
    }

}
