package tanks.game.graphics.animations;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

import java.util.ArrayList;
import java.util.List;

@RegistrableAnimationPrototypeXmlLoader(registerAs = "AtlasAnimation")
public class AtlasAnimationPrototypeXmlLoader implements ConcreteAnimationPrototypeXmlLoader<AtlasAnimationPrototype> {

    public static final String FRAME_ELEMENT = "frame";
    public static final String ATLAS_FILENAME_ATTRIBUTE = "atlasFilename";
    public static final String REGION_NAME_ATTRIBUTE = "regionName";
    public static final String FRAME_DURATION_ATTRIBUTE = "frameDuration";
    public static final String PLAY_MODE_ATTRIBUTE = "playMode";

    @Override
    public AtlasAnimationPrototype loadAnimationPrototype(XmlReader.Element element) {
        AtlasAnimationPrototype prototype = new AtlasAnimationPrototype();
        prototype.setAtlasFilename(element.getAttribute(ATLAS_FILENAME_ATTRIBUTE).trim());
        prototype.setFrameDuration(Float.valueOf(element.getAttribute(FRAME_DURATION_ATTRIBUTE)));
        prototype.setPlayMode(Animation.PlayMode.valueOf(element.getAttribute(PLAY_MODE_ATTRIBUTE)));
        prototype.setRegionNames(loadRegionNames(element.getChildrenByName(FRAME_ELEMENT)));

        return prototype;
    }

    private List<String> loadRegionNames(Array<XmlReader.Element> elements) {
        List<String> regionNames = new ArrayList<String>();

        for (XmlReader.Element frame : elements) {
            regionNames.add(frame.getAttribute(REGION_NAME_ATTRIBUTE));
        }

        return regionNames;
    }

}
