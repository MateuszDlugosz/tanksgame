package tanks.game.graphics.animations;

import com.badlogic.gdx.graphics.g2d.Animation;
import tanks.game.assets.AssetStorage;
import tanks.game.utils.scales.Scale;

public interface ConcreteAnimationFactory<T extends AnimationPrototype> {

    Animation createAnimation(T prototype, AssetStorage assetStorage, Scale scale);

}
