package tanks.game.graphics.animations;

import com.badlogic.gdx.utils.XmlReader;

public interface ConcreteAnimationPrototypeXmlLoader<T extends AnimationPrototype> {

    T loadAnimationPrototype(XmlReader.Element element);

}
