package tanks.game.graphics.animations;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.utils.Array;
import tanks.game.assets.AssetStorage;
import tanks.game.utils.scales.Scale;

import java.util.List;

@RegistrableAnimationFactory(registerAs = FileAnimationPrototype.class)
public class FileAnimationFactory implements ConcreteAnimationFactory<FileAnimationPrototype> {

    @Override
    public Animation createAnimation(FileAnimationPrototype prototype, AssetStorage assetStorage, Scale scale) {
        return new Animation(
                prototype.getFrameDuration(),
                createAnimationFrames(prototype.getFilenames(), assetStorage, scale),
                prototype.getPlayMode()
        );
    }

    private Array<Sprite> createAnimationFrames(List<String> filenames, AssetStorage assetStorage, Scale scale) {
        Array<Sprite> textures = new Array<Sprite>();

        for (String filename : filenames) {
            Sprite sprite = new Sprite(assetStorage.getAsset(filename, Texture.class));
            sprite.setSize(
                    scale.scaleValue(sprite.getWidth()),
                    scale.scaleValue(sprite.getHeight())
            );
            textures.add(sprite);
        }

        return textures;
    }

}
