package tanks.game.graphics.animations;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.google.common.base.MoreObjects;

import java.util.List;

public class FileAnimationPrototype implements AnimationPrototype {

    private List<String> filenames;
    private float frameDuration;
    private Animation.PlayMode playMode;

    public List<String> getFilenames() {
        return filenames;
    }

    public void setFilenames(List<String> filenames) {
        this.filenames = filenames;
    }

    public float getFrameDuration() {
        return frameDuration;
    }

    public void setFrameDuration(float frameDuration) {
        this.frameDuration = frameDuration;
    }

    public Animation.PlayMode getPlayMode() {
        return playMode;
    }

    public void setPlayMode(Animation.PlayMode playMode) {
        this.playMode = playMode;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("filenames", filenames)
                .add("frameDuration", frameDuration)
                .add("playMode", playMode)
                .toString();
    }

}
