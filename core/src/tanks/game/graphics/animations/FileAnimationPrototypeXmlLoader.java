package tanks.game.graphics.animations;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

import java.util.ArrayList;
import java.util.List;

@RegistrableAnimationPrototypeXmlLoader(registerAs = "FileAnimation")
public class FileAnimationPrototypeXmlLoader implements ConcreteAnimationPrototypeXmlLoader<FileAnimationPrototype> {

    public static final String FRAME_ELEMENT = "frame";
    public static final String FILENAME_ATTRIBUTE = "filename";
    public static final String FRAME_DURATION_ATTRIBUTE = "frameDuration";
    public static final String PLAY_MODE_ATTRIBUTE = "playMode";

    @Override
    public FileAnimationPrototype loadAnimationPrototype(XmlReader.Element element) {
        FileAnimationPrototype prototype = new FileAnimationPrototype();
        prototype.setFrameDuration(Float.valueOf(element.getAttribute(FRAME_DURATION_ATTRIBUTE)));
        prototype.setPlayMode(Animation.PlayMode.valueOf(element.getAttribute(PLAY_MODE_ATTRIBUTE)));
        prototype.setFilenames(loadFilenames(element.getChildrenByName(FRAME_ELEMENT)));

        return prototype;
    }

    private List<String> loadFilenames(Array<XmlReader.Element> elements) {
        List<String> filenames = new ArrayList<String>();

        for (XmlReader.Element frame : elements) {
            filenames.add(frame.getAttribute(FILENAME_ATTRIBUTE).trim());
        }

        return filenames;
    }

}
