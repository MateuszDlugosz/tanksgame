package tanks.game.graphics.animations;

public class LoaderNotFoundException extends RuntimeException {

    public LoaderNotFoundException(String message) {
        super(message);
    }

}
