package tanks.game.graphics.cameras;

import com.badlogic.gdx.graphics.Camera;
import tanks.game.utils.scales.Scale;

public interface CameraFactory<T extends Camera> {

    T createCamera(float width, float height, boolean yDown, Scale scale);

}
