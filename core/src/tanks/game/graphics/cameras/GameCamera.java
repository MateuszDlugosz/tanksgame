package tanks.game.graphics.cameras;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.viewport.Viewport;
import tanks.game.utils.boundaries.Boundaries;
import tanks.game.utils.positions.Position;

public class GameCamera {

    private Boundaries boundaries;
    private Viewport viewport;
    private Camera camera;

    public GameCamera(Viewport viewport, Camera camera) {
        this.viewport = viewport;
        this.camera = camera;
    }

    public GameCamera(Boundaries boundaries, Viewport viewport, OrthographicCamera orthographicCamera) {
        this.boundaries = boundaries;
        this.viewport = viewport;
        this.camera = camera;
    }

    public void setBoundaries(Boundaries boundaries) {
        this.boundaries = boundaries;
    }

    public Boundaries getBoundaries() {
        return boundaries;
    }

    public Camera getCamera() {
        return camera;
    }

    public Viewport getViewport() {
        return viewport;
    }

    public void update(float delta) {
        camera.update();
    }

    public Position getPosition() {
        return new Position(
                camera.position.x,
                camera.position.y
        );
    }

    public void setPosition(float x, float y) {
        if (boundaries == null) {
            camera.position.set(x, y, 0);
        }
        else {
            float posX = MathUtils.clamp(x, boundaries.getMinX(), boundaries.getMaxX());
            float posY = MathUtils.clamp(y, boundaries.getMinY(), boundaries.getMaxY());

            camera.position.set(posX, posY, 0);
        }
    }

}
