package tanks.game.graphics.cameras;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.utils.viewport.Viewport;
import tanks.game.graphics.viewports.ViewportFactory;
import tanks.game.utils.scales.Scale;

public class GameCameraFactory {

    private final CameraFactory cameraFactory;
    private final ViewportFactory viewportFactory;

    public GameCameraFactory(CameraFactory cameraFactory, ViewportFactory viewportFactory) {
        this.cameraFactory = cameraFactory;
        this.viewportFactory = viewportFactory;
    }

    public GameCamera createGameCamera(float width, float height, boolean yDown, Scale scale) {
        Camera camera = cameraFactory.createCamera(width, height, yDown, scale);
        Viewport viewport = viewportFactory.createViewport(width, height, scale, camera);

        return new GameCamera(viewport, camera);
    }

}
