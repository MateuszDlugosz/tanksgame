package tanks.game.graphics.cameras;

import com.badlogic.gdx.graphics.OrthographicCamera;
import tanks.game.utils.scales.Scale;

public class OrthographicCameraFactory implements CameraFactory<OrthographicCamera> {

    @Override
    public OrthographicCamera createCamera(float width, float height, boolean yDown, Scale scale) {
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(
                yDown,
                scale.scaleValue(width),
                scale.scaleValue(height)
        );
        camera.position.set(0, 0, 0);

        return camera;
    }
}
