package tanks.game.graphics.colors;

import com.badlogic.gdx.graphics.Color;

import java.util.HashMap;
import java.util.Map;

public class ColorFactory {

    private final Map<Class<? extends ColorPrototype>, ConcreteColorFactory> innerFactories;

    public ColorFactory(ConcreteColorFactory[] factories) {
        innerFactories = new HashMap<Class<? extends ColorPrototype>, ConcreteColorFactory>();

        for (ConcreteColorFactory factory : factories) {
            RegistrableColorFactory annotation = factory.getClass().getAnnotation(RegistrableColorFactory.class);
            if (annotation == null) {
                throw new AnnotationNotFoundException(String.format("Annotation %s not found in class %s.",
                        RegistrableColorFactory.class, factory.getClass()));
            }

            innerFactories.put(annotation.registerAs(), factory);
        }
    }

    public Color createColor(ColorPrototype prototype) {
        Class<? extends ColorPrototype> type = prototype.getClass();

        if (innerFactories.containsKey(type)) {
            return innerFactories.get(type).createColor(prototype);
        }
        else {
            throw new FactoryNotFoundException(String.format("Factory of type %s not found.", type));
        }
    }

}
