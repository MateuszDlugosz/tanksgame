package tanks.game.graphics.colors;

import com.badlogic.gdx.utils.XmlReader;

import java.util.HashMap;
import java.util.Map;

public class ColorPrototypeXmlLoader {

    public static final String COLORS_ELEMENT = "colors";
    public static final String COLOR_ELEMENT = "color";
    public static final String TYPE_ATTRIBUTE = "type";

    private final Map<String, ConcreteColorPrototypeXmlLoader> innerLoaders;

    public ColorPrototypeXmlLoader(ConcreteColorPrototypeXmlLoader[] loaders) {
        innerLoaders = new HashMap<String, ConcreteColorPrototypeXmlLoader>();

        for (ConcreteColorPrototypeXmlLoader loader : loaders) {
            RegistrableColorPrototypeXmlLoader annotation
                    = loader.getClass().getAnnotation(RegistrableColorPrototypeXmlLoader.class);

            if (annotation == null) {
                throw new AnnotationNotFoundException(String.format("Annotation %s not found in class %s.",
                        RegistrableColorPrototypeXmlLoader.class, loader.getClass()));
            }

            innerLoaders.put(annotation.registerAs(), loader);
        }
    }

    public ColorPrototype loadColorPrototype(XmlReader.Element element) {
        String type = element.getAttribute(TYPE_ATTRIBUTE);

        if (innerLoaders.containsKey(type)) {
            return innerLoaders.get(type).loadColorPrototype(element);
        }
        else {
            throw new LoaderNotFoundException(String.format("Prototype loader of type %s not found.", type));
        }
    }

}
