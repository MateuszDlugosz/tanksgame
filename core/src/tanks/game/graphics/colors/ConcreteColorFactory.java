package tanks.game.graphics.colors;

import com.badlogic.gdx.graphics.Color;

public interface ConcreteColorFactory<T extends ColorPrototype> {

    Color createColor(T prototype);

}
