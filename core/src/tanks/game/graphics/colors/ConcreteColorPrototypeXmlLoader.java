package tanks.game.graphics.colors;

import com.badlogic.gdx.utils.XmlReader;

public interface ConcreteColorPrototypeXmlLoader<T extends ColorPrototype> {

    T loadColorPrototype(XmlReader.Element element);

}
