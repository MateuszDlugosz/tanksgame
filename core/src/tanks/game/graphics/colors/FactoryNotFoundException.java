package tanks.game.graphics.colors;

public class FactoryNotFoundException extends RuntimeException {

    public FactoryNotFoundException(String message) {
        super(message);
    }

}
