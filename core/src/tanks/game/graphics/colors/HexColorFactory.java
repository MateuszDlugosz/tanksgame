package tanks.game.graphics.colors;

import com.badlogic.gdx.graphics.Color;

@RegistrableColorFactory(registerAs = HexColorPrototype.class)
public class HexColorFactory implements ConcreteColorFactory<HexColorPrototype> {

    @Override
    public Color createColor(HexColorPrototype prototype) {
        return Color.valueOf(prototype.getHexValue());
    }

}
