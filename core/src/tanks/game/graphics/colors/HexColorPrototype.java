package tanks.game.graphics.colors;

import com.google.common.base.MoreObjects;

public class HexColorPrototype implements ColorPrototype {

    private String hexValue;

    public String getHexValue() {
        return hexValue;
    }

    public void setHexValue(String hexValue) {
        this.hexValue = hexValue;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("hexValue", hexValue)
                .toString();
    }

}
