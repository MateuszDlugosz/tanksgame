package tanks.game.graphics.colors;

import com.badlogic.gdx.utils.XmlReader;

@RegistrableColorPrototypeXmlLoader(registerAs = "HexColor")
public class HexColorPrototypeXmlLoader implements ConcreteColorPrototypeXmlLoader<HexColorPrototype> {

    public static final String HEX_VALUE_ELEMENT = "hexValue";

    @Override
    public HexColorPrototype loadColorPrototype(XmlReader.Element element) {
        HexColorPrototype prototype = new HexColorPrototype();
        prototype.setHexValue(element.getChildByName(HEX_VALUE_ELEMENT).getText().trim());

        return prototype;
    }

}
