package tanks.game.graphics.colors;

public class LoaderNotFoundException extends RuntimeException {

    public LoaderNotFoundException(String message) {
        super(message);
    }

}
