package tanks.game.graphics.colors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;

@RegistrableColorFactory(registerAs = RgbColorPrototype.class)
public class RgbColorFactory implements ConcreteColorFactory<RgbColorPrototype> {

    public static final float MIN_COLOR_VALUE = 0f;
    public static final float MAX_COLOR_VALUE = 255f;
    public static final float DEFAULT_ALPHA_VALUE = 1f;

    @Override
    public Color createColor(RgbColorPrototype prototype) {
        return new Color(
                prepareColorValue(prototype.getR()),
                prepareColorValue(prototype.getG()),
                prepareColorValue(prototype.getB()),
                DEFAULT_ALPHA_VALUE
        );
    }

    private float prepareColorValue(float value) {
        return clampColorValue(value) / MAX_COLOR_VALUE;
    }

    private float clampColorValue(float value) {
        return MathUtils.clamp(value, MIN_COLOR_VALUE, MAX_COLOR_VALUE);
    }

}
