package tanks.game.graphics.colors;

import com.badlogic.gdx.utils.XmlReader;

@RegistrableColorPrototypeXmlLoader(registerAs = "RgbColor")
public class RgbColorPrototypeXmlLoader implements ConcreteColorPrototypeXmlLoader<RgbColorPrototype> {

    public static final String R_ELEMENT = "r";
    public static final String G_ELEMENT = "g";
    public static final String B_ELEMENT = "b";

    @Override
    public RgbColorPrototype loadColorPrototype(XmlReader.Element element) {
        RgbColorPrototype prototype = new RgbColorPrototype();
        prototype.setR(loadColorValue(element.getChildByName(R_ELEMENT)));
        prototype.setG(loadColorValue(element.getChildByName(G_ELEMENT)));
        prototype.setB(loadColorValue(element.getChildByName(B_ELEMENT)));

        return prototype;
    }

    private float loadColorValue(XmlReader.Element element) {
        return Float.valueOf(element.getText().trim());
    }

}
