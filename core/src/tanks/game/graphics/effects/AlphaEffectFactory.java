package tanks.game.graphics.effects;

import com.badlogic.gdx.math.MathUtils;

@RegistrableEffectFactory(registerAs = AlphaEffectPrototype.class)
public class AlphaEffectFactory implements ConcreteEffectFactory<AlphaEffect, AlphaEffectPrototype> {

    public static final float MIN_ALPHA_VALUE = 0;
    public static final float MAX_ALPHA_VALUE = 255f;

    @Override
    public AlphaEffect createEffect(AlphaEffectPrototype prototype) {
        return new AlphaEffect(
                MathUtils.clamp(
                        prototype.getAlphaValue(),
                        MIN_ALPHA_VALUE,
                        MAX_ALPHA_VALUE
                ) / MAX_ALPHA_VALUE
        );
    }

}
