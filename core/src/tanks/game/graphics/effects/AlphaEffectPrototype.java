package tanks.game.graphics.effects;

import com.google.common.base.MoreObjects;

public class AlphaEffectPrototype implements EffectPrototype {

    private float alphaValue;

    public float getAlphaValue() {
        return alphaValue;
    }

    public void setAlphaValue(float alphaValue) {
        this.alphaValue = alphaValue;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("alphaValue", alphaValue)
                .toString();
    }

}
