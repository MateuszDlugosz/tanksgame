package tanks.game.graphics.effects;

import com.badlogic.gdx.utils.XmlReader;

@RegistrableEffectPrototypeXmlLoader(registerAs = "AlphaEffect")
public class AlphaEffectPrototypeXmlLoader implements ConcreteEffectPrototypeXmlLoader<AlphaEffectPrototype> {

    public static final String ALPHA_VALUE_ELEMENT = "alphaValue";

    @Override
    public AlphaEffectPrototype loadEffectPrototype(XmlReader.Element element) {
        AlphaEffectPrototype prototype = new AlphaEffectPrototype();
        prototype.setAlphaValue(Float.valueOf(element.getChildByName(ALPHA_VALUE_ELEMENT).getText()));

        return prototype;
    }

}
