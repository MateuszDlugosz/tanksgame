package tanks.game.graphics.effects;

public interface ConcreteEffectFactory<T extends Effect, U extends EffectPrototype> {

    T createEffect(U prototype);

}
