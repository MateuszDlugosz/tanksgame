package tanks.game.graphics.effects;

import com.badlogic.gdx.utils.XmlReader;

public interface ConcreteEffectPrototypeXmlLoader<T extends EffectPrototype> {

    T loadEffectPrototype(XmlReader.Element element);

}
