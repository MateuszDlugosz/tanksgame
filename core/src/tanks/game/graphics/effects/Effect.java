package tanks.game.graphics.effects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface Effect {

    void begin(SpriteBatch spriteBatch);
    void end(SpriteBatch spriteBatch);

}
