package tanks.game.graphics.effects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EffectFactory {

    private final Map<Class<? extends EffectPrototype>, ConcreteEffectFactory> innerFactories;

    public EffectFactory(ConcreteEffectFactory[] factories) {
        innerFactories = new HashMap<Class<? extends EffectPrototype>, ConcreteEffectFactory>();

        for (ConcreteEffectFactory factory : factories) {
            RegistrableEffectFactory annotation = factory.getClass().getAnnotation(RegistrableEffectFactory.class);
            if (annotation == null) {
                throw new AnnotationNotFoundException(String.format("Annotation %s not found in class %s.",
                        RegistrableEffectFactory.class, factory.getClass()));
            }

            innerFactories.put(annotation.registerAs(), factory);
        }
    }

    public Effect createEffect(EffectPrototype prototype) {
        Class<? extends EffectPrototype> type = prototype.getClass();

        if (innerFactories.containsKey(type)) {
            return innerFactories.get(type).createEffect(prototype);
        }
        else {
            throw new FactoryNotFoundException(String.format("Factory of type %s not found.", type));
        }
    }

    public EffectList createEffectList(List<EffectPrototype> prototypes) {
        List<Effect> effects = new ArrayList<Effect>();

        for (EffectPrototype prototype : prototypes) {
            effects.add(createEffect(prototype));
        }

        return new EffectList(effects);
    }

}
