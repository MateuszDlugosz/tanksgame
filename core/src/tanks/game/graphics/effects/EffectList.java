package tanks.game.graphics.effects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Iterator;
import java.util.List;

public class EffectList {

    private final List<Effect> effects;

    public EffectList(List<Effect> effects) {
        this.effects = effects;
    }

    public void updateAll(float delta) {
        Iterator<Effect> iterator = effects.iterator();

        while (iterator.hasNext()) {
            Effect effect = iterator.next();

            if (effect instanceof UpdatableEffect) {
                ((UpdatableEffect) effect).update(delta);
            }

            if (effect instanceof TerminableEffect) {
                if (((TerminableEffect) effect).isTerminated()) {
                    iterator.remove();
                }
            }
        }
    }

    public void resetAll() {
        for (Effect effect : effects) {
            if (effect instanceof ResettableEffect) {
                ((ResettableEffect) effect).reset();
            }
        }
    }

    public void beginAll(SpriteBatch spriteBatch) {
        for (Effect effect : effects) {
            effect.begin(spriteBatch);
        }
    }

    public void endAll(SpriteBatch spriteBatch) {
        for (Effect effect : effects) {
            effect.end(spriteBatch);
        }
    }

    public void addEffect(Effect effect) {
        effects.add(effect);
    }

    public void addAll(List<Effect> effects) {
        for (Effect effect : effects) {
            addEffect(effect);
        }
    }

}
