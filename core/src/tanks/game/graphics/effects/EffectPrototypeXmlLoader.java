package tanks.game.graphics.effects;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EffectPrototypeXmlLoader {

    public static final String EFFECT_ELEMENT = "effect";
    public static final String EFFECTS_ELEMENT = "effects";
    public static final String TYPE_ATTRIBUTE = "type";

    private final Map<String, ConcreteEffectPrototypeXmlLoader> innerLoaders;

    public EffectPrototypeXmlLoader(ConcreteEffectPrototypeXmlLoader[] loaders) {
        innerLoaders = new HashMap<String, ConcreteEffectPrototypeXmlLoader>();

        for (ConcreteEffectPrototypeXmlLoader loader : loaders) {
            RegistrableEffectPrototypeXmlLoader annotation
                    = loader.getClass().getAnnotation(RegistrableEffectPrototypeXmlLoader.class);

            if (annotation == null) {
                throw new AnnotationNotFoundException(String.format("Annotation %s not found in class %s.",
                        RegistrableEffectPrototypeXmlLoader.class, loader.getClass()));
            }

            innerLoaders.put(annotation.registerAs(), loader);
        }
    }

    public EffectPrototype loadEffectPrototype(XmlReader.Element element) {
        String type = element.getAttribute(TYPE_ATTRIBUTE);

        if (innerLoaders.containsKey(type)) {
            return innerLoaders.get(type).loadEffectPrototype(element);
        }
        else {
            throw new LoaderNotFoundException(String.format("Prototype loader of type %s not found.", type));
        }
    }

    public List<EffectPrototype> loadEffectPrototypes(Array<XmlReader.Element> elements) {
        List<EffectPrototype> prototypes = new ArrayList<EffectPrototype>();

        for (XmlReader.Element element : elements) {
            prototypes.add(loadEffectPrototype(element));
        }

        return prototypes;
    }

}
