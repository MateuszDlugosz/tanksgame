package tanks.game.graphics.effects;

public interface ResettableEffect {

    void reset();

}
