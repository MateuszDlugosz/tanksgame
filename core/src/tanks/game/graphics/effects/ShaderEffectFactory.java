package tanks.game.graphics.effects;

import tanks.game.graphics.shaders.ShaderProgramFactory;

@RegistrableEffectFactory(registerAs = ShaderEffectPrototype.class)
public class ShaderEffectFactory implements ConcreteEffectFactory<ShaderEffect, ShaderEffectPrototype> {

    private final ShaderProgramFactory shaderProgramFactory;

    public ShaderEffectFactory(ShaderProgramFactory shaderProgramFactory) {
        this.shaderProgramFactory = shaderProgramFactory;
    }

    @Override
    public ShaderEffect createEffect(ShaderEffectPrototype prototype) {
        return new ShaderEffect(shaderProgramFactory.createShaderProgram(prototype.getShaderProgramPrototype()));
    }

}
