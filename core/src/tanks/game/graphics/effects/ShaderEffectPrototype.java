package tanks.game.graphics.effects;

import com.google.common.base.MoreObjects;
import tanks.game.graphics.shaders.ShaderProgramPrototype;

public class ShaderEffectPrototype implements EffectPrototype {

    private ShaderProgramPrototype shaderProgramPrototype;

    public ShaderProgramPrototype getShaderProgramPrototype() {
        return shaderProgramPrototype;
    }

    public void setShaderProgramPrototype(ShaderProgramPrototype shaderProgramPrototype) {
        this.shaderProgramPrototype = shaderProgramPrototype;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("shaderProgramPrototype", shaderProgramPrototype)
                .toString();
    }

}
