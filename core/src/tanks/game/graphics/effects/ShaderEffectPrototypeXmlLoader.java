package tanks.game.graphics.effects;

import com.badlogic.gdx.utils.XmlReader;
import tanks.game.graphics.shaders.ShaderProgramPrototypeXmlLoader;

@RegistrableEffectPrototypeXmlLoader(registerAs = "ShaderEffect")
public class ShaderEffectPrototypeXmlLoader implements ConcreteEffectPrototypeXmlLoader<ShaderEffectPrototype> {

    private final ShaderProgramPrototypeXmlLoader shaderProgramPrototypeXmlLoader;

    public ShaderEffectPrototypeXmlLoader(ShaderProgramPrototypeXmlLoader shaderProgramPrototypeXmlLoader) {
        this.shaderProgramPrototypeXmlLoader = shaderProgramPrototypeXmlLoader;
    }

    @Override
    public ShaderEffectPrototype loadEffectPrototype(XmlReader.Element element) {
        ShaderEffectPrototype prototype = new ShaderEffectPrototype();
        prototype.setShaderProgramPrototype(
                shaderProgramPrototypeXmlLoader.loadShaderProgramPrototype(
                        element.getChildByName(ShaderProgramPrototypeXmlLoader.SHADER_PROGRAM_ELEMENT)
                )
        );

        return prototype;
    }

}
