package tanks.game.graphics.effects;

public interface TerminableEffect {

    boolean isTerminated();

}
