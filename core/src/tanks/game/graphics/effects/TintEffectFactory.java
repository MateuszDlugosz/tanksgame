package tanks.game.graphics.effects;

import tanks.game.graphics.colors.ColorFactory;

@RegistrableEffectFactory(registerAs = TintEffectPrototype.class)
public class TintEffectFactory implements ConcreteEffectFactory<TintEffect, TintEffectPrototype> {

    private final ColorFactory colorFactory;

    public TintEffectFactory(ColorFactory colorFactory) {
        this.colorFactory = colorFactory;
    }

    @Override
    public TintEffect createEffect(TintEffectPrototype prototype) {
        return new TintEffect(colorFactory.createColor(prototype.getColorPrototype()));
    }

}
