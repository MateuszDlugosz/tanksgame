package tanks.game.graphics.effects;

import com.google.common.base.MoreObjects;
import tanks.game.graphics.colors.ColorPrototype;

public class TintEffectPrototype implements EffectPrototype {

    private ColorPrototype colorPrototype;

    public ColorPrototype getColorPrototype() {
        return colorPrototype;
    }

    public void setColorPrototype(ColorPrototype colorPrototype) {
        this.colorPrototype = colorPrototype;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("colorPrototype", colorPrototype)
                .toString();
    }

}
