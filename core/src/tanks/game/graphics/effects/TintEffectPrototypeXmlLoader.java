package tanks.game.graphics.effects;

import com.badlogic.gdx.utils.XmlReader;
import tanks.game.graphics.colors.ColorPrototypeXmlLoader;

@RegistrableEffectPrototypeXmlLoader(registerAs = "TintEffect")
public class TintEffectPrototypeXmlLoader implements ConcreteEffectPrototypeXmlLoader<TintEffectPrototype> {

    private final ColorPrototypeXmlLoader colorPrototypeXmlLoader;

    public TintEffectPrototypeXmlLoader(ColorPrototypeXmlLoader colorPrototypeXmlLoader) {
        this.colorPrototypeXmlLoader = colorPrototypeXmlLoader;
    }

    @Override
    public TintEffectPrototype loadEffectPrototype(XmlReader.Element element) {
        TintEffectPrototype prototype = new TintEffectPrototype();
        prototype.setColorPrototype(
                colorPrototypeXmlLoader.loadColorPrototype(
                        element.getChildByName(ColorPrototypeXmlLoader.COLOR_ELEMENT)
                )
        );

        return prototype;
    }

}
