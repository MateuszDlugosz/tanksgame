package tanks.game.graphics.effects;

public interface UpdatableEffect {

    void update(float delta);

}
