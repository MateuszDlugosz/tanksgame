package tanks.game.graphics.particles;

import com.badlogic.gdx.assets.loaders.ParticleEffectLoader;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import tanks.game.assets.AssetStorage;

@RegistrableParticleEffectFactory(registerAs = AtlasParticleEffectPrototype.class)
public class AtlasParticleEffectFactory implements ConcreteParticleEffectFactory<AtlasParticleEffectPrototype> {

    @Override
    public ParticleEffect createParticleEffect(AtlasParticleEffectPrototype prototype, AssetStorage assetStorage) {
        ParticleEffectLoader.ParticleEffectParameter pep = new ParticleEffectLoader.ParticleEffectParameter();
        pep.atlasFile = prototype.getAtlasFilename();

        assetStorage.getAssetManager().load(
                prototype.getFilename(),
                ParticleEffect.class,
                pep
        );
        assetStorage.getAssetManager().finishLoading();

        return assetStorage.getAsset(prototype.getFilename(), ParticleEffect.class);
    }

}
