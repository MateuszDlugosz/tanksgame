package tanks.game.graphics.particles;

import com.google.common.base.MoreObjects;

public class AtlasParticleEffectPrototype implements ParticleEffectPrototype {

    private String filename;
    private String atlasFilename;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getAtlasFilename() {
        return atlasFilename;
    }

    public void setAtlasFilename(String atlasFilename) {
        this.atlasFilename = atlasFilename;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("filename", filename)
                .add("atlasFilename", atlasFilename)
                .toString();
    }

}
