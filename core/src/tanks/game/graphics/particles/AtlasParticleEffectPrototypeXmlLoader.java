package tanks.game.graphics.particles;

import com.badlogic.gdx.utils.XmlReader;

@RegistrableParticleEffectPrototypeXmlLoader(registerAs = "AtlasParticleEffect")
public class AtlasParticleEffectPrototypeXmlLoader implements ConcreteParticleEffectPrototypeXmlLoader<AtlasParticleEffectPrototype> {

    public static final String FILENAME_ELEMENT = "filename";
    public static final String ATLAS_FILENAME_ELEMENT = "atlasFilename";

    @Override
    public AtlasParticleEffectPrototype loadParticleEffectPrototype(XmlReader.Element element) {
        AtlasParticleEffectPrototype prototype = new AtlasParticleEffectPrototype();
        prototype.setFilename(element.getChildByName(FILENAME_ELEMENT).getText().trim());
        prototype.setAtlasFilename(element.getChildByName(ATLAS_FILENAME_ELEMENT).getText().trim());

        return prototype;
    }

}
