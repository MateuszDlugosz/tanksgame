package tanks.game.graphics.particles;

import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import tanks.game.assets.AssetStorage;

public interface ConcreteParticleEffectFactory<T extends ParticleEffectPrototype> {

    ParticleEffect createParticleEffect(T prototype, AssetStorage assetStorage);

}
