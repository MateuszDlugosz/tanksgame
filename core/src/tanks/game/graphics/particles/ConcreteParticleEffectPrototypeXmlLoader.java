package tanks.game.graphics.particles;

import com.badlogic.gdx.utils.XmlReader;

public interface ConcreteParticleEffectPrototypeXmlLoader<T extends ParticleEffectPrototype> {

    T loadParticleEffectPrototype(XmlReader.Element element);

}
