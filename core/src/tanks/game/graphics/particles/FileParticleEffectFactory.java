package tanks.game.graphics.particles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import tanks.game.assets.AssetStorage;

@RegistrableParticleEffectFactory(registerAs = FileParticleEffectPrototype.class)
public class FileParticleEffectFactory implements ConcreteParticleEffectFactory<FileParticleEffectPrototype> {

    @Override
    public ParticleEffect createParticleEffect(FileParticleEffectPrototype prototype, AssetStorage assetStorage) {
        ParticleEffectLoader.ParticleEffectParameter pep = new ParticleEffectLoader.ParticleEffectParameter();
        pep.imagesDir = Gdx.files.internal(prototype.getImagesDir());

        assetStorage.getAssetManager().load(
                prototype.getFilename(),
                ParticleEffect.class,
                pep
        );
        assetStorage.getAssetManager().finishLoading();

        return assetStorage.getAsset(prototype.getFilename(), ParticleEffect.class);
    }

}
