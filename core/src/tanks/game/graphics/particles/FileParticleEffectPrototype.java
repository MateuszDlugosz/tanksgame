package tanks.game.graphics.particles;

import com.google.common.base.MoreObjects;

public class FileParticleEffectPrototype implements ParticleEffectPrototype {

    private String filename;
    private String imagesDir;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getImagesDir() {
        return imagesDir;
    }

    public void setImagesDir(String imagesDir) {
        this.imagesDir = imagesDir;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("filename", filename)
                .add("imagesDir", imagesDir)
                .toString();
    }

}
