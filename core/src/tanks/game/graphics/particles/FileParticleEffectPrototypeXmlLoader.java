package tanks.game.graphics.particles;

import com.badlogic.gdx.utils.XmlReader;

@RegistrableParticleEffectPrototypeXmlLoader(registerAs = "FileParticleEffect")
public class FileParticleEffectPrototypeXmlLoader implements ConcreteParticleEffectPrototypeXmlLoader<FileParticleEffectPrototype> {

    public static final String FILENAME_ELEMENT = "filename";
    public static final String IMAGES_DIR_ELEMENT = "imagesDir";

    @Override
    public FileParticleEffectPrototype loadParticleEffectPrototype(XmlReader.Element element) {
        FileParticleEffectPrototype prototype = new FileParticleEffectPrototype();
        prototype.setFilename(element.getChildByName(FILENAME_ELEMENT).getText().trim());
        prototype.setImagesDir(loadImagesDir(element.getChildByName(IMAGES_DIR_ELEMENT)));

        return prototype;
    }

    private String loadImagesDir(XmlReader.Element element) {
        if (element.getText() == null) return "";
        return element.getText().trim();
    }

}
