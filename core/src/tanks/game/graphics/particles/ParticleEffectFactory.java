package tanks.game.graphics.particles;

import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import tanks.game.assets.AssetStorage;

import java.util.HashMap;
import java.util.Map;

public class ParticleEffectFactory {

    private final Map<Class<? extends ParticleEffectPrototype>, ConcreteParticleEffectFactory> innerFactories;

    public ParticleEffectFactory(ConcreteParticleEffectFactory[] factories) {
        innerFactories = new HashMap<Class<? extends ParticleEffectPrototype>, ConcreteParticleEffectFactory>();

        for (ConcreteParticleEffectFactory factory : factories) {
            RegistrableParticleEffectFactory annotation = factory.getClass().getAnnotation(RegistrableParticleEffectFactory.class);
            if (annotation == null) {
                throw new AnnotationNotFoundException(String.format("Annotation %s not found in class %s.",
                        RegistrableParticleEffectFactory.class, factory.getClass()));
            }

            innerFactories.put(annotation.registerAs(), factory);
        }
    }

    public ParticleEffect createParticleEffect(ParticleEffectPrototype prototype, AssetStorage assetStorage) {
        Class<? extends ParticleEffectPrototype> type = prototype.getClass();

        if (innerFactories.containsKey(type)) {
            return innerFactories.get(type).createParticleEffect(prototype, assetStorage);
        }
        else {
            throw new FactoryNotFoundException(String.format("Factory of type %s not found.", type));
        }
    }

}
