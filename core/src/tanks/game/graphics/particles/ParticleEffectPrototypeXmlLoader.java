package tanks.game.graphics.particles;

import com.badlogic.gdx.utils.XmlReader;

import java.util.HashMap;
import java.util.Map;

public class ParticleEffectPrototypeXmlLoader {

    public static final String PARTICLE_EFFECTS_ELEMENT = "particleEffects";
    public static final String PARTICLE_EFFECT_ELEMENT = "particleEffect";
    public static final String TYPE_ATTRIBUTE = "type";

    private final Map<String, ConcreteParticleEffectPrototypeXmlLoader> innerLoaders;

    public ParticleEffectPrototypeXmlLoader(ConcreteParticleEffectPrototypeXmlLoader[] loaders) {
        innerLoaders = new HashMap<String, ConcreteParticleEffectPrototypeXmlLoader>();

        for (ConcreteParticleEffectPrototypeXmlLoader loader : loaders) {
            RegistrableParticleEffectPrototypeXmlLoader annotation
                    = loader.getClass().getAnnotation(RegistrableParticleEffectPrototypeXmlLoader.class);

            if (annotation == null) {
                throw new AnnotationNotFoundException(String.format("Annotation %s not found in class %s.",
                        RegistrableParticleEffectPrototypeXmlLoader.class, loader.getClass()));
            }

            innerLoaders.put(annotation.registerAs(), loader);
        }
    }

    public ParticleEffectPrototype loadParticleEffectPrototype(XmlReader.Element element) {
        String type = element.getAttribute(TYPE_ATTRIBUTE);

        if (innerLoaders.containsKey(type)) {
            return innerLoaders.get(type).loadParticleEffectPrototype(element);
        }
        else {
            throw new LoaderNotFoundException(String.format("Prototype loader of type %s not found.", type));
        }
    }

}
