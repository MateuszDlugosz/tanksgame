package tanks.game.graphics.shaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

public class ShaderProgramFactory {

    public ShaderProgram createShaderProgram(ShaderProgramPrototype prototype) {
        return new ShaderProgram(
                Gdx.files.internal(prototype.getVertexShaderFilename()),
                Gdx.files.internal(prototype.getFragmentShaderFilename())
        );
    }

}
