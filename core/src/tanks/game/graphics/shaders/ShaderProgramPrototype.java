package tanks.game.graphics.shaders;

import com.google.common.base.MoreObjects;

import java.io.Serializable;

public class ShaderProgramPrototype implements Serializable {

    private String vertexShaderFilename;
    private String fragmentShaderFilename;

    public String getVertexShaderFilename() {
        return vertexShaderFilename;
    }

    public void setVertexShaderFilename(String vertexShaderFilename) {
        this.vertexShaderFilename = vertexShaderFilename;
    }

    public String getFragmentShaderFilename() {
        return fragmentShaderFilename;
    }

    public void setFragmentShaderFilename(String fragmentShaderFilename) {
        this.fragmentShaderFilename = fragmentShaderFilename;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(getClass())
                .add("vertexShaderFilename", vertexShaderFilename)
                .add("fragmentShaderFilename", fragmentShaderFilename)
                .toString();
    }

}
