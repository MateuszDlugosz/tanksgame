package tanks.game.graphics.shaders;

import com.badlogic.gdx.utils.XmlReader;

public class ShaderProgramPrototypeXmlLoader {

    public static final String SHADER_PROGRAM_ELEMENT = "shaderProgram";
    public static final String VERTEX_SHADER_FILENAME_ELEMENT = "vertexShaderFilename";
    public static final String FRAGMENT_SHADER_FILENAME_ELEMENT = "fragmentShaderFilename";

    public ShaderProgramPrototype loadShaderProgramPrototype(XmlReader.Element element) {
        ShaderProgramPrototype prototype = new ShaderProgramPrototype();
        prototype.setVertexShaderFilename(
                element.getChildByName(VERTEX_SHADER_FILENAME_ELEMENT).getText().trim()
        );
        prototype.setFragmentShaderFilename(
                element.getChildByName(FRAGMENT_SHADER_FILENAME_ELEMENT).getText().trim()
        );

        return prototype;
    }

}
