package tanks.game.graphics.sprites;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import tanks.game.assets.AssetStorage;
import tanks.game.utils.scales.Scale;

@RegistrableSpriteFactory(registerAs = AtlasSpritePrototype.class)
public class AtlasSpriteFactory implements ConcreteSpriteFactory<AtlasSpritePrototype> {

    @Override
    public Sprite createSprite(AtlasSpritePrototype prototype, AssetStorage assetStorage, Scale scale) {
        Sprite sprite = new Sprite(
                assetStorage.getAsset(prototype.getAtlasFilename(), TextureAtlas.class)
                        .findRegion(prototype.getRegionName())
        );
        sprite.setSize(
                scale.scaleValue(sprite.getWidth()),
                scale.scaleValue(sprite.getHeight())
        );

        return sprite;
    }

}
