package tanks.game.graphics.sprites;

import com.google.common.base.MoreObjects;

public class AtlasSpritePrototype implements SpritePrototype {

    private String atlasFilename;
    private String regionName;

    public String getAtlasFilename() {
        return atlasFilename;
    }

    public void setAtlasFilename(String atlasFilename) {
        this.atlasFilename = atlasFilename;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("atlasFilename", atlasFilename)
                .add("regionName", regionName)
                .toString();
    }

}
