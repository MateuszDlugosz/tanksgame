package tanks.game.graphics.sprites;

import com.badlogic.gdx.utils.XmlReader;

@RegistrableSpritePrototypeXmlLoader(registerAs = "AtlasSprite")
public class AtlasSpritePrototypeXmlLoader implements ConcreteSpritePrototypeXmlLoader<AtlasSpritePrototype> {

    public static final String ATLAS_FILENAME_ELEMENT = "atlasFilename";
    public static final String REGION_NAME_ELEMENT = "regionName";

    @Override
    public AtlasSpritePrototype loadSpritePrototype(XmlReader.Element element) {
        AtlasSpritePrototype prototype = new AtlasSpritePrototype();
        prototype.setAtlasFilename(element.getChildByName(ATLAS_FILENAME_ELEMENT).getText().trim());
        prototype.setRegionName(element.getChildByName(REGION_NAME_ELEMENT).getText());

        return prototype;
    }

}
