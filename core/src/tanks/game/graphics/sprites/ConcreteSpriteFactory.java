package tanks.game.graphics.sprites;

import com.badlogic.gdx.graphics.g2d.Sprite;
import tanks.game.assets.AssetStorage;
import tanks.game.utils.scales.Scale;

public interface ConcreteSpriteFactory<T extends SpritePrototype> {

    Sprite createSprite(T prototype, AssetStorage assetStorage, Scale scale);

}
