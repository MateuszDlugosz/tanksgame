package tanks.game.graphics.sprites;

import com.badlogic.gdx.utils.XmlReader;

public interface ConcreteSpritePrototypeXmlLoader<T extends SpritePrototype> {

    T loadSpritePrototype(XmlReader.Element element);

}
