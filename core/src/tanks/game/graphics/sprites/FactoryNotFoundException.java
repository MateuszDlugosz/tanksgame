package tanks.game.graphics.sprites;

public class FactoryNotFoundException extends RuntimeException {

    public FactoryNotFoundException(String message) {
        super(message);
    }

}
