package tanks.game.graphics.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import tanks.game.assets.AssetStorage;
import tanks.game.utils.scales.Scale;

@RegistrableSpriteFactory(registerAs = FileSpritePrototype.class)
public class FileSpriteFactory implements ConcreteSpriteFactory<FileSpritePrototype> {

    @Override
    public Sprite createSprite(FileSpritePrototype prototype, AssetStorage assetStorage, Scale scale) {
        Sprite sprite = new Sprite(assetStorage.getAsset(prototype.getFilename(), Texture.class));
        sprite.setSize(
                scale.scaleValue(sprite.getWidth()),
                scale.scaleValue(sprite.getHeight())
        );

        return sprite;
    }

}
