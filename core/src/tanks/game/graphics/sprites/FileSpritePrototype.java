package tanks.game.graphics.sprites;

import com.google.common.base.MoreObjects;

public class FileSpritePrototype implements SpritePrototype {

    private String filename;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("filename", filename)
                .toString();
    }

}
