package tanks.game.graphics.sprites;

import com.badlogic.gdx.utils.XmlReader;

@RegistrableSpritePrototypeXmlLoader(registerAs = "FileSprite")
public class FileSpritePrototypeXmlLoader implements ConcreteSpritePrototypeXmlLoader<FileSpritePrototype> {

    public static final String FILENAME_ELEMENT = "filename";

    @Override
    public FileSpritePrototype loadSpritePrototype(XmlReader.Element element) {
        FileSpritePrototype prototype = new FileSpritePrototype();
        prototype.setFilename(element.getChildByName(FILENAME_ELEMENT).getText().trim());

        return prototype;
    }

}
