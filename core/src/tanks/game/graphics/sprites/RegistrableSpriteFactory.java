package tanks.game.graphics.sprites;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@interface RegistrableSpriteFactory {

    Class<? extends SpritePrototype> registerAs();

}
