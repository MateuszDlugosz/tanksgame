package tanks.game.graphics.sprites;

import com.badlogic.gdx.graphics.g2d.Sprite;
import tanks.game.assets.AssetStorage;
import tanks.game.utils.scales.Scale;

import java.util.HashMap;
import java.util.Map;

public class SpriteFactory {

    private final Map<Class<? extends SpritePrototype>, ConcreteSpriteFactory> innerFactories;

    public SpriteFactory(ConcreteSpriteFactory[] factories) {
        innerFactories = new HashMap<Class<? extends SpritePrototype>, ConcreteSpriteFactory>();

        for (ConcreteSpriteFactory factory : factories) {
            RegistrableSpriteFactory annotation = factory.getClass().getAnnotation(RegistrableSpriteFactory.class);
            if (annotation == null) {
                throw new AnnotationNotFoundException(String.format("Annotation %s not found in class %s.",
                        RegistrableSpriteFactory.class, factory.getClass()));
            }

            innerFactories.put(annotation.registerAs(), factory);
        }
    }

    public Sprite createSprite(SpritePrototype prototype, AssetStorage assetStorage, Scale scale) {
        Class<? extends SpritePrototype> type = prototype.getClass();

        if (innerFactories.containsKey(type)) {
            return innerFactories.get(type).createSprite(prototype, assetStorage, scale);
        }
        else {
            throw new FactoryNotFoundException(String.format("Factory of type %s not found.", type));
        }
    }

}
