package tanks.game.graphics.sprites;

import com.badlogic.gdx.utils.XmlReader;

import java.util.HashMap;
import java.util.Map;

public class SpritePrototypeXmlLoader {

    public static final String SPRITES_ELEMENT = "sprites";
    public static final String SPRITE_ELEMENT = "sprite";
    public static final String TYPE_ATTRIBUTE = "type";

    private final Map<String, ConcreteSpritePrototypeXmlLoader> innerLoaders;

    public SpritePrototypeXmlLoader(ConcreteSpritePrototypeXmlLoader[] loaders) {
        innerLoaders = new HashMap<String, ConcreteSpritePrototypeXmlLoader>();

        for (ConcreteSpritePrototypeXmlLoader loader : loaders) {
            RegistrableSpritePrototypeXmlLoader annotation
                    = loader.getClass().getAnnotation(RegistrableSpritePrototypeXmlLoader.class);

            if (annotation == null) {
                throw new AnnotationNotFoundException(String.format("Annotation %s not found in class %s.",
                        RegistrableSpritePrototypeXmlLoader.class, loader.getClass()));
            }

            innerLoaders.put(annotation.registerAs(), loader);
        }
    }

    public SpritePrototype loadSpritePrototype(XmlReader.Element element) {
        String type = element.getAttribute(TYPE_ATTRIBUTE);

        if (innerLoaders.containsKey(type)) {
            return innerLoaders.get(type).loadSpritePrototype(element);
        }
        else {
            throw new LoaderNotFoundException(String.format("Prototype loader of type %s not found.", type));
        }
    }

}
