package tanks.game.graphics.viewports;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.utils.viewport.FitViewport;
import tanks.game.utils.scales.Scale;

public class FitViewportFactory implements ViewportFactory<FitViewport> {

    @Override
    public FitViewport createViewport(float width, float height, Scale scale, Camera camera) {
        FitViewport viewport = new FitViewport(
                scale.scaleValue(width),
                scale.scaleValue(height),
                camera
        );

        return viewport;
    }

}
