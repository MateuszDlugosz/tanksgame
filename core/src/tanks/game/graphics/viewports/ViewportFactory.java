package tanks.game.graphics.viewports;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.utils.viewport.Viewport;
import tanks.game.utils.scales.Scale;

public interface ViewportFactory<T extends Viewport> {

    T createViewport(float width, float height, Scale scale, Camera camera);

}
