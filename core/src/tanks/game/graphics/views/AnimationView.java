package tanks.game.graphics.views;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import tanks.game.utils.positions.Position;

public class AnimationView implements View, UpdatableView, ResettableView, FlippableView, RotatableView {

    private final String id;
    private final Position position;
    private final Animation animation;
    private float stateTime;
    private boolean flipX;
    private boolean flipY;
    private float angle;

    public AnimationView(String id, Position position, Animation animation) {
        this.id = id;
        this.position = position;
        this.animation = animation;
        this.flipX = false;
        this.flipY = false;
        this.angle = 0;
    }

    @Override
    public void update(float delta) {
        stateTime += delta;
    }

    @Override
    public void reset() {
        stateTime = 0;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Position getPosition() {
        return position;
    }

    public Animation getAnimation() {
        return animation;
    }

    public Sprite getCurrentSprite() {
        return (Sprite) animation.getKeyFrame(stateTime);
    }

    @Override
    public void setFlipX(boolean flip) {
        this.flipX = flip;
    }

    @Override
    public boolean getFlipX() {
        return flipX;
    }

    @Override
    public void setFlipY(boolean flip) {
        this.flipY = flip;
    }

    @Override
    public boolean getFlipY() {
        return flipY;
    }

    @Override
    public void setAngle(float angle) {
        this.angle = angle;
    }

    @Override
    public float getAngle() {
        return angle;
    }
}
