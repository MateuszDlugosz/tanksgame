package tanks.game.graphics.views;

import tanks.game.assets.AssetStorage;
import tanks.game.graphics.animations.AnimationFactory;
import tanks.game.utils.positions.PositionFactory;
import tanks.game.utils.scales.Scale;

@RegistrableViewFactory(registerAs = AnimationViewPrototype.class)
public class AnimationViewFactory implements ConcreteViewFactory<AnimationView, AnimationViewPrototype> {

    private final PositionFactory positionFactory;
    private final AnimationFactory animationFactory;

    public AnimationViewFactory(PositionFactory positionFactory, AnimationFactory animationFactory) {
        this.positionFactory = positionFactory;
        this.animationFactory = animationFactory;
    }

    @Override
    public AnimationView createView(AnimationViewPrototype prototype, AssetStorage assetStorage, Scale scale) {
        return new AnimationView(
                prototype.getId(),
                positionFactory.createPosition(prototype.getPositionPrototype(), scale),
                animationFactory.createAnimation(prototype.getAnimationPrototype(), assetStorage, scale)
        );
    }

}
