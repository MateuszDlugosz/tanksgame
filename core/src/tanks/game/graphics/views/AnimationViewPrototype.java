package tanks.game.graphics.views;

import com.google.common.base.MoreObjects;
import tanks.game.graphics.animations.AnimationPrototype;
import tanks.game.utils.positions.PositionPrototype;

public class AnimationViewPrototype implements ViewPrototype {

    private String id;
    private PositionPrototype positionPrototype;
    private AnimationPrototype animationPrototype;

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setPositionPrototype(PositionPrototype positionPrototype) {
        this.positionPrototype = positionPrototype;
    }

    @Override
    public PositionPrototype getPositionPrototype() {
        return positionPrototype;
    }

    public AnimationPrototype getAnimationPrototype() {
        return animationPrototype;
    }

    public void setAnimationPrototype(AnimationPrototype animationPrototype) {
        this.animationPrototype = animationPrototype;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("positionPrototype", positionPrototype)
                .add("animationPrototype", animationPrototype)
                .toString();
    }

}
