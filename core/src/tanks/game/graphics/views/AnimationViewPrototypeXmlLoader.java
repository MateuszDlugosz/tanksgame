package tanks.game.graphics.views;

import com.badlogic.gdx.utils.XmlReader;
import tanks.game.graphics.animations.AnimationPrototypeXmlLoader;
import tanks.game.utils.positions.PositionPrototype;
import tanks.game.utils.positions.PositionPrototypeXmlLoader;

@RegistrableViewPrototypeXmlLoader(registerAs = "AnimationView")
public class AnimationViewPrototypeXmlLoader implements ConcreteViewPrototypeXmlLoader<AnimationViewPrototype> {

    public static final String ID_ATTRIBUTE = "id";

    private final PositionPrototypeXmlLoader positionPrototypeXmlLoader;
    private final AnimationPrototypeXmlLoader animationPrototypeXmlLoader;

    public AnimationViewPrototypeXmlLoader(PositionPrototypeXmlLoader positionPrototypeXmlLoader,
                                           AnimationPrototypeXmlLoader animationPrototypeXmlLoader)
    {
        this.positionPrototypeXmlLoader = positionPrototypeXmlLoader;
        this.animationPrototypeXmlLoader = animationPrototypeXmlLoader;
    }

    @Override
    public AnimationViewPrototype loadViewPrototype(XmlReader.Element element) {
        AnimationViewPrototype prototype = new AnimationViewPrototype();
        prototype.setId(element.getAttribute(ID_ATTRIBUTE));
        prototype.setAnimationPrototype(
                animationPrototypeXmlLoader.loadAnimationPrototype(
                        element.getChildByName(AnimationPrototypeXmlLoader.ANIMATION_ELEMENT)
                )
        );
        prototype.setPositionPrototype(loadViewPosition(element));

        return prototype;
    }

    private PositionPrototype loadViewPosition(XmlReader.Element element) {
        XmlReader.Element posElement = element.getChildByName(PositionPrototypeXmlLoader.POSITION_ELEMENT);

        if (posElement != null) {
            return positionPrototypeXmlLoader.loadPositionPrototype(posElement);
        }
        else {
            PositionPrototype prototype = new PositionPrototype();
            prototype.setX(0);
            prototype.setY(0);

            return prototype;
        }
    }

}
