package tanks.game.graphics.views;

public class AnnotationNotFoundException extends RuntimeException {

    public AnnotationNotFoundException(String message) {
        super(message);
    }

}
