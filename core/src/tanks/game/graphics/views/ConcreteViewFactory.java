package tanks.game.graphics.views;

import tanks.game.assets.AssetStorage;
import tanks.game.utils.scales.Scale;

public interface ConcreteViewFactory<T extends View, U extends ViewPrototype> {

    T createView(U prototype, AssetStorage assetStorage, Scale scale);

}
