package tanks.game.graphics.views;

import com.badlogic.gdx.utils.XmlReader;

public interface ConcreteViewPrototypeXmlLoader<T extends ViewPrototype> {

    T loadViewPrototype(XmlReader.Element element);

}
