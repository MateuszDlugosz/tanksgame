package tanks.game.graphics.views;

public class FactoryNotFoundException extends RuntimeException {

    public FactoryNotFoundException(String message) {
        super(message);
    }

}
