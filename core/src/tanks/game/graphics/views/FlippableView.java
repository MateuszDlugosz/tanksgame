package tanks.game.graphics.views;

public interface FlippableView {

    void setFlipX(boolean flip);
    boolean getFlipX();
    void setFlipY(boolean flip);
    boolean getFlipY();

}
