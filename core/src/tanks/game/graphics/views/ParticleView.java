package tanks.game.graphics.views;

import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import tanks.game.utils.positions.Position;

public class ParticleView implements View, UpdatableView, ResettableView, FlippableView {

    private final String id;
    private final Position position;
    private final ParticleEffect particleEffect;
    private boolean flipX;
    private boolean flipY;

    public ParticleView(String id, Position position, ParticleEffect particleEffect) {
        this.id = id;
        this.position = position;
        this.particleEffect = particleEffect;
        this.flipX = false;
        this.flipY = false;
    }

    @Override
    public void update(float delta) {
        particleEffect.update(delta);
    }

    @Override
    public void reset() {
        particleEffect.reset();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Position getPosition() {
        return position;
    }

    public ParticleEffect getParticleEffect() {
        return particleEffect;
    }

    @Override
    public void setFlipX(boolean flip) {
        this.flipX = flip;
    }

    @Override
    public boolean getFlipX() {
        return flipX;
    }

    @Override
    public void setFlipY(boolean flip) {
        this.flipY = flip;
    }

    @Override
    public boolean getFlipY() {
        return flipY;
    }

}
