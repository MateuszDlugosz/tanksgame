package tanks.game.graphics.views;

import tanks.game.assets.AssetStorage;
import tanks.game.graphics.particles.ParticleEffectFactory;
import tanks.game.utils.positions.PositionFactory;
import tanks.game.utils.scales.Scale;

@RegistrableViewFactory(registerAs = ParticleViewPrototype.class)
public class ParticleViewFactory implements ConcreteViewFactory<ParticleView, ParticleViewPrototype> {

    private final PositionFactory positionFactory;
    private final ParticleEffectFactory particleEffectFactory;

    public ParticleViewFactory(PositionFactory positionFactory, ParticleEffectFactory particleEffectFactory) {
        this.positionFactory = positionFactory;
        this.particleEffectFactory = particleEffectFactory;
    }

    @Override
    public ParticleView createView(ParticleViewPrototype prototype, AssetStorage assetStorage, Scale scale) {
        return new ParticleView(
                prototype.getId(),
                positionFactory.createPosition(
                        prototype.getPositionPrototype(),
                        scale
                ),
                particleEffectFactory.createParticleEffect(
                        prototype.getParticleEffectPrototype(),
                        assetStorage
                )
        );
    }

}
