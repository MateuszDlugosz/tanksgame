package tanks.game.graphics.views;

import com.google.common.base.MoreObjects;
import tanks.game.graphics.particles.ParticleEffectPrototype;
import tanks.game.utils.positions.PositionPrototype;

public class ParticleViewPrototype implements ViewPrototype {

    private String id;
    private PositionPrototype positionPrototype;
    private ParticleEffectPrototype particleEffectPrototype;

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setPositionPrototype(PositionPrototype positionPrototype) {
        this.positionPrototype = positionPrototype;
    }

    @Override
    public PositionPrototype getPositionPrototype() {
        return positionPrototype;
    }

    public ParticleEffectPrototype getParticleEffectPrototype() {
        return particleEffectPrototype;
    }

    public void setParticleEffectPrototype(ParticleEffectPrototype particleEffectPrototype) {
        this.particleEffectPrototype = particleEffectPrototype;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("positionPrototype", positionPrototype)
                .add("particleEffectPrototype", particleEffectPrototype)
                .toString();
    }

}
