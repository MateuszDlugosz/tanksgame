package tanks.game.graphics.views;

import com.badlogic.gdx.utils.XmlReader;
import tanks.game.graphics.particles.ParticleEffectPrototypeXmlLoader;
import tanks.game.utils.positions.PositionPrototypeXmlLoader;

@RegistrableViewPrototypeXmlLoader(registerAs = "ParticleView")
public class ParticleViewPrototypeXmlLoader implements ConcreteViewPrototypeXmlLoader<ParticleViewPrototype> {

    public static final String ID_ATTRIBUTE = "id";

    private final PositionPrototypeXmlLoader positionPrototypeXmlLoader;
    private final ParticleEffectPrototypeXmlLoader particleEffectPrototypeXmlLoader;

    public ParticleViewPrototypeXmlLoader(PositionPrototypeXmlLoader positionPrototypeXmlLoader,
                                          ParticleEffectPrototypeXmlLoader particleEffectPrototypeXmlLoader)
    {
        this.positionPrototypeXmlLoader = positionPrototypeXmlLoader;
        this.particleEffectPrototypeXmlLoader = particleEffectPrototypeXmlLoader;
    }

    @Override
    public ParticleViewPrototype loadViewPrototype(XmlReader.Element element) {
        ParticleViewPrototype prototype = new ParticleViewPrototype();
        prototype.setId(element.getAttribute(ID_ATTRIBUTE));
        prototype.setPositionPrototype(
                positionPrototypeXmlLoader.loadPositionPrototype(
                        element.getChildByName(PositionPrototypeXmlLoader.POSITION_ELEMENT)
                )
        );
        prototype.setParticleEffectPrototype(
                particleEffectPrototypeXmlLoader.loadParticleEffectPrototype(
                        element.getChildByName(ParticleEffectPrototypeXmlLoader.PARTICLE_EFFECT_ELEMENT)
                )
        );

        return prototype;
    }

}
