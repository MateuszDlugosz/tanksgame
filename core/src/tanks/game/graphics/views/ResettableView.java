package tanks.game.graphics.views;

public interface ResettableView {

    void reset();

}
