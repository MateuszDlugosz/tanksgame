package tanks.game.graphics.views;

public interface RotatableView {

    void setAngle(float angle);
    float getAngle();

}
