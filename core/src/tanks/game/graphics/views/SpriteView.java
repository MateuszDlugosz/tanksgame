package tanks.game.graphics.views;

import com.badlogic.gdx.graphics.g2d.Sprite;
import tanks.game.utils.positions.Position;

public class SpriteView implements View, FlippableView, RotatableView {

    private final String id;
    private final Position position;
    private final Sprite sprite;
    private boolean flipX;
    private boolean flipY;
    private float angle;

    public SpriteView(String id, Position position, Sprite sprite) {
        this.id = id;
        this.position = position;
        this.sprite = sprite;
        this.flipX = false;
        this.flipY = false;
        this.angle = 0;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Position getPosition() {
        return position;
    }

    public Sprite getSprite() {
        return sprite;
    }

    @Override
    public void setFlipX(boolean flip) {
        this.flipX = flip;
    }

    @Override
    public boolean getFlipX() {
        return flipX;
    }

    @Override
    public void setFlipY(boolean flip) {
        this.flipY = flip;
    }

    @Override
    public boolean getFlipY() {
        return flipY;
    }

    @Override
    public void setAngle(float angle) {
        this.angle = angle;
    }

    @Override
    public float getAngle() {
        return angle;
    }

}
