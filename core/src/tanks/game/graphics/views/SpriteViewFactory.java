package tanks.game.graphics.views;

import tanks.game.assets.AssetStorage;
import tanks.game.graphics.sprites.SpriteFactory;
import tanks.game.utils.positions.PositionFactory;
import tanks.game.utils.scales.Scale;

@RegistrableViewFactory(registerAs = SpriteViewPrototype.class)
public class SpriteViewFactory implements ConcreteViewFactory<SpriteView, SpriteViewPrototype> {

    private final PositionFactory positionFactory;
    private final SpriteFactory spriteFactory;

    public SpriteViewFactory(PositionFactory positionFactory, SpriteFactory spriteFactory) {
        this.positionFactory = positionFactory;
        this.spriteFactory = spriteFactory;
    }

    @Override
    public SpriteView createView(SpriteViewPrototype prototype, AssetStorage assetStorage, Scale scale) {
        return new SpriteView(
                prototype.getId(),
                positionFactory.createPosition(prototype.getPositionPrototype(), scale),
                spriteFactory.createSprite(prototype.getSpritePrototype(), assetStorage, scale)
        );
    }

}
