package tanks.game.graphics.views;

import com.google.common.base.MoreObjects;
import tanks.game.graphics.sprites.SpritePrototype;
import tanks.game.utils.positions.PositionPrototype;

public class SpriteViewPrototype implements ViewPrototype {

    private String id;
    private PositionPrototype positionPrototype;
    private SpritePrototype spritePrototype;

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setPositionPrototype(PositionPrototype positionPrototype) {
        this.positionPrototype = positionPrototype;
    }

    @Override
    public PositionPrototype getPositionPrototype() {
        return positionPrototype;
    }

    public SpritePrototype getSpritePrototype() {
        return spritePrototype;
    }

    public void setSpritePrototype(SpritePrototype spritePrototype) {
        this.spritePrototype = spritePrototype;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("positionPrototype", positionPrototype)
                .add("spritePrototype", spritePrototype)
                .toString();
    }

}
