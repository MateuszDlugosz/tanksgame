package tanks.game.graphics.views;

import com.badlogic.gdx.utils.XmlReader;
import tanks.game.graphics.sprites.SpritePrototypeXmlLoader;
import tanks.game.utils.positions.PositionPrototype;
import tanks.game.utils.positions.PositionPrototypeXmlLoader;

@RegistrableViewPrototypeXmlLoader(registerAs = "SpriteView")
public class SpriteViewPrototypeXmlLoader implements ConcreteViewPrototypeXmlLoader<SpriteViewPrototype> {

    public static final String ID_ATTRIBUTE = "id";

    private final PositionPrototypeXmlLoader positionPrototypeXmlLoader;
    private final SpritePrototypeXmlLoader spritePrototypeXmlLoader;

    public SpriteViewPrototypeXmlLoader(PositionPrototypeXmlLoader positionPrototypeXmlLoader,
                                        SpritePrototypeXmlLoader spritePrototypeXmlLoader)
    {
        this.positionPrototypeXmlLoader = positionPrototypeXmlLoader;
        this.spritePrototypeXmlLoader = spritePrototypeXmlLoader;
    }

    @Override
    public SpriteViewPrototype loadViewPrototype(XmlReader.Element element) {
        SpriteViewPrototype prototype = new SpriteViewPrototype();
        prototype.setId(element.getAttribute(ID_ATTRIBUTE));
        prototype.setSpritePrototype(
                spritePrototypeXmlLoader.loadSpritePrototype(
                        element.getChildByName(SpritePrototypeXmlLoader.SPRITE_ELEMENT)
                )
        );
        prototype.setPositionPrototype(loadViewPosition(element));

        return prototype;
    }

    private PositionPrototype loadViewPosition(XmlReader.Element element) {
        XmlReader.Element posElement = element.getChildByName(PositionPrototypeXmlLoader.POSITION_ELEMENT);

        if (posElement != null) {
            return positionPrototypeXmlLoader.loadPositionPrototype(posElement);
        }
        else {
            PositionPrototype prototype = new PositionPrototype();
            prototype.setX(0);
            prototype.setY(0);

            return prototype;
        }
    }

}
