package tanks.game.graphics.views;

public interface UpdatableView {

    void update(float delta);

}
