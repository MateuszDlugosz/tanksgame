package tanks.game.graphics.views;

import tanks.game.utils.positions.Position;

public interface View {

    String getId();
    Position getPosition();

}
