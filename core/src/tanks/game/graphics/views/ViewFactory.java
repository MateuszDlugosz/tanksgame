package tanks.game.graphics.views;

import tanks.game.assets.AssetStorage;
import tanks.game.utils.scales.Scale;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViewFactory {

    private final Map<Class<? extends ViewPrototype>, ConcreteViewFactory> innerFactories;

    public ViewFactory(ConcreteViewFactory[] factories) {
        innerFactories = new HashMap<Class<? extends ViewPrototype>, ConcreteViewFactory>();

        for (ConcreteViewFactory factory : factories) {
            RegistrableViewFactory annotation = factory.getClass().getAnnotation(RegistrableViewFactory.class);
            if (annotation == null) {
                throw new AnnotationNotFoundException(String.format("Annotation %s not found in class %s.",
                        RegistrableViewFactory.class, factory.getClass()));
            }

            innerFactories.put(annotation.registerAs(), factory);
        }
    }

    public View createView(ViewPrototype prototype, AssetStorage assetStorage, Scale scale) {
        Class<? extends ViewPrototype> type = prototype.getClass();

        if (innerFactories.containsKey(type)) {
            return innerFactories.get(type).createView(prototype, assetStorage, scale);
        }
        else {
            throw new FactoryNotFoundException(String.format("Factory of type %s not found.", type));
        }
    }

    public Map<String, View> createViews(List<ViewPrototype> prototypes, AssetStorage assetStorage, Scale scale) {
        Map<String, View> views = new HashMap<String, View>();

        for (ViewPrototype prototype : prototypes) {
            View view = createView(prototype, assetStorage, scale);
            views.put(view.getId(), view);
        }

        return views;
    }

}
