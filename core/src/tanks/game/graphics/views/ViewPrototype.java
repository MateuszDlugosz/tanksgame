package tanks.game.graphics.views;

import tanks.game.utils.positions.PositionPrototype;

import java.io.Serializable;

public interface ViewPrototype extends Serializable {

    void setId(String id);
    String getId();
    void setPositionPrototype(PositionPrototype positionPrototype);
    PositionPrototype getPositionPrototype();

}
