package tanks.game.graphics.views;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViewPrototypeXmlLoader {

    public static final String VIEW_ELEMENT = "view";
    public static final String VIEWS_ELEMENT = "views";
    public static final String TYPE_ATTRIBUTE = "type";

    private final Map<String, ConcreteViewPrototypeXmlLoader> innerLoaders;

    public ViewPrototypeXmlLoader(ConcreteViewPrototypeXmlLoader[] loaders) {
        innerLoaders = new HashMap<String, ConcreteViewPrototypeXmlLoader>();

        for (ConcreteViewPrototypeXmlLoader loader : loaders) {
            RegistrableViewPrototypeXmlLoader annotation
                    = loader.getClass().getAnnotation(RegistrableViewPrototypeXmlLoader.class);

            if (annotation == null) {
                throw new AnnotationNotFoundException(String.format("Annotation %s not found in class %s.",
                        RegistrableViewPrototypeXmlLoader.class, loader.getClass()));
            }

            innerLoaders.put(annotation.registerAs(), loader);
        }
    }

    public ViewPrototype loadViewPrototype(XmlReader.Element element) {
        String type = element.getAttribute(TYPE_ATTRIBUTE);

        if (innerLoaders.containsKey(type)) {
            return innerLoaders.get(type).loadViewPrototype(element);
        }
        else {
            throw new LoaderNotFoundException(String.format("Prototype loader of type %s not found.", type));
        }
    }

    public List<ViewPrototype> loadViewPrototypes(Array<XmlReader.Element> elements) {
        List<ViewPrototype> prototypes = new ArrayList<ViewPrototype>();

        for (XmlReader.Element element : elements) {
            prototypes.add(loadViewPrototype(element));
        }

        return prototypes;
    }

}
