package tanks.game.graphics.views.renderers;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import tanks.game.graphics.views.AnimationView;
import tanks.game.utils.positions.Position;

@RegistrableViewRenderer(registerAs = AnimationView.class)
public class AnimationViewRenderer implements ConcreteViewRenderer<AnimationView> {

    @Override
    public void render(AnimationView view, SpriteBatch spriteBatch, Position position, float angle) {
        view.getCurrentSprite().setFlip(view.getFlipX(), view.getFlipY());
        float width = view.getCurrentSprite().getWidth();
        float height = view.getCurrentSprite().getHeight();
        float posX = position.getX() - ((width - view.getPosition().getX()) * 0.5f);
        float posY = position.getY() - ((height - view.getPosition().getY()) * 0.5f);
        float origX = ((width - view.getPosition().getX())) * 0.5f;
        float origY = ((height - view.getPosition().getY())) * 0.5f;

        spriteBatch.begin();
        spriteBatch.draw(
                (TextureRegion) view.getCurrentSprite(),
                posX, posY,
                origX, origY,
                width, height,
                1, 1,
                angle + view.getAngle()
        );
        spriteBatch.end();
    }

}
