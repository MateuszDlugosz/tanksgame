package tanks.game.graphics.views.renderers;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import tanks.game.graphics.views.View;
import tanks.game.utils.positions.Position;

public interface ConcreteViewRenderer<T extends View> {

    void render(T view, SpriteBatch spriteBatch, Position position, float angle);

}
