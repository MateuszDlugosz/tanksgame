package tanks.game.graphics.views.renderers;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import tanks.game.graphics.views.ParticleView;
import tanks.game.utils.positions.Position;

@RegistrableViewRenderer(registerAs = ParticleView.class)
public class ParticleViewRenderer implements ConcreteViewRenderer<ParticleView> {

    @Override
    public void render(ParticleView view, SpriteBatch spriteBatch, Position position, float angle) {
        view.getParticleEffect().setPosition(position.getX(), position.getY());
        view.getParticleEffect().setFlip(view.getFlipX(), view.getFlipY());
        view.getParticleEffect().draw(spriteBatch);
    }

}
