package tanks.game.graphics.views.renderers;

public class RendererNotFoundException extends RuntimeException {

    public RendererNotFoundException(String message) {
        super(message);
    }

}
