package tanks.game.graphics.views.renderers;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import tanks.game.graphics.views.SpriteView;
import tanks.game.utils.positions.Position;

@RegistrableViewRenderer(registerAs = SpriteView.class)
public class SpriteViewRenderer implements ConcreteViewRenderer<SpriteView> {

    @Override
    public void render(SpriteView view, SpriteBatch spriteBatch, Position position, float angle) {
        view.getSprite().setFlip(view.getFlipX(), view.getFlipY());
        float width = view.getSprite().getWidth();
        float height = view.getSprite().getHeight();
        float posX = position.getX() - ((width - view.getPosition().getX()) * 0.5f);
        float posY = position.getY() - ((height - view.getPosition().getY()) * 0.5f);
        float origX = ((width - view.getPosition().getX())) * 0.5f;
        float origY = ((height - view.getPosition().getY())) * 0.5f;

        spriteBatch.begin();
        spriteBatch.draw(
                (TextureRegion) view.getSprite(),
                posX, posY,
                origX, origY,
                width, height,
                1, 1,
                angle + view.getAngle()
        );
        spriteBatch.end();
    }

}
