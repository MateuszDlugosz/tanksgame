package tanks.game.graphics.views.renderers;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import tanks.game.graphics.views.View;
import tanks.game.utils.positions.Position;

import java.util.HashMap;
import java.util.Map;

public class ViewRenderer {

    private final Map<Class<? extends View>, ConcreteViewRenderer> innerRenderers;

    public ViewRenderer(ConcreteViewRenderer[] renderers) {
        innerRenderers = new HashMap<Class<? extends View>, ConcreteViewRenderer>();

        for (ConcreteViewRenderer renderer : renderers) {
            RegistrableViewRenderer annotation = renderer.getClass().getAnnotation(RegistrableViewRenderer.class);
            if (annotation == null)
                throw new AnnotationNotFoundException(
                        String.format(
                                "Annotation %s not found in class %s.",
                                RegistrableViewRenderer.class,
                                renderer.getClass()
                        )
                );

            innerRenderers.put(annotation.registerAs(), renderer);
        }
    }

    public void render(View view, SpriteBatch spriteBatch, Position position, float angle) {
        if (innerRenderers.containsKey(view.getClass())) {
            innerRenderers.get(view.getClass()).render(view, spriteBatch, position, angle);
        }
        else {
            throw new RendererNotFoundException(String.format("Renderer of view %s not found.", view.getClass()));
        }
    }

}
