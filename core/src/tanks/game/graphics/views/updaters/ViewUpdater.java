package tanks.game.graphics.views.updaters;

import tanks.game.graphics.views.UpdatableView;
import tanks.game.graphics.views.View;

import java.util.List;

public class ViewUpdater {

    public void update(View view, float delta) {
        if (view instanceof UpdatableView) {
            ((UpdatableView) view).update(delta);
        }
    }

    public void update(List<View> views, float delta) {
        for (View view : views) {
            update(view, delta);
        }
    }

}
