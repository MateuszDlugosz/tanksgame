package tanks.game.levels;

import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.google.common.eventbus.EventBus;
import tanks.game.assets.AssetStorage;
import tanks.game.utils.scales.Scale;

public class Level {

    private final Scale scale;
    private final AssetStorage assetStorage;
    private final EventBus eventBus;
    private final Engine engine;
    private final World world;

    public Level(LevelBuilder builder) {
        scale = builder.getScale();
        assetStorage = builder.getAssetStorage();
        eventBus = builder.getEventBus();
        engine = builder.getEngine();
        world = builder.getWorld();
    }

    public Scale getScale() {
        return scale;
    }

    public AssetStorage getAssetStorage() {
        return assetStorage;
    }

    public EventBus getEventBus() {
        return eventBus;
    }

    public Engine getEngine() {
        return engine;
    }

    public World getWorld() {
        return world;
    }

}
