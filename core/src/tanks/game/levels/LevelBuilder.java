package tanks.game.levels;

import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.google.common.eventbus.EventBus;
import tanks.game.assets.AssetStorage;
import tanks.game.utils.scales.Scale;

public class LevelBuilder {

    private Scale scale;
    private AssetStorage assetStorage;
    private EventBus eventBus;
    private World world;
    private Engine engine;

    public Scale getScale() {
        if (scale == null)
            throw new LevelBuilderException("Scale is not set.");

        return scale;
    }

    public LevelBuilder setScale(Scale scale) {
        this.scale = scale;
        return this;
    }

    public AssetStorage getAssetStorage() {
        if (assetStorage == null)
            throw new LevelBuilderException("AssetStorage is not set.");

        return assetStorage;
    }

    public LevelBuilder setAssetStorage(AssetStorage assetStorage) {
        this.assetStorage = assetStorage;
        return this;
    }

    public EventBus getEventBus() {
        if (eventBus == null)
            throw new LevelBuilderException("EventBus is not set.");

        return eventBus;
    }

    public LevelBuilder setEventBus(EventBus eventBus) {
        this.eventBus = eventBus;
        return this;
    }

    public World getWorld() {
        if (world == null)
            throw new LevelBuilderException("World is not set.");

        return world;
    }

    public LevelBuilder setWorld(World world) {
        this.world = world;
        return this;
    }

    public Engine getEngine() {
        if (engine == null)
            throw new LevelBuilderException("Engine is not set.");

        return engine;
    }

    public LevelBuilder setEngine(Engine engine) {
        this.engine = engine;
        return this;
    }

    public Level build() {
        return new Level(this);
    }

}
