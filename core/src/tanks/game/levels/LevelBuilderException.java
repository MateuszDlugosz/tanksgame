package tanks.game.levels;

public class LevelBuilderException extends RuntimeException {

    public LevelBuilderException(String message) {
        super(message);
    }

}
