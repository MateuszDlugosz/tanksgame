package tanks.game.levels;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.google.common.eventbus.Subscribe;
import tanks.game.levels.entities.EntityFactory;
import tanks.game.levels.entities.EntityPrototype;
import tanks.game.levels.entities.EntityPrototypeRepository;
import tanks.game.levels.entities.components.ComponentPrototype;
import tanks.game.levels.entities.components.DirectionComponentPrototype;
import tanks.game.levels.entities.components.PhysicsComponent;
import tanks.game.levels.events.DestroyEntityEvent;
import tanks.game.levels.events.SpawnBulletEvent;
import tanks.game.levels.events.SpawnExplosionEvent;
import tanks.game.levels.renderers.LevelRenderer;
import tanks.game.levels.updaters.LevelUpdater;

import java.util.ArrayList;
import java.util.List;

public class LevelController {

    private final Level level;
    private final EntityPrototypeRepository repository;
    private final EntityFactory entityFactory;
    private final LevelUpdater levelUpdater;
    private final LevelRenderer levelRenderer;

    private List<Entity> destroyQueue;

    public LevelController(Level level,
                           EntityPrototypeRepository repository,
                           EntityFactory entityFactory,
                           LevelUpdater levelUpdater,
                           LevelRenderer levelRenderer)
    {
        this.level = level;
        this.repository = repository;
        this.entityFactory = entityFactory;
        this.levelUpdater = levelUpdater;
        this.levelRenderer = levelRenderer;
        this.destroyQueue = new ArrayList<Entity>();
    }

    public Level getLevel() {
        return level;
    }

    @Subscribe
    public void handleSpawnBulletEvent(SpawnBulletEvent event) {
        String code;
        if (event.isPlayerBullet()) code = "PlayerBullet";
            else code = "EnemyBullet";

        EntityPrototype prototype = repository.getEntityPrototype(code);
        for (ComponentPrototype cp : prototype.getComponentPrototypes()) {
            if (cp instanceof DirectionComponentPrototype) {
                ((DirectionComponentPrototype) cp).setValue(event.getDirection());
            }
        }

        Entity entity = entityFactory.createEntity(prototype, level, event.getPosition());
        level.getEngine().addEntity(entity);
    }

    @Subscribe
    public void handleDestroyEntityEvent(DestroyEntityEvent event) {
        destroyQueue.add(event.getEntity());
    }

    @Subscribe
    public void handleSpawnExplosionEvent(SpawnExplosionEvent event) {
        EntityPrototype prototype = repository.getEntityPrototype(event.getCode());
        Entity entity = entityFactory.createEntity(prototype, level, event.getPosition());
        level.getEngine().addEntity(entity);
    }

    public void update(float delta) {
        levelUpdater.update(level, delta, 1/60f, 6, 2);
        destroyEntities();
    }

    public void render(SpriteBatch spriteBatch, Camera camera, boolean debug) {
        levelRenderer.render(level, spriteBatch, camera, debug);
    }

    private void destroyEntities() {
        for (Entity entity : destroyQueue) {
            PhysicsComponent pc = entity.getComponent(PhysicsComponent.class);
            level.getWorld().destroyBody(pc.getBody());
            level.getEngine().removeEntity(entity);
        }

        destroyQueue.clear();
    }


}
