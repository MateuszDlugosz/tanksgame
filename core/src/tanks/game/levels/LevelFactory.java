package tanks.game.levels;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.google.common.eventbus.EventBus;
import tanks.game.assets.AssetStorage;
import tanks.game.levels.entities.EntityFactory;
import tanks.game.levels.entities.EntityPrototypeRepository;
import tanks.game.levels.entities.constacts.EntityContactListener;
import tanks.game.levels.entities.systems.*;
import tanks.game.physics.worlds.WorldFactory;
import tanks.game.utils.properties.MapPropertiesConverter;
import tanks.game.utils.properties.Properties;
import tanks.game.utils.scales.DividingScale;
import tanks.game.utils.scales.Scale;

public class LevelFactory {

    public static final String SCALE_PROPERTY = "scale";
    public static final float SCALE_PROPERTY_DEFAULT_VALUE = 32;

    public static final String ENTITIES_LAYER_NAME = "entities";

    private final MapPropertiesConverter mapPropertiesConverter;
    private final WorldFactory worldFactory;
    private final EntityFactory entityFactory;

    public LevelFactory(MapPropertiesConverter mapPropertiesConverter, WorldFactory worldFactory, EntityFactory entityFactory) {
        this.mapPropertiesConverter = mapPropertiesConverter;
        this.worldFactory = worldFactory;
        this.entityFactory = entityFactory;
    }

    public Level createLevel(LevelPrototype prototype,
                             SpriteBatch spriteBatch,
                             EntityPrototypeRepository repository,
                             EventBus eventBus)
    {
        TiledMap tiledMap = createTiledMap(prototype.getMapFilename());
        Properties properties = mapPropertiesConverter.convert(tiledMap.getProperties());
        Scale scale = createScale(properties);

        LevelBuilder builder = new LevelBuilder();
        builder.setScale(scale)
                .setAssetStorage(new AssetStorage(new AssetManager()))
                .setEngine(new Engine())
                .setWorld(worldFactory.createWorld(properties))
                .setEventBus(eventBus);

        Level level = builder.build();
        level.getWorld().setContactListener(new EntityContactListener());

        createEntities(tiledMap, repository, level);

        level.getEngine().addSystem(new ViewUpdateSystemFactory().createEntitySystem(level));
        level.getEngine().addSystem(new AISystemFactory().createEntitySystem(level));
        level.getEngine().addSystem(new BulletSystemFactory().createEntitySystem(level));
        level.getEngine().addSystem(new DestroySystemFactory().createEntitySystem(level));
        level.getEngine().addSystem(new LifetimeSystemFactory().createEntitySystem(level));

        return level;
    }

    private TiledMap createTiledMap(String filename) {
        return new TmxMapLoader().load(filename);
    }

    private Scale createScale(Properties properties) {
        return new DividingScale(properties.getFloat(
                SCALE_PROPERTY,
                SCALE_PROPERTY_DEFAULT_VALUE
        ));
    }

    private void createEntities(TiledMap tiledMap, EntityPrototypeRepository repository, Level level) {
        MapObjects mapObjects = tiledMap.getLayers().get(ENTITIES_LAYER_NAME).getObjects();

        for (MapObject mapObject : mapObjects) {
            Entity entity = entityFactory.createEntity(repository, level, mapObject);
            level.getEngine().addEntity(entity);
        }
    }

}
