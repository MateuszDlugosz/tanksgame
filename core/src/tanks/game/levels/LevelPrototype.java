package tanks.game.levels;

import com.google.common.base.MoreObjects;

import java.io.Serializable;

public class LevelPrototype implements Serializable {

    private String mapFilename;

    public String getMapFilename() {
        return mapFilename;
    }

    public void setMapFilename(String mapFilename) {
        this.mapFilename = mapFilename;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("mapFilename", mapFilename)
                .toString();
    }

}
