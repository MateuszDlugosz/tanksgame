package tanks.game.levels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.XmlReader;

import java.io.IOException;

public class LevelPrototypeXmlLoader {

    public static final String MAP_FILENAME_ELEMENT = "mapFilename";

    public LevelPrototype loadLevelPrototype(String filename) {
        LevelPrototype prototype = new LevelPrototype();

        try {
            XmlReader xmlReader = new XmlReader();
            XmlReader.Element element = null;
            element = xmlReader.parse(Gdx.files.internal(filename));

            prototype.setMapFilename(element.getChildByName(MAP_FILENAME_ELEMENT).getText());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return prototype;
    }

}
