package tanks.game.levels.entities;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.Body;
import tanks.game.levels.Level;
import tanks.game.levels.entities.components.ComponentFactory;
import tanks.game.levels.entities.components.PhysicsComponent;
import tanks.game.utils.positions.Position;
import tanks.game.utils.positions.PositionFactory;
import tanks.game.utils.positions.PositionPrototype;
import tanks.game.utils.positions.PositionPrototypeMapObjectLoader;

import java.util.List;

public class EntityFactory {

    public static final String PROTOTYPE_CODE_PROPERTY = "prototype_code";

    private final ComponentFactory componentFactory;
    private final PositionPrototypeMapObjectLoader positionPrototypeMapObjectLoader;
    private final PositionFactory positionFactory;

    public EntityFactory(ComponentFactory componentFactory,
                         PositionPrototypeMapObjectLoader positionPrototypeMapObjectLoader,
                         PositionFactory positionFactory)
    {
        this.componentFactory = componentFactory;
        this.positionPrototypeMapObjectLoader = positionPrototypeMapObjectLoader;
        this.positionFactory = positionFactory;
    }

    public Entity createEntity(EntityPrototypeRepository repository, Level level, MapObject mapObject) {
        PositionPrototype positionPrototype = positionPrototypeMapObjectLoader.loadPositionPrototype(mapObject);
        Position position = positionFactory.createPosition(positionPrototype, level.getScale());
        String prototypeCode = mapObject.getProperties().get(PROTOTYPE_CODE_PROPERTY, String.class);

        return createEntity(repository.getEntityPrototype(prototypeCode), level, position);
    }

    public Entity createEntity(EntityPrototype prototype, Level level, Position position) {
        Entity entity = new Entity();
        List<Component> components = componentFactory.createComponents(prototype.getComponentPrototypes(), level, entity);

        for (Component component : components) {
            if (component instanceof PhysicsComponent) {
                Body body = ((PhysicsComponent) component).getBody();
                body.setTransform(position.getX(), position.getY(), body.getAngle());
            }

            entity.add(component);
        }

        return entity;
    }

}
