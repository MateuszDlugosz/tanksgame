package tanks.game.levels.entities;

import com.google.common.base.MoreObjects;
import tanks.game.levels.entities.components.ComponentPrototype;

import java.io.Serializable;
import java.util.List;

public class EntityPrototype implements Serializable {

    private List<ComponentPrototype> componentPrototypes;

    public List<ComponentPrototype> getComponentPrototypes() {
        return componentPrototypes;
    }

    public void setComponentPrototypes(List<ComponentPrototype> componentPrototypes) {
        this.componentPrototypes = componentPrototypes;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("componentPrototypes", componentPrototypes)
                .toString();
    }

}
