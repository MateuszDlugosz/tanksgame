package tanks.game.levels.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.XmlReader;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EntityPrototypeRepository {

    private final Map<String, String> paths;
    private final Map<String, EntityPrototype> prototypes;

    private final EntityPrototypeXmlLoader entityPrototypeXmlLoader;

    public EntityPrototypeRepository(Map<String, String> paths,
                                     EntityPrototypeXmlLoader entityPrototypeXmlLoader)
    {
        this.paths = paths;
        this.entityPrototypeXmlLoader = entityPrototypeXmlLoader;
        this.prototypes = new HashMap<String, EntityPrototype>();
    }

    public void preloadEntityPrototypes(List<String> codeList) {
        for (String code : codeList) {
            loadEntityPrototype(code);
        }
    }

    private void loadEntityPrototype(String code) {
        XmlReader xmlReader = new XmlReader();
        try {
            XmlReader.Element root = xmlReader.parse(Gdx.files.internal(paths.get(code)));
            EntityPrototype prototype = entityPrototypeXmlLoader.loadEntityPrototype(root);
            prototypes.put(code, prototype);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public EntityPrototype getEntityPrototype(String code) {
        //if (!hasEntityPrototype(code)) {
        loadEntityPrototype(code);
        //}

        return prototypes.get(code);
    }

    private boolean hasEntityPrototype(String code) {
        return prototypes.containsKey(code);
    }


}
