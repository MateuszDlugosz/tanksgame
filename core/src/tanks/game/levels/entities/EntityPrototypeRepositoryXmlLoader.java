package tanks.game.levels.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class EntityPrototypeRepositoryXmlLoader {

    public static final String PROTOTYPE_ELEMENT = "prototype";
    public static final String CODE_ATTRIBUTE = "code";

    public EntityPrototypeRepository loadEntityPrototypeRepository(String xmlMapPath, EntityPrototypeXmlLoader entityPrototypeXmlLoader) {
        XmlReader xmlReader = new XmlReader();
        XmlReader.Element root = null;
        try {
            root = xmlReader.parse(Gdx.files.internal(xmlMapPath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new EntityPrototypeRepository(
                loadPrototypeMap(root.getChildrenByName(PROTOTYPE_ELEMENT)),
                entityPrototypeXmlLoader
        );
    }

    private Map<String, String> loadPrototypeMap(Array<XmlReader.Element> elements) {
        Map<String, String> prototypes = new HashMap<String, String>();

        for (XmlReader.Element element : elements) {
            prototypes.put(
                    element.getAttribute(CODE_ATTRIBUTE),
                    element.getText().trim()
            );
        }

        return prototypes;
    }

}
