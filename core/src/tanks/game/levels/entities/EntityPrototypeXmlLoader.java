package tanks.game.levels.entities;

import com.badlogic.gdx.utils.XmlReader;
import tanks.game.levels.entities.components.ComponentPrototypeXmlLoader;

public class EntityPrototypeXmlLoader {

    public static final String TYPE_ATTRIBUTE = "type";

    private final ComponentPrototypeXmlLoader componentPrototypeXmlLoader;

    public EntityPrototypeXmlLoader(ComponentPrototypeXmlLoader componentPrototypeXmlLoader) {
        this.componentPrototypeXmlLoader = componentPrototypeXmlLoader;
    }

    public EntityPrototype loadEntityPrototype(XmlReader.Element element) {
        EntityPrototype prototype = new EntityPrototype();
        prototype.setComponentPrototypes(
                componentPrototypeXmlLoader.loadComponentPrototypes(
                        element.getChildByName(ComponentPrototypeXmlLoader.COMPONENTS_ELEMENT)
                            .getChildrenByName(ComponentPrototypeXmlLoader.COMPONENT_ELEMENT)
                )
        );

        return prototype;
    }

}
