package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Component;

public class AIComponent implements Component {

    private float directionMinFrequency;
    private float directionMaxFrequency;
    private float directionTime;
    private float directionTimer;

    private float shootMinFrequency;
    private float shootMaxFrequency;
    private float shootTime;
    private float shootTimer;

    public float getDirectionMinFrequency() {
        return directionMinFrequency;
    }

    public void setDirectionMinFrequency(float directionMinFrequency) {
        this.directionMinFrequency = directionMinFrequency;
    }

    public float getDirectionMaxFrequency() {
        return directionMaxFrequency;
    }

    public void setDirectionMaxFrequency(float directionMaxFrequency) {
        this.directionMaxFrequency = directionMaxFrequency;
    }

    public float getDirectionTime() {
        return directionTime;
    }

    public void setDirectionTime(float directionTime) {
        this.directionTime = directionTime;
    }

    public float getDirectionTimer() {
        return directionTimer;
    }

    public void setDirectionTimer(float directionTimer) {
        this.directionTimer = directionTimer;
    }

    public float getShootMinFrequency() {
        return shootMinFrequency;
    }

    public void setShootMinFrequency(float shootMinFrequency) {
        this.shootMinFrequency = shootMinFrequency;
    }

    public float getShootMaxFrequency() {
        return shootMaxFrequency;
    }

    public void setShootMaxFrequency(float shootMaxFrequency) {
        this.shootMaxFrequency = shootMaxFrequency;
    }

    public float getShootTime() {
        return shootTime;
    }

    public void setShootTime(float shootTime) {
        this.shootTime = shootTime;
    }

    public float getShootTimer() {
        return shootTimer;
    }

    public void setShootTimer(float shootTimer) {
        this.shootTimer = shootTimer;
    }

}
