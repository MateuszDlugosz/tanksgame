package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Entity;
import tanks.game.levels.Level;

@RegistrableComponentFactory(registerAs = AIComponentPrototype.class)
public class AIComponentFactory implements ConcreteComponentFactory<AIComponent, AIComponentPrototype> {

    @Override
    public AIComponent createComponent(AIComponentPrototype prototype, Level level, Entity entity) {
        AIComponent component = new AIComponent();
        component.setDirectionMinFrequency(prototype.getDirectionMinFrequency());
        component.setDirectionMaxFrequency(prototype.getDirectionMaxFrequency());
        component.setDirectionTime(prototype.getDirectionTime());
        component.setDirectionTimer(0);
        component.setShootMinFrequency(prototype.getShootMinFrequency());
        component.setShootMaxFrequency(prototype.getShootMaxFrequency());
        component.setShootTime(prototype.getShootTime());
        component.setShootTimer(0);

        return component;
    }

}
