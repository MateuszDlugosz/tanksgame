package tanks.game.levels.entities.components;

import com.google.common.base.MoreObjects;

public class AIComponentPrototype implements ComponentPrototype {

    private float directionMinFrequency;
    private float directionMaxFrequency;
    private float directionTime;
    private float shootMinFrequency;
    private float shootMaxFrequency;
    private float shootTime;

    public float getDirectionMinFrequency() {
        return directionMinFrequency;
    }

    public void setDirectionMinFrequency(float directionMinFrequency) {
        this.directionMinFrequency = directionMinFrequency;
    }

    public float getDirectionMaxFrequency() {
        return directionMaxFrequency;
    }

    public void setDirectionMaxFrequency(float directionMaxFrequency) {
        this.directionMaxFrequency = directionMaxFrequency;
    }

    public float getDirectionTime() {
        return directionTime;
    }

    public void setDirectionTime(float directionTime) {
        this.directionTime = directionTime;
    }

    public float getShootMinFrequency() {
        return shootMinFrequency;
    }

    public void setShootMinFrequency(float shootMinFrequency) {
        this.shootMinFrequency = shootMinFrequency;
    }

    public float getShootMaxFrequency() {
        return shootMaxFrequency;
    }

    public void setShootMaxFrequency(float shootMaxFrequency) {
        this.shootMaxFrequency = shootMaxFrequency;
    }

    public float getShootTime() {
        return shootTime;
    }

    public void setShootTime(float shootTime) {
        this.shootTime = shootTime;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("directionMinFrequency", directionMinFrequency)
                .add("directionMaxFrequency", directionMaxFrequency)
                .add("directionTime", directionTime)
                .add("shootMinFrequency", shootMinFrequency)
                .add("shootMaxFrequency", shootMaxFrequency)
                .add("shootTime", shootTime)
                .toString();
    }

}
