package tanks.game.levels.entities.components;

import com.badlogic.gdx.utils.XmlReader;

import java.util.Random;

@RegistrableComponentPrototypeXmlLoader(registerAs = "AIComponent")
public class AIComponentPrototypeXmlLoader implements ConcreteComponentPrototypeXmlLoader<AIComponentPrototype> {

    public static final String DIRECTION_MIN_FREQUENCY_ELEMENT = "directionMinFrequency";
    public static final String DIRECTION_MAX_FREQUENCY_ELEMENT = "directionMaxFrequency";
    public static final String SHOOT_MIN_FREQUENCY = "shootMinFrequency";
    public static final String SHOOT_MAX_FREQUENCY = "shootMaxFrequency";

    @Override
    public AIComponentPrototype loadComponentPrototype(XmlReader.Element element) {
        AIComponentPrototype prototype = new AIComponentPrototype();
        prototype.setDirectionMinFrequency(Float.valueOf(element.getChildByName(DIRECTION_MIN_FREQUENCY_ELEMENT).getText()));
        prototype.setDirectionMaxFrequency(Float.valueOf(element.getChildByName(DIRECTION_MAX_FREQUENCY_ELEMENT).getText()));
        prototype.setDirectionTime(
                new Random().nextFloat()
                        * (prototype.getDirectionMaxFrequency()
                            - prototype.getDirectionMinFrequency())
                        + prototype.getDirectionMinFrequency()
        );
        prototype.setShootMinFrequency(Float.valueOf(element.getChildByName(SHOOT_MIN_FREQUENCY).getText()));
        prototype.setShootMaxFrequency(Float.valueOf(element.getChildByName(SHOOT_MAX_FREQUENCY).getText()));
        prototype.setShootTime(
                new Random().nextFloat()
                        * (prototype.getShootMaxFrequency()
                        - prototype.getShootMinFrequency())
                        + prototype.getShootMinFrequency()
        );

        return prototype;
    }

}
