package tanks.game.levels.entities.components;

public class AnnotationNotFoundException extends RuntimeException {

    public AnnotationNotFoundException(String message) {
        super(message);
    }

}
