package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Component;

public class BulletComponent implements Component {

    private float power;

    public float getPower() {
        return power;
    }

    public void setPower(float power) {
        this.power = power;
    }

}
