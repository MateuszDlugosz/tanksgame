package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Entity;
import tanks.game.levels.Level;

@RegistrableComponentFactory(registerAs = BulletComponentPrototype.class)
public class BulletComponentFactory implements ConcreteComponentFactory<BulletComponent, BulletComponentPrototype> {

    @Override
    public BulletComponent createComponent(BulletComponentPrototype prototype, Level level, Entity entity) {
        BulletComponent component = new BulletComponent();
        component.setPower(prototype.getPower());

        return component;
    }

}
