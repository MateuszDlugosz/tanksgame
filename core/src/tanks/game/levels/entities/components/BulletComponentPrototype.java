package tanks.game.levels.entities.components;

import com.google.common.base.MoreObjects;

public class BulletComponentPrototype implements ComponentPrototype {

    private float power;

    public float getPower() {
        return power;
    }

    public void setPower(float power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("power", power)
                .toString();
    }

}
