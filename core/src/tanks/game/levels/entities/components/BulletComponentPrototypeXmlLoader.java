package tanks.game.levels.entities.components;

import com.badlogic.gdx.utils.XmlReader;

@RegistrableComponentPrototypeXmlLoader(registerAs = "BulletComponent")
public class BulletComponentPrototypeXmlLoader implements ConcreteComponentPrototypeXmlLoader<BulletComponentPrototype> {

    public static final String POWER_ELEMENT = "power";

    @Override
    public BulletComponentPrototype loadComponentPrototype(XmlReader.Element element) {
        BulletComponentPrototype prototype = new BulletComponentPrototype();
        prototype.setPower(Float.valueOf(element.getChildByName(POWER_ELEMENT).getText()));

        return prototype;
    }

}
