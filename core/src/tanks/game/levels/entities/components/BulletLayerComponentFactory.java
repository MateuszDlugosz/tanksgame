package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Entity;
import tanks.game.levels.Level;

@RegistrableComponentFactory(registerAs = BulletLayerComponentPrototype.class)
public class BulletLayerComponentFactory implements ConcreteComponentFactory<BulletLayerComponent, BulletLayerComponentPrototype> {

    @Override
    public BulletLayerComponent createComponent(BulletLayerComponentPrototype prototype, Level level, Entity entity) {
        return new BulletLayerComponent();
    }

}
