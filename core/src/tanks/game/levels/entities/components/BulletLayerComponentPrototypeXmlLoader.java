package tanks.game.levels.entities.components;

import com.badlogic.gdx.utils.XmlReader;

@RegistrableComponentPrototypeXmlLoader(registerAs = "BulletLayerComponent")
public class BulletLayerComponentPrototypeXmlLoader implements ConcreteComponentPrototypeXmlLoader<BulletLayerComponentPrototype> {

    @Override
    public BulletLayerComponentPrototype loadComponentPrototype(XmlReader.Element element) {
        return new BulletLayerComponentPrototype();
    }

}
