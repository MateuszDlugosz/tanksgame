package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import tanks.game.levels.Level;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ComponentFactory {

    private final Map<Class<? extends ComponentPrototype>, ConcreteComponentFactory> innerFactories;

    public ComponentFactory(ConcreteComponentFactory[] factories) {
        innerFactories = new HashMap<Class<? extends ComponentPrototype>, ConcreteComponentFactory>();

        for (ConcreteComponentFactory factory : factories) {
            RegistrableComponentFactory annotation = factory.getClass().getAnnotation(RegistrableComponentFactory.class);
            if (annotation == null) {
                throw new AnnotationNotFoundException(String.format("Annotation %s not found in class %s.",
                        RegistrableComponentFactory.class, factory.getClass()));
            }

            innerFactories.put(annotation.registerAs(), factory);
        }
    }

    public Component createComponent(ComponentPrototype prototype, Level level, Entity entity) {
        Class<? extends ComponentPrototype> type = prototype.getClass();

        if (innerFactories.containsKey(type)) {
            return innerFactories.get(type).createComponent(prototype, level, entity);
        }
        else {
            throw new FactoryNotFoundException(String.format("Factory of type %s not found.", type));
        }
    }

    public List<Component> createComponents(List<ComponentPrototype> prototypes, Level level, Entity entity) {
        List<Component> components = new ArrayList<Component>();

        for (ComponentPrototype prototype : prototypes) {
            components.add(createComponent(prototype, level, entity));
        }

        return components;
    }

}
