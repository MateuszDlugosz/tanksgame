package tanks.game.levels.entities.components;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ComponentPrototypeXmlLoader {

    public static final String COMPONENTS_ELEMENT = "components";
    public static final String COMPONENT_ELEMENT = "component";
    public static final String TYPE_ATTRIBUTE = "type";

    private final Map<String, ConcreteComponentPrototypeXmlLoader> innerLoaders;

    public ComponentPrototypeXmlLoader(ConcreteComponentPrototypeXmlLoader[] loaders) {
        innerLoaders = new HashMap<String, ConcreteComponentPrototypeXmlLoader>();

        for (ConcreteComponentPrototypeXmlLoader loader : loaders) {
            RegistrableComponentPrototypeXmlLoader annotation
                    = loader.getClass().getAnnotation(RegistrableComponentPrototypeXmlLoader.class);

            if (annotation == null) {
                throw new AnnotationNotFoundException(String.format("Annotation %s not found in class %s.",
                        RegistrableComponentPrototypeXmlLoader.class, loader.getClass()));
            }

            innerLoaders.put(annotation.registerAs(), loader);
        }
    }

    public ComponentPrototype loadComponentPrototype(XmlReader.Element element) {
        String type = element.getAttribute(TYPE_ATTRIBUTE);

        if (innerLoaders.containsKey(type)) {
            return innerLoaders.get(type).loadComponentPrototype(element);
        }
        else {
            throw new LoaderNotFoundException(String.format("Prototype loader of type %s not found.", type));
        }
    }

    public List<ComponentPrototype> loadComponentPrototypes(Array<XmlReader.Element> elements) {
        List<ComponentPrototype> prototypes = new ArrayList<ComponentPrototype>();

        for (XmlReader.Element element : elements) {
            prototypes.add(loadComponentPrototype(element));
        }

        return prototypes;
    }

}
