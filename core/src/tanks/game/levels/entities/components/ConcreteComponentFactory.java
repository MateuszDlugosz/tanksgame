package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import tanks.game.levels.Level;

public interface ConcreteComponentFactory<T extends Component, U extends ComponentPrototype> {

    T createComponent(U prototype, Level level, Entity entity);

}
