package tanks.game.levels.entities.components;

import com.badlogic.gdx.utils.XmlReader;

public interface ConcreteComponentPrototypeXmlLoader<T extends ComponentPrototype> {

    T loadComponentPrototype(XmlReader.Element element);

}
