package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Entity;
import tanks.game.levels.Level;

@RegistrableComponentFactory(registerAs = DestroyedComponentPrototype.class)
public class DestroyedComponentFactory implements ConcreteComponentFactory<DestroyedComponent, DestroyedComponentPrototype> {

    @Override
    public DestroyedComponent createComponent(DestroyedComponentPrototype prototype, Level level, Entity entity) {
        return new DestroyedComponent();
    }

}
