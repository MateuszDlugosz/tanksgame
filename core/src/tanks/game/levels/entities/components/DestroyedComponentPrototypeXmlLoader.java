package tanks.game.levels.entities.components;

import com.badlogic.gdx.utils.XmlReader;

@RegistrableComponentPrototypeXmlLoader(registerAs = "DestroyedComponent")
public class DestroyedComponentPrototypeXmlLoader implements ConcreteComponentPrototypeXmlLoader<DestroyedComponentPrototype> {

    @Override
    public DestroyedComponentPrototype loadComponentPrototype(XmlReader.Element element) {
        return new DestroyedComponentPrototype();
    }

}
