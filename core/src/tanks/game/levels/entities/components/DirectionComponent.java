package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Component;

public class DirectionComponent implements Component {

    public enum Direction {
        Left, Right, Up, Down
    }

    private Direction value;

    public Direction getValue() {
        return value;
    }

    public void setValue(Direction value) {
        this.value = value;
    }

}
