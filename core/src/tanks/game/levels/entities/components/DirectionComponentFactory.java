package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Entity;
import tanks.game.levels.Level;

@RegistrableComponentFactory(registerAs = DirectionComponentPrototype.class)
public class DirectionComponentFactory implements ConcreteComponentFactory<DirectionComponent, DirectionComponentPrototype> {

    @Override
    public DirectionComponent createComponent(DirectionComponentPrototype prototype, Level level, Entity entity) {
        DirectionComponent component = new DirectionComponent();
        component.setValue(prototype.getValue());

        return component;
    }

}
