package tanks.game.levels.entities.components;

import com.google.common.base.MoreObjects;

public class DirectionComponentPrototype implements ComponentPrototype {

    private DirectionComponent.Direction value;

    public DirectionComponent.Direction getValue() {
        return value;
    }

    public void setValue(DirectionComponent.Direction value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("value", value)
                .toString();
    }

}
