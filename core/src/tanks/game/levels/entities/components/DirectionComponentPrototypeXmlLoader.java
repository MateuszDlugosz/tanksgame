package tanks.game.levels.entities.components;

import com.badlogic.gdx.utils.XmlReader;

@RegistrableComponentPrototypeXmlLoader(registerAs = "DirectionComponent")
public class DirectionComponentPrototypeXmlLoader implements ConcreteComponentPrototypeXmlLoader<DirectionComponentPrototype> {

    public static final String VALUE_ELEMENT = "value";

    @Override
    public DirectionComponentPrototype loadComponentPrototype(XmlReader.Element element) {
        DirectionComponentPrototype prototype = new DirectionComponentPrototype();
        prototype.setValue(DirectionComponent.Direction.valueOf(element.getChildByName(VALUE_ELEMENT).getText()));

        return prototype;
    }

}
