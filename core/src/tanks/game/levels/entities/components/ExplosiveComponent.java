package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Component;

public class ExplosiveComponent implements Component {

    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
