package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Entity;
import tanks.game.levels.Level;

@RegistrableComponentFactory(registerAs = ExplosiveComponentPrototype.class)
public class ExplosiveComponentFactory implements ConcreteComponentFactory<ExplosiveComponent, ExplosiveComponentPrototype> {

    @Override
    public ExplosiveComponent createComponent(ExplosiveComponentPrototype prototype, Level level, Entity entity) {
        ExplosiveComponent component = new ExplosiveComponent();
        component.setCode(prototype.getCode());

        return component;
    }

}
