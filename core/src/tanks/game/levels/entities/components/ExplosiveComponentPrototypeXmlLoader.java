package tanks.game.levels.entities.components;

import com.badlogic.gdx.utils.XmlReader;

@RegistrableComponentPrototypeXmlLoader(registerAs = "ExplosiveComponent")
public class ExplosiveComponentPrototypeXmlLoader implements ConcreteComponentPrototypeXmlLoader<ExplosiveComponentPrototype> {

    public static final String CODE_ELEMENT = "code";

    @Override
    public ExplosiveComponentPrototype loadComponentPrototype(XmlReader.Element element) {
        ExplosiveComponentPrototype prototype = new ExplosiveComponentPrototype();
        prototype.setCode(element.getChildByName(CODE_ELEMENT).getText());

        return prototype;
    }

}
