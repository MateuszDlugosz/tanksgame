package tanks.game.levels.entities.components;

public class FactoryNotFoundException extends RuntimeException {

    public FactoryNotFoundException(String message) {
        super(message);
    }

}
