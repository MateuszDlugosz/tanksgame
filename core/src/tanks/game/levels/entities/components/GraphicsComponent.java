package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Component;
import tanks.game.graphics.effects.EffectList;
import tanks.game.graphics.views.View;

import java.util.Map;

public class GraphicsComponent implements Component {

    private Map<String, View> views;
    private String currentViewId;

    public Map<String, View> getViews() {
        return views;
    }

    public void setViews(Map<String, View> views) {
        this.views = views;
    }

    public String getCurrentViewId() {
        return currentViewId;
    }

    public void setCurrentViewId(String currentViewId) {
        this.currentViewId = currentViewId;
    }

}
