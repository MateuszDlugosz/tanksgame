package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Entity;
import tanks.game.graphics.views.ViewFactory;
import tanks.game.levels.Level;

@RegistrableComponentFactory(registerAs = GraphicsComponentPrototype.class)
public class GraphicsComponentFactory implements ConcreteComponentFactory<GraphicsComponent, GraphicsComponentPrototype> {

    private final ViewFactory viewFactory;

    public GraphicsComponentFactory(ViewFactory viewFactory) {
        this.viewFactory = viewFactory;
    }

    @Override
    public GraphicsComponent createComponent(GraphicsComponentPrototype prototype, Level level, Entity entity) {
        GraphicsComponent component = new GraphicsComponent();
        component.setCurrentViewId(prototype.getCurrentViewId());
        component.setViews(viewFactory.createViews(prototype.getViewPrototypes(), level.getAssetStorage(), level.getScale()));

        return component;
    }

}
