package tanks.game.levels.entities.components;

import com.google.common.base.MoreObjects;
import tanks.game.graphics.effects.EffectPrototype;
import tanks.game.graphics.views.ViewPrototype;

import java.util.List;

public class GraphicsComponentPrototype implements ComponentPrototype {

    private List<ViewPrototype> viewPrototypes;
    private String currentViewId;

    public List<ViewPrototype> getViewPrototypes() {
        return viewPrototypes;
    }

    public void setViewPrototypes(List<ViewPrototype> viewPrototypes) {
        this.viewPrototypes = viewPrototypes;
    }

    public String getCurrentViewId() {
        return currentViewId;
    }

    public void setCurrentViewId(String currentViewId) {
        this.currentViewId = currentViewId;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("viewPrototypes", viewPrototypes)
                .add("currentViewId", currentViewId)
                .toString();
    }

}
