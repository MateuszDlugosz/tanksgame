package tanks.game.levels.entities.components;

import com.badlogic.gdx.utils.XmlReader;
import tanks.game.graphics.effects.EffectPrototypeXmlLoader;
import tanks.game.graphics.views.ViewPrototypeXmlLoader;

@RegistrableComponentPrototypeXmlLoader(registerAs = "GraphicsComponent")
public class GraphicsComponentPrototypeXmlLoader implements ConcreteComponentPrototypeXmlLoader<GraphicsComponentPrototype> {

    public static final String CURRENT_VIEW_ID_ELEMENT = "currentViewId";

    private final ViewPrototypeXmlLoader viewPrototypeXmlLoader;

    public GraphicsComponentPrototypeXmlLoader(ViewPrototypeXmlLoader viewPrototypeXmlLoader) {
        this.viewPrototypeXmlLoader = viewPrototypeXmlLoader;
    }

    @Override
    public GraphicsComponentPrototype loadComponentPrototype(XmlReader.Element element) {
        GraphicsComponentPrototype prototype = new GraphicsComponentPrototype();
        prototype.setCurrentViewId(element.getChildByName(CURRENT_VIEW_ID_ELEMENT).getText());
        prototype.setViewPrototypes(
            viewPrototypeXmlLoader.loadViewPrototypes(
                    element.getChildByName(ViewPrototypeXmlLoader.VIEWS_ELEMENT)
                            .getChildrenByName(ViewPrototypeXmlLoader.VIEW_ELEMENT)
            )
        );

        return prototype;
    }

}
