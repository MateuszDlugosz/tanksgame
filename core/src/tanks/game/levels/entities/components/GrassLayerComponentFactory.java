package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Entity;
import tanks.game.levels.Level;

@RegistrableComponentFactory(registerAs = GrassLayerComponentPrototype.class)
public class GrassLayerComponentFactory implements ConcreteComponentFactory<GrassLayerComponent, GrassLayerComponentPrototype> {

    @Override
    public GrassLayerComponent createComponent(GrassLayerComponentPrototype prototype, Level level, Entity entity) {
        return new GrassLayerComponent();
    }

}
