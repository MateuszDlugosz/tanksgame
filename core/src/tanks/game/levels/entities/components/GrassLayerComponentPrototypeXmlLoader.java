package tanks.game.levels.entities.components;

import com.badlogic.gdx.utils.XmlReader;

@RegistrableComponentPrototypeXmlLoader(registerAs = "GrassLayerComponent")
public class GrassLayerComponentPrototypeXmlLoader implements ConcreteComponentPrototypeXmlLoader<GrassLayerComponentPrototype> {

    @Override
    public GrassLayerComponentPrototype loadComponentPrototype(XmlReader.Element element) {
        return new GrassLayerComponentPrototype();
    }

}
