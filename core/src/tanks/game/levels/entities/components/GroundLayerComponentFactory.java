package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Entity;
import tanks.game.levels.Level;

@RegistrableComponentFactory(registerAs = GroundLayerComponentPrototype.class)
public class GroundLayerComponentFactory implements ConcreteComponentFactory<GroundLayerComponent, GroundLayerComponentPrototype> {

    @Override
    public GroundLayerComponent createComponent(GroundLayerComponentPrototype prototype, Level level, Entity entity) {
        return new GroundLayerComponent();
    }

}
