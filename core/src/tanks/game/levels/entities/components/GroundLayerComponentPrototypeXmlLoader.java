package tanks.game.levels.entities.components;

import com.badlogic.gdx.utils.XmlReader;

@RegistrableComponentPrototypeXmlLoader(registerAs = "GroundLayerComponent")
public class GroundLayerComponentPrototypeXmlLoader implements ConcreteComponentPrototypeXmlLoader<GroundLayerComponentPrototype> {

    @Override
    public GroundLayerComponentPrototype loadComponentPrototype(XmlReader.Element element) {
        return new GroundLayerComponentPrototype();
    }

}
