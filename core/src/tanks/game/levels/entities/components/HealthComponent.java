package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Component;

public class HealthComponent implements Component {

    private float value;
    private float protection;

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public float getProtection() {
        return protection;
    }

    public void setProtection(float protection) {
        this.protection = protection;
    }

}
