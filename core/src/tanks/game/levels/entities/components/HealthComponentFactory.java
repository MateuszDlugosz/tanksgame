package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Entity;
import tanks.game.levels.Level;

@RegistrableComponentFactory(registerAs = HealthComponentPrototype.class)
public class HealthComponentFactory implements ConcreteComponentFactory<HealthComponent, HealthComponentPrototype> {

    @Override
    public HealthComponent createComponent(HealthComponentPrototype prototype, Level level, Entity entity) {
        HealthComponent component = new HealthComponent();
        component.setValue(prototype.getValue());
        component.setProtection(prototype.getProtection());

        return component;
    }

}
