package tanks.game.levels.entities.components;

import com.google.common.base.MoreObjects;

public class HealthComponentPrototype implements ComponentPrototype {

    private float value;
    private float protection;

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public float getProtection() {
        return protection;
    }

    public void setProtection(float protection) {
        this.protection = protection;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("value", value)
                .add("protection", protection)
                .toString();
    }

}
