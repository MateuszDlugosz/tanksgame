package tanks.game.levels.entities.components;

import com.badlogic.gdx.utils.XmlReader;

@RegistrableComponentPrototypeXmlLoader(registerAs = "HealthComponent")
public class HealthComponentPrototypeXmlLoader implements ConcreteComponentPrototypeXmlLoader<HealthComponentPrototype> {

    public static final String VALUE_ELEMENT = "value";
    public static final String PROTECTION_ELEMENT = "protection";

    @Override
    public HealthComponentPrototype loadComponentPrototype(XmlReader.Element element) {
        HealthComponentPrototype prototype = new HealthComponentPrototype();
        prototype.setValue(Float.valueOf(element.getChildByName(VALUE_ELEMENT).getText()));
        prototype.setProtection(Float.valueOf(element.getChildByName(PROTECTION_ELEMENT).getText()));

        return prototype;
    }

}
