package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Component;

public class LifetimeComponent implements Component {

    private float time;
    private float timer;

    public float getTime() {
        return time;
    }

    public void setTime(float time) {
        this.time = time;
    }

    public float getTimer() {
        return timer;
    }

    public void setTimer(float timer) {
        this.timer = timer;
    }

}
