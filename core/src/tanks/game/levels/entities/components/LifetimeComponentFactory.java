package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Entity;
import tanks.game.levels.Level;

@RegistrableComponentFactory(registerAs = LifetimeComponentPrototype.class)
public class LifetimeComponentFactory implements ConcreteComponentFactory<LifetimeComponent, LifetimeComponentPrototype> {

    @Override
    public LifetimeComponent createComponent(LifetimeComponentPrototype prototype, Level level, Entity entity) {
        LifetimeComponent component = new LifetimeComponent();
        component.setTime(prototype.getTime());
        component.setTimer(0);

        return component;
    }

}
