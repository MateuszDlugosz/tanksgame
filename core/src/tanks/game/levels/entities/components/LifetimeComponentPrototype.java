package tanks.game.levels.entities.components;

import com.google.common.base.MoreObjects;

public class LifetimeComponentPrototype implements ComponentPrototype {

    private float time;

    public float getTime() {
        return time;
    }

    public void setTime(float time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("time", time)
                .toString();
    }

}
