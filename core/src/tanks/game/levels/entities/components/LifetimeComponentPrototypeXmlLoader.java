package tanks.game.levels.entities.components;

import com.badlogic.gdx.utils.XmlReader;

@RegistrableComponentPrototypeXmlLoader(registerAs = "LifetimeComponent")
public class LifetimeComponentPrototypeXmlLoader implements ConcreteComponentPrototypeXmlLoader<LifetimeComponentPrototype> {

    public static final String TIME_ELEMENT = "time";

    @Override
    public LifetimeComponentPrototype loadComponentPrototype(XmlReader.Element element) {
        LifetimeComponentPrototype prototype = new LifetimeComponentPrototype();
        prototype.setTime(Float.valueOf(element.getChildByName(TIME_ELEMENT).getText()));

        return prototype;
    }

}
