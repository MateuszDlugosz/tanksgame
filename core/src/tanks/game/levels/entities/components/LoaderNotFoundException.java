package tanks.game.levels.entities.components;

public class LoaderNotFoundException extends RuntimeException {

    public LoaderNotFoundException(String message) {
        super(message);
    }

}
