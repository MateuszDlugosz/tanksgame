package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Entity;
import tanks.game.levels.Level;

@RegistrableComponentFactory(registerAs = ObstacleLayerComponentPrototype.class)
public class ObstacleLayerComponentFactory implements ConcreteComponentFactory<ObstacleLayerComponent, ObstacleLayerComponentPrototype> {

    @Override
    public ObstacleLayerComponent createComponent(ObstacleLayerComponentPrototype prototype, Level level, Entity entity) {
        return new ObstacleLayerComponent();
    }

}
