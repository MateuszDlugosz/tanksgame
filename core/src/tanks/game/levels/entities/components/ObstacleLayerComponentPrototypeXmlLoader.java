package tanks.game.levels.entities.components;

import com.badlogic.gdx.utils.XmlReader;

@RegistrableComponentPrototypeXmlLoader(registerAs = "ObstacleLayerComponent")
public class ObstacleLayerComponentPrototypeXmlLoader implements ConcreteComponentPrototypeXmlLoader<ObstacleLayerComponentPrototype> {

    @Override
    public ObstacleLayerComponentPrototype loadComponentPrototype(XmlReader.Element element) {
        return new ObstacleLayerComponentPrototype();
    }

}
