package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import tanks.game.levels.entities.constacts.Sensor;

import java.util.Map;

public class PhysicsComponent implements Component {

    private Body body;
    private Map<String, Fixture> fixtures;
    private Map<String, Sensor> sensors;

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Map<String, Fixture> getFixtures() {
        return fixtures;
    }

    public void setFixtures(Map<String, Fixture> fixtures) {
        this.fixtures = fixtures;
    }

    public Map<String, Sensor> getSensors() {
        return sensors;
    }

    public void setSensors(Map<String, Sensor> sensors) {
        this.sensors = sensors;
    }

}
