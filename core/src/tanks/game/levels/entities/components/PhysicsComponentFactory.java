package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import tanks.game.levels.Level;
import tanks.game.physics.bodies.BodyFactory;
import tanks.game.physics.fixtures.FixtureFactory;
import tanks.game.physics.fixtures.FixturePrototype;
import tanks.game.levels.entities.constacts.Sensor;
import tanks.game.utils.scales.Scale;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RegistrableComponentFactory(registerAs = PhysicsComponentPrototype.class)
public class PhysicsComponentFactory implements ConcreteComponentFactory<PhysicsComponent, PhysicsComponentPrototype> {

    private final BodyFactory bodyFactory;
    private final FixtureFactory fixtureFactory;

    public PhysicsComponentFactory(BodyFactory bodyFactory, FixtureFactory fixtureFactory) {
        this.bodyFactory = bodyFactory;
        this.fixtureFactory = fixtureFactory;
    }

    @Override
    public PhysicsComponent createComponent(PhysicsComponentPrototype prototype, Level level, Entity entity) {
        Body body = bodyFactory.createBody(prototype.getBodyPrototype(), level.getWorld(), level.getScale());
        body.setUserData(entity);

        PhysicsComponent component = new PhysicsComponent();
        component.setBody(body);
        component.setFixtures(loadFixtures(prototype.getFixturePrototypes(), body, level.getScale(), entity));
        component.setSensors(loadSensors(prototype.getFixturePrototypes(), body, level.getScale()));

        return component;
    }

    private Map<String, Fixture> loadFixtures(List<FixturePrototype> prototypes, Body body, Scale scale, Entity entity) {
        Map<String, Fixture> fixtures = new HashMap<String, Fixture>();

        for (FixturePrototype prototype : prototypes) {
            if (!prototype.isSensor()) {
                Fixture fixture = fixtureFactory.createFixture(prototype, body, scale);
                fixture.setUserData(entity);
                fixtures.put(prototype.getId(), fixture);
            }
        }

        return fixtures;
    }

    private Map<String, Sensor> loadSensors(List<FixturePrototype> prototypes, Body body, Scale scale) {
        Map<String, Sensor> sensors = new HashMap<String, Sensor>();

        for (FixturePrototype prototype : prototypes) {
            if (prototype.isSensor()) {
                Fixture fixture = fixtureFactory.createFixture(prototype, body, scale);
                Sensor sensor = new Sensor(prototype.getId(), fixture);
                fixture.setUserData(sensor);

                sensors.put(
                        prototype.getId(),
                        sensor
                );
            }
        }

        return sensors;
    }

}
