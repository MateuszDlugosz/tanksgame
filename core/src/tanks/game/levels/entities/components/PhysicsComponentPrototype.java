package tanks.game.levels.entities.components;

import com.google.common.base.MoreObjects;
import tanks.game.physics.bodies.BodyPrototype;
import tanks.game.physics.fixtures.FixturePrototype;

import java.util.List;

public class PhysicsComponentPrototype implements ComponentPrototype {

    private BodyPrototype bodyPrototype;
    private List<FixturePrototype> fixturePrototypes;

    public BodyPrototype getBodyPrototype() {
        return bodyPrototype;
    }

    public void setBodyPrototype(BodyPrototype bodyPrototype) {
        this.bodyPrototype = bodyPrototype;
    }

    public List<FixturePrototype> getFixturePrototypes() {
        return fixturePrototypes;
    }

    public void setFixturePrototypes(List<FixturePrototype> fixturePrototypes) {
        this.fixturePrototypes = fixturePrototypes;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("bodyPrototype", bodyPrototype)
                .add("fixturePrototypes", fixturePrototypes)
                .toString();
    }

}
