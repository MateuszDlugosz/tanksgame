package tanks.game.levels.entities.components;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import tanks.game.physics.bodies.BodyPrototypeXmlLoader;
import tanks.game.physics.fixtures.FixturePrototype;
import tanks.game.physics.fixtures.FixturePrototypeXmlLoader;

import java.util.ArrayList;
import java.util.List;

@RegistrableComponentPrototypeXmlLoader(registerAs = "PhysicsComponent")
public class PhysicsComponentPrototypeXmlLoader implements ConcreteComponentPrototypeXmlLoader<PhysicsComponentPrototype> {

    private final BodyPrototypeXmlLoader bodyPrototypeXmlLoader;
    private final FixturePrototypeXmlLoader fixturePrototypeXmlLoader;

    public PhysicsComponentPrototypeXmlLoader(BodyPrototypeXmlLoader bodyPrototypeXmlLoader,
                                              FixturePrototypeXmlLoader fixturePrototypeXmlLoader)
    {
        this.bodyPrototypeXmlLoader = bodyPrototypeXmlLoader;
        this.fixturePrototypeXmlLoader = fixturePrototypeXmlLoader;
    }

    @Override
    public PhysicsComponentPrototype loadComponentPrototype(XmlReader.Element element) {
        PhysicsComponentPrototype prototype = new PhysicsComponentPrototype();
        prototype.setBodyPrototype(
                bodyPrototypeXmlLoader.loadBodyPrototype(
                        element.getChildByName(BodyPrototypeXmlLoader.BODY_ELEMENT)
                )
        );
        prototype.setFixturePrototypes(
                loadFixturePrototypes(
                        element.getChildByName(FixturePrototypeXmlLoader.FIXTURES_ELEMENT)
                                .getChildrenByName(FixturePrototypeXmlLoader.FIXTURE_ELEMENT)
                )
        );

        return prototype;
    }

    private List<FixturePrototype> loadFixturePrototypes(Array<XmlReader.Element> elements) {
        List<FixturePrototype> prototypes = new ArrayList<FixturePrototype>();

        for (XmlReader.Element element : elements) {
            prototypes.add(fixturePrototypeXmlLoader.loadFixturePrototype(element));
        }

        return prototypes;
    }

}
