package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Entity;
import tanks.game.levels.Level;

@RegistrableComponentFactory(registerAs = PlayerInputComponentPrototype.class)
public class PlayerInputComponentFactory implements ConcreteComponentFactory<PlayerInputComponent, PlayerInputComponentPrototype> {

    @Override
    public PlayerInputComponent createComponent(PlayerInputComponentPrototype prototype, Level level, Entity entity) {
        return new PlayerInputComponent();
    }

}
