package tanks.game.levels.entities.components;

import com.badlogic.gdx.utils.XmlReader;

@RegistrableComponentPrototypeXmlLoader(registerAs = "PlayerInputComponent")
public class PlayerInputComponentPrototypeXmlLoader implements ConcreteComponentPrototypeXmlLoader<PlayerInputComponentPrototype> {

    @Override
    public PlayerInputComponentPrototype loadComponentPrototype(XmlReader.Element element) {
        return new PlayerInputComponentPrototype();
    }

}
