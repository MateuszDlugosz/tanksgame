package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Component;

public class SpeedComponent implements Component {

    private float value;

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

}
