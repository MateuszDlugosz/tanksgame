package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Entity;
import tanks.game.levels.Level;

@RegistrableComponentFactory(registerAs = SpeedComponentPrototype.class)
public class SpeedComponentFactory implements ConcreteComponentFactory<SpeedComponent, SpeedComponentPrototype> {

    @Override
    public SpeedComponent createComponent(SpeedComponentPrototype prototype, Level level, Entity entity) {
        SpeedComponent component = new SpeedComponent();
        component.setValue(prototype.getValue());

        return component;
    }

}
