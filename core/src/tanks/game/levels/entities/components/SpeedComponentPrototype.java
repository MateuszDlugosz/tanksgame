package tanks.game.levels.entities.components;

import com.google.common.base.MoreObjects;

public class SpeedComponentPrototype implements ComponentPrototype {

    private float value;

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("value", value)
                .toString();
    }

}
