package tanks.game.levels.entities.components;

import com.badlogic.gdx.utils.XmlReader;

@RegistrableComponentPrototypeXmlLoader(registerAs = "SpeedComponent")
public class SpeedComponentPrototypeXmlLoader implements ConcreteComponentPrototypeXmlLoader<SpeedComponentPrototype> {

    public static final String VALUE_ELEMENT = "value";

    @Override
    public SpeedComponentPrototype loadComponentPrototype(XmlReader.Element element) {
        SpeedComponentPrototype prototype = new SpeedComponentPrototype();
        prototype.setValue(Float.valueOf(element.getChildByName(VALUE_ELEMENT).getText()));

        return prototype;
    }

}
