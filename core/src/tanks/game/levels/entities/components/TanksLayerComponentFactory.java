package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Entity;
import tanks.game.levels.Level;

@RegistrableComponentFactory(registerAs = TanksLayerComponentPrototype.class)
public class TanksLayerComponentFactory implements ConcreteComponentFactory<TanksLayerComponent, TanksLayerComponentPrototype> {

    @Override
    public TanksLayerComponent createComponent(TanksLayerComponentPrototype prototype, Level level, Entity entity) {
        return new TanksLayerComponent();
    }

}
