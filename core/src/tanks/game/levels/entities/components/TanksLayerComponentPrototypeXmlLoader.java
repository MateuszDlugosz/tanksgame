package tanks.game.levels.entities.components;

import com.badlogic.gdx.utils.XmlReader;

@RegistrableComponentPrototypeXmlLoader(registerAs = "TanksLayerComponent")
public class TanksLayerComponentPrototypeXmlLoader implements ConcreteComponentPrototypeXmlLoader<TanksLayerComponentPrototype> {

    @Override
    public TanksLayerComponentPrototype loadComponentPrototype(XmlReader.Element element) {
        return new TanksLayerComponentPrototype();
    }

}
