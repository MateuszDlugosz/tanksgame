package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Component;

public class TiledGraphicsComponent implements Component {

    private float tileX;
    private float tileY;
    private String layerName;

    public float getTileX() {
        return tileX;
    }

    public void setTileX(float tileX) {
        this.tileX = tileX;
    }

    public float getTileY() {
        return tileY;
    }

    public void setTileY(float tileY) {
        this.tileY = tileY;
    }

    public String getLayerName() {
        return layerName;
    }

    public void setLayerName(String layerName) {
        this.layerName = layerName;
    }

}
