package tanks.game.levels.entities.components;

import com.badlogic.ashley.core.Entity;
import tanks.game.levels.Level;

@RegistrableComponentFactory(registerAs = TiledGraphicsComponentPrototype.class)
public class TiledGraphicsComponentFactory implements ConcreteComponentFactory<TiledGraphicsComponent, TiledGraphicsComponentPrototype> {

    @Override
    public TiledGraphicsComponent createComponent(TiledGraphicsComponentPrototype prototype, Level level, Entity entity) {
        TiledGraphicsComponent component = new TiledGraphicsComponent();
        component.setLayerName(prototype.getLayerName());
        component.setTileX(prototype.getTileX());
        component.setTileY(prototype.getTileY());

        return component;
    }

}
