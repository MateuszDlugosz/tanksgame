package tanks.game.levels.entities.constacts;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

public class EntityContactListener implements ContactListener {

    public enum ContactType {
        BeginContact,
        EndContact,
        PreSolveContact,
        PostSolveContact
    }

    @Override
    public void beginContact(Contact contact) {
        processContact(contact, ContactType.BeginContact);
    }

    @Override
    public void endContact(Contact contact) {
        processContact(contact, ContactType.EndContact);
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
        processContact(contact, ContactType.PreSolveContact);
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
        processContact(contact, ContactType.PostSolveContact);
    }

    private void processContact(Contact contact, ContactType contactType) {
        Object objA = contact.getFixtureA().getUserData();
        Object objB = contact.getFixtureB().getUserData();

        if (objA instanceof Sensor) {
            if (objB instanceof Entity) {
                ((Sensor) objA).contact((Entity) objB, contact, contactType);
            }
        }

        if (objB instanceof Sensor) {
            if (objA instanceof Entity) {
                ((Sensor) objB).contact((Entity) objA, contact, contactType);
            }
        }
    }

}
