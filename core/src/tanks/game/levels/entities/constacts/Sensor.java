package tanks.game.levels.entities.constacts;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.utils.Array;
import tanks.game.levels.entities.constacts.EntityContactListener;

public class Sensor {

    private final String id;
    private final Fixture fixture;
    private final Array<Entity> contacts;

    public Sensor(String id, Fixture fixture) {
        this.id = id;
        this.fixture = fixture;
        this.contacts = new Array<Entity>();
    }

    public String getId() {
        return id;
    }

    public Fixture getFixture() {
        return fixture;
    }

    public Array<Entity> getContacts() {
        return contacts;
    }

    public boolean isContacted() {
        return (contacts.size > 0);
    }

    public void contact(Entity entity, Contact contact, EntityContactListener.ContactType contactType) {
        if (contactType == EntityContactListener.ContactType.BeginContact) contacts.add(entity);
        if (contactType == EntityContactListener.ContactType.EndContact) contacts.removeValue(entity, true);
    }

}
