package tanks.game.levels.entities.renderers;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.BodyDef;
import tanks.game.graphics.views.renderers.ViewRenderer;
import tanks.game.levels.entities.components.GraphicsComponent;
import tanks.game.levels.entities.components.PhysicsComponent;
import tanks.game.utils.positions.Position;

import java.util.List;

public class EntityRenderer {

    private final ViewRenderer viewRenderer;

    public EntityRenderer(ViewRenderer viewRenderer) {
        this.viewRenderer = viewRenderer;
    }

    public void render(List<Entity> entities, SpriteBatch spriteBatch) {
        for (Entity entity : entities) {
            render(entity, spriteBatch);
        }
    }

    public void render(Entity entity, SpriteBatch spriteBatch) {
        GraphicsComponent graphicsComponent = entity.getComponent(GraphicsComponent.class);

        if (graphicsComponent.getCurrentViewId() != null) {
            PhysicsComponent physicsComponent = entity.getComponent(PhysicsComponent.class);
            float angle = physicsComponent.getBody().getAngle() * MathUtils.radiansToDegrees;
            Position position;

            if (physicsComponent.getBody().getType() == BodyDef.BodyType.DynamicBody) {
                position = new Position(
                        physicsComponent.getBody().getPosition().x,
                        physicsComponent.getBody().getPosition().y
                );
            }
            else {
                position = new Position(
                        physicsComponent.getBody().getWorldCenter().x,
                        physicsComponent.getBody().getWorldCenter().y
                );
            }

            viewRenderer.render(graphicsComponent.getViews().get(graphicsComponent.getCurrentViewId()), spriteBatch, position, angle);
        }
    }

}
