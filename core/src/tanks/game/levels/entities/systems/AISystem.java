package tanks.game.levels.entities.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.math.Vector2;
import com.google.common.collect.Lists;
import tanks.game.levels.Level;
import tanks.game.levels.entities.components.*;
import tanks.game.levels.entities.constacts.Sensor;
import tanks.game.levels.events.SpawnBulletEvent;
import tanks.game.utils.positions.Position;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public class AISystem extends EntityIteratingSystem {

    private Random random;

    public AISystem(Level level, Family family, int priority) {
        super(level, family, priority);
        this.random = new Random();
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        processEntityMove(entity, deltaTime);
        processEntityShoot(entity, deltaTime);
        processEntityGraphics(entity);
    }

    private void processEntityMove(Entity entity, float deltaTime) {
        PhysicsComponent pc = entity.getComponent(PhysicsComponent.class);
        AIComponent ai = entity.getComponent(AIComponent.class);
        SpeedComponent sc = entity.getComponent(SpeedComponent.class);
        DirectionComponent dc = entity.getComponent(DirectionComponent.class);

        List<DirectionComponent.Direction> dirs = getAllowedDirection(pc);

        if (ai.getDirectionTimer() >= ai.getDirectionTime()) {
            if (dirs.size() > 0) dc.setValue(dirs.get(random.nextInt(dirs.size())));
            ai.setDirectionTime(random.nextFloat()
                    * (ai.getDirectionMaxFrequency()
                    - ai.getDirectionMinFrequency())
                    + ai.getDirectionMinFrequency());

            ai.setDirectionTimer(0);
        } else {
            ai.setDirectionTimer(ai.getDirectionTimer() + deltaTime);
        }

        if (!pc.getSensors().get(dc.getValue().name()).isContacted()) {
            pc.getBody().setLinearVelocity(getVectorDirection(dc.getValue(), sc.getValue()));
        } else {
            pc.getBody().setLinearVelocity(0, 0);
        }
    }

    private void processEntityShoot(Entity entity, float deltaTime) {
        AIComponent ai = entity.getComponent(AIComponent.class);

        if (ai.getShootTimer() >= ai.getShootTime()) {
            PhysicsComponent pc = entity.getComponent(PhysicsComponent.class);
            DirectionComponent dc = entity.getComponent(DirectionComponent.class);

            ai.setShootTime(random.nextFloat()
                    * (ai.getShootMaxFrequency()
                    - ai.getShootMinFrequency())
                    + ai.getShootMinFrequency());

            getLevel().getEventBus().post(
                    new SpawnBulletEvent(
                            false,
                            new Position(pc.getBody().getPosition()),
                            dc.getValue())
            );

            ai.setShootTimer(0);
        } else {
            ai.setShootTimer(ai.getShootTimer() + deltaTime);
        }
    }

    private void processEntityGraphics(Entity entity) {
        DirectionComponent dc = entity.getComponent(DirectionComponent.class);
        GraphicsComponent gc = entity.getComponent(GraphicsComponent.class);

        gc.setCurrentViewId(dc.getValue().name());
    }

    private DirectionComponent.Direction computeDirection(PhysicsComponent pc) {
        return DirectionComponent.Direction.Left;
    }

    private Vector2 getVectorDirection(DirectionComponent.Direction direction, float speed) {
        switch (direction) {
            case Left: return new Vector2(-speed, 0);
            case Right: return new Vector2(speed, 0);
            case Up: return new Vector2(0, speed);
            case Down: return new Vector2(0, -speed);
        }

        return new Vector2(0, 0);
    }

    private List<DirectionComponent.Direction> getAllowedDirection(PhysicsComponent physicsComponent) {
        List<DirectionComponent.Direction> dirs = new ArrayList<DirectionComponent.Direction>();
        /*Collection<Sensor> sensors = physicsComponent.getSensors().values();

        for (Sensor sensor : sensors) {
            if (!sensor.isContacted()) dirs.add(DirectionComponent.Direction.valueOf(sensor.getId()));
        }
*/
        return Lists.newArrayList(DirectionComponent.Direction.values());
        //return dirs;
    }

}
