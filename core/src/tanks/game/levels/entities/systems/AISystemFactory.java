package tanks.game.levels.entities.systems;

import com.badlogic.ashley.core.Family;
import tanks.game.levels.Level;
import tanks.game.levels.entities.components.AIComponent;
import tanks.game.levels.entities.components.DirectionComponent;
import tanks.game.levels.entities.components.PhysicsComponent;
import tanks.game.levels.entities.components.SpeedComponent;

public class AISystemFactory implements ConcreteEntitySystemFactory<AISystem> {

    public static final Family FAMILY = Family.all(
            AIComponent.class,
            SpeedComponent.class,
            DirectionComponent.class,
            PhysicsComponent.class
    ).get();

    @Override
    public AISystem createEntitySystem(Level level) {
        return new AISystem(level, FAMILY, 1);
    }

}
