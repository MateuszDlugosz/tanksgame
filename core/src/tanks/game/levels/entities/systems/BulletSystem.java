package tanks.game.levels.entities.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.math.Vector2;
import tanks.game.levels.Level;
import tanks.game.levels.entities.components.*;
import tanks.game.levels.entities.constacts.Sensor;

public class BulletSystem extends EntityIteratingSystem {

    public BulletSystem(Level level, Family family, int priority) {
        super(level, family, priority);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        PhysicsComponent pc = entity.getComponent(PhysicsComponent.class);
        GraphicsComponent gc = entity.getComponent(GraphicsComponent.class);
        DirectionComponent dc = entity.getComponent(DirectionComponent.class);
        SpeedComponent sc = entity.getComponent(SpeedComponent.class);

        gc.setCurrentViewId(dc.getValue().name());
        pc.getBody().setLinearVelocity(getVectorDirection(dc.getValue(), sc.getValue()));

        processContacts(entity);
    }

    private Vector2 getVectorDirection(DirectionComponent.Direction direction, float speed) {
        switch (direction) {
            case Left: return new Vector2(-speed, 0);
            case Right: return new Vector2(speed, 0);
            case Up: return new Vector2(0, speed);
            case Down: return new Vector2(0, -speed);
        }

        return new Vector2(0, 0);
    }

    private void processContacts(Entity entity) {
        PhysicsComponent pc = entity.getComponent(PhysicsComponent.class);
        BulletComponent bc = entity.getComponent(BulletComponent.class);
        Sensor sensor = pc.getSensors().get("Base");

        if (sensor.isContacted()) {
            for (Entity cEnt : sensor.getContacts()) {
                if (cEnt.getComponent(HealthComponent.class) != null) {
                    HealthComponent hc = cEnt.getComponent(HealthComponent.class);
                    float protection = hc.getProtection();

                    if (protection - bc.getPower() < 0) {
                        hc.setValue(hc.getValue() - (bc.getPower() - protection));
                        if (hc.getValue() <= 0) {
                            cEnt.add(new DestroyedComponent());
                        }
                    }
                }
            }
            entity.add(new DestroyedComponent());
        }
    }

}
