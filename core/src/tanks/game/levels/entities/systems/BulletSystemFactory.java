package tanks.game.levels.entities.systems;

import com.badlogic.ashley.core.Family;
import tanks.game.levels.Level;
import tanks.game.levels.entities.components.BulletComponent;

public class BulletSystemFactory implements ConcreteEntitySystemFactory<BulletSystem> {

    public static final Family FAMILY = Family.all(BulletComponent.class).get();

    @Override
    public BulletSystem createEntitySystem(Level level) {
        return new BulletSystem(level, FAMILY, 2);
    }

}
