package tanks.game.levels.entities.systems;

import tanks.game.levels.Level;

public interface ConcreteEntitySystemFactory<T extends EntityIteratingSystem> {

    T createEntitySystem(Level level);

}
