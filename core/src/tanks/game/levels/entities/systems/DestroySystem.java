package tanks.game.levels.entities.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import tanks.game.levels.Level;
import tanks.game.levels.entities.components.ExplosiveComponent;
import tanks.game.levels.entities.components.PhysicsComponent;
import tanks.game.levels.events.DestroyEntityEvent;
import tanks.game.levels.events.SpawnExplosionEvent;
import tanks.game.utils.positions.Position;

public class DestroySystem extends EntityIteratingSystem {

    public DestroySystem(Level level, Family family, int priority) {
        super(level, family, priority);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        getLevel().getEventBus().post(new DestroyEntityEvent(entity));
        if (entity.getComponent(ExplosiveComponent.class) != null) {
            PhysicsComponent pc = entity.getComponent(PhysicsComponent.class);
            ExplosiveComponent ec = entity.getComponent(ExplosiveComponent.class);

            getLevel().getEventBus().post(new SpawnExplosionEvent(ec.getCode(), new Position(pc.getBody().getPosition())));
        }
    }

}
