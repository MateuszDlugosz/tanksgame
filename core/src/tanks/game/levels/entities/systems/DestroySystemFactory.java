package tanks.game.levels.entities.systems;

import com.badlogic.ashley.core.Family;
import tanks.game.levels.Level;
import tanks.game.levels.entities.components.DestroyedComponent;

public class DestroySystemFactory implements ConcreteEntitySystemFactory<DestroySystem> {

    public static final Family FAMILY = Family.all(DestroyedComponent.class).get();

    @Override
    public DestroySystem createEntitySystem(Level level) {
        return new DestroySystem(level, FAMILY, 100);
    }

}
