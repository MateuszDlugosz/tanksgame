package tanks.game.levels.entities.systems;

import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import tanks.game.levels.Level;

public abstract class EntityIteratingSystem extends IteratingSystem {

    private Level level;

    public EntityIteratingSystem(Level level, Family family, int priority) {
        super(family, priority);
        this.level = level;
    }

    public Level getLevel() {
        return level;
    }

}
