package tanks.game.levels.entities.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import tanks.game.levels.Level;
import tanks.game.levels.entities.components.DestroyedComponent;
import tanks.game.levels.entities.components.LifetimeComponent;

public class LifetimeSystem extends EntityIteratingSystem {

    public LifetimeSystem(Level level, Family family, int priority) {
        super(level, family, priority);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        LifetimeComponent lc = entity.getComponent(LifetimeComponent.class);

        if (lc.getTimer() >= lc.getTime()) {
            entity.add(new DestroyedComponent());
        } else {
            lc.setTimer(lc.getTimer() + deltaTime);
        }
    }

}
