package tanks.game.levels.entities.systems;

import com.badlogic.ashley.core.Family;
import tanks.game.levels.Level;
import tanks.game.levels.entities.components.LifetimeComponent;

public class LifetimeSystemFactory implements ConcreteEntitySystemFactory<LifetimeSystem> {

    public static final Family FAMILY = Family.all(LifetimeComponent.class).get();

    @Override
    public LifetimeSystem createEntitySystem(Level level) {
        return new LifetimeSystem(level, FAMILY, 2);
    }

}
