package tanks.game.levels.entities.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import tanks.game.graphics.views.UpdatableView;
import tanks.game.graphics.views.View;
import tanks.game.levels.Level;
import tanks.game.levels.entities.components.GraphicsComponent;

public class ViewUpdateSystem extends EntityIteratingSystem {

    public ViewUpdateSystem(Level level, Family family, int priority) {
        super(level, family, priority);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        GraphicsComponent gc = entity.getComponent(GraphicsComponent.class);
        View view = gc.getViews().get(gc.getCurrentViewId());

        if (view != null) {
            if (view instanceof UpdatableView) {
                ((UpdatableView) view).update(deltaTime);
            }
        }
    }

}
