package tanks.game.levels.entities.systems;

import com.badlogic.ashley.core.Family;
import tanks.game.levels.Level;
import tanks.game.levels.entities.components.GraphicsComponent;

public class ViewUpdateSystemFactory implements ConcreteEntitySystemFactory<ViewUpdateSystem> {

    public static final int PRIORITY = 1;
    public static final Family FAMILY = Family.all(GraphicsComponent.class).get();

    @Override
    public ViewUpdateSystem createEntitySystem(Level level) {
        return new ViewUpdateSystem(level, FAMILY, PRIORITY);
    }

}
