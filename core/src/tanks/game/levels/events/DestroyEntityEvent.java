package tanks.game.levels.events;

import com.badlogic.ashley.core.Entity;

public class DestroyEntityEvent {

    private final Entity entity;

    public DestroyEntityEvent(Entity entity) {
        this.entity = entity;
    }

    public Entity getEntity() {
        return entity;
    }

}
