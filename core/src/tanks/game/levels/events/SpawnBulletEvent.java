package tanks.game.levels.events;

import tanks.game.levels.entities.components.DirectionComponent;
import tanks.game.utils.positions.Position;

public class SpawnBulletEvent {

    private final boolean playerBullet;
    private final Position position;
    private final DirectionComponent.Direction direction;

    public SpawnBulletEvent(boolean playerBullet, Position position, DirectionComponent.Direction direction) {
        this.playerBullet = playerBullet;
        this.position = position;
        this.direction = direction;
    }

    public boolean isPlayerBullet() {
        return playerBullet;
    }

    public Position getPosition() {
        return position;
    }

    public DirectionComponent.Direction getDirection() {
        return direction;
    }

}
