package tanks.game.levels.events;

import tanks.game.utils.positions.Position;

public class SpawnExplosionEvent {

    private final String code;
    private final Position position;

    public SpawnExplosionEvent(String code, Position position) {
        this.code = code;
        this.position = position;
    }

    public String getCode() {
        return code;
    }

    public Position getPosition() {
        return position;
    }

}
