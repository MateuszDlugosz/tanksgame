package tanks.game.levels.renderers;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import tanks.game.levels.Level;
import tanks.game.levels.entities.components.*;
import tanks.game.levels.entities.renderers.EntityRenderer;
import tanks.game.physics.worlds.renderers.WorldRenderer;

public class LevelRenderer {

    public static final Family OBSTACLES = Family.all(
            PhysicsComponent.class,
            GraphicsComponent.class,
            ObstacleLayerComponent.class).get();
    public static final Family GRASS = Family.all(
            PhysicsComponent.class,
            GraphicsComponent.class,
            GrassLayerComponent.class).get();
    public static final Family ENEMY_TANKS = Family.all(
            PhysicsComponent.class,
            GraphicsComponent.class,
            TanksLayerComponent.class).get();
    public static final Family GROUND = Family.all(
            PhysicsComponent.class,
            GraphicsComponent.class,
            GroundLayerComponent.class).get();
    public static final Family BULLETS = Family.all(
            PhysicsComponent.class,
            GraphicsComponent.class,
            BulletLayerComponent.class).get();

    private final WorldRenderer worldRenderer;
    private final EntityRenderer entityRenderer;

    public LevelRenderer(WorldRenderer worldRenderer, EntityRenderer entityRenderer) {
        this.worldRenderer = worldRenderer;
        this.entityRenderer = entityRenderer;
    }

    public void render(Level level, SpriteBatch spriteBatch, Camera camera, boolean debug) {
        spriteBatch.setProjectionMatrix(camera.combined);

        renderEntities(level.getEngine().getEntitiesFor(GROUND), spriteBatch);
        renderEntities(level.getEngine().getEntitiesFor(OBSTACLES), spriteBatch);
        renderEntities(level.getEngine().getEntitiesFor(BULLETS), spriteBatch);
        renderEntities(level.getEngine().getEntitiesFor(ENEMY_TANKS), spriteBatch);
        renderEntities(level.getEngine().getEntitiesFor(GRASS), spriteBatch);

        if (debug) worldRenderer.render(level.getWorld(), camera);
    }

    private void renderEntities(ImmutableArray<Entity> entities, SpriteBatch spriteBatch) {
        for (Entity entity : entities) {
            entityRenderer.render(entity, spriteBatch);
        }
    }

}
