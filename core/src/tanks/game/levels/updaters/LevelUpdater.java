package tanks.game.levels.updaters;

import tanks.game.levels.Level;
import tanks.game.physics.worlds.updaters.WorldUpdater;

public class LevelUpdater {

    private final WorldUpdater worldUpdater;

    public LevelUpdater(WorldUpdater worldUpdater) {
        this.worldUpdater = worldUpdater;
    }

    public void update(Level level, float delta, float timeStep, int velocityIterations, int positionIterations) {
        worldUpdater.step(level.getWorld(), delta, timeStep, velocityIterations, positionIterations);
        level.getEngine().update(delta);
    }

}
