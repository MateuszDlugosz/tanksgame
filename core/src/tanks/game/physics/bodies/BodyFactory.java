package tanks.game.physics.bodies;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import tanks.game.utils.scales.Scale;

public class BodyFactory {

    public Body createBody(BodyPrototype prototype, World world, Scale scale) {
        return world.createBody(createBodyDef(prototype, scale));
    }

    public BodyDef createBodyDef(BodyPrototype prototype, Scale scale) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = prototype.getType();
        bodyDef.active = prototype.isActive();
        bodyDef.awake = prototype.isAwake();
        bodyDef.allowSleep = prototype.isAllowSleep();
        bodyDef.fixedRotation = prototype.isFixedRotation();
        bodyDef.bullet = prototype.isBullet();
        bodyDef.gravityScale = prototype.getGravityScale();
        bodyDef.angle = prototype.getAngle() * MathUtils.degreesToRadians;
        bodyDef.linearDamping = prototype.getLinearDamping();
        bodyDef.angularDamping = prototype.getAngularDamping();

        return bodyDef;
    }

}
