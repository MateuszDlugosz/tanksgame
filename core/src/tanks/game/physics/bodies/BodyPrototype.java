package tanks.game.physics.bodies;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.google.common.base.MoreObjects;

import java.io.Serializable;

public class BodyPrototype implements Serializable {

    private BodyDef.BodyType type;
    private boolean active;
    private boolean allowSleep;
    private boolean awake;
    private boolean bullet;
    private boolean fixedRotation;
    private float gravityScale;
    private float angle;
    private float angularDamping;
    private float linearDamping;

    public BodyDef.BodyType getType() {
        return type;
    }

    public void setType(BodyDef.BodyType type) {
        this.type = type;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isAllowSleep() {
        return allowSleep;
    }

    public void setAllowSleep(boolean allowSleep) {
        this.allowSleep = allowSleep;
    }

    public boolean isAwake() {
        return awake;
    }

    public void setAwake(boolean awake) {
        this.awake = awake;
    }

    public boolean isBullet() {
        return bullet;
    }

    public void setBullet(boolean bullet) {
        this.bullet = bullet;
    }

    public boolean isFixedRotation() {
        return fixedRotation;
    }

    public void setFixedRotation(boolean fixedRotation) {
        this.fixedRotation = fixedRotation;
    }

    public float getGravityScale() {
        return gravityScale;
    }

    public void setGravityScale(float gravityScale) {
        this.gravityScale = gravityScale;
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public float getAngularDamping() {
        return angularDamping;
    }

    public void setAngularDamping(float angularDamping) {
        this.angularDamping = angularDamping;
    }

    public float getLinearDamping() {
        return linearDamping;
    }

    public void setLinearDamping(float linearDamping) {
        this.linearDamping = linearDamping;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("type", type)
                .add("active", active)
                .add("allowSleep", allowSleep)
                .add("awake", awake)
                .add("bullet", bullet)
                .add("fixedRotation", fixedRotation)
                .add("gravityScale", gravityScale)
                .add("angle", angle)
                .add("angularDamping", angularDamping)
                .add("linearDamping", linearDamping)
                .toString();
    }

}
