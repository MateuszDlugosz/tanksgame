package tanks.game.physics.bodies;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.utils.XmlReader;

public class BodyPrototypeXmlLoader {

    public static final String BODY_ELEMENT = "body";
    public static final String BODY_TYPE_ELEMENT = "type";
    public static final String ACTIVE_ELEMENT = "active";
    public static final String AWAKE_ELEMENT = "awake";
    public static final String ALLOW_SLEEP_ELEMENT = "allowSleep";
    public static final String FIXED_ROTATION_ELEMENT = "fixedRotation";
    public static final String BULLET_ELEMENT = "bullet";
    public static final String GRAVITY_SCALE_ELEMENT = "gravityScale";
    public static final String ANGLE_ELEMENT = "angle";
    public static final String LINEAR_DAMPING_ELEMENT = "linearDamping";
    public static final String ANGULAR_DAMPING_ELEMENT = "angularDamping";

    public BodyPrototype loadBodyPrototype(XmlReader.Element element) {
        BodyPrototype prototype = new BodyPrototype();
        prototype.setType(loadBodyType(element.getChildByName(BODY_TYPE_ELEMENT)));
        prototype.setActive(loadActive(element.getChildByName(ACTIVE_ELEMENT)));
        prototype.setAwake(loadAwake(element.getChildByName(AWAKE_ELEMENT)));
        prototype.setAllowSleep(loadAllowSleep(element.getChildByName(ALLOW_SLEEP_ELEMENT)));
        prototype.setFixedRotation(loadFixedRotation(element.getChildByName(FIXED_ROTATION_ELEMENT)));
        prototype.setBullet(loadBullet(element.getChildByName(BULLET_ELEMENT)));
        prototype.setGravityScale(loadGravityScale(element.getChildByName(GRAVITY_SCALE_ELEMENT)));
        prototype.setAngle(loadAngle(element.getChildByName(ANGLE_ELEMENT)));
        prototype.setLinearDamping(loadLinearDamping(element.getChildByName(LINEAR_DAMPING_ELEMENT)));
        prototype.setAngularDamping(loadAngularDamping(element.getChildByName(ANGULAR_DAMPING_ELEMENT)));

        return prototype;
    }

    private BodyDef.BodyType loadBodyType(XmlReader.Element element) {
        return BodyDef.BodyType.valueOf(element.getText().trim());
    }

    private boolean loadActive(XmlReader.Element element) {
        if (element == null) return true;
        return Boolean.valueOf(element.getText().trim());
    }

    private boolean loadAwake(XmlReader.Element element) {
        if (element == null) return true;
        return Boolean.valueOf(element.getText().trim());
    }

    private boolean loadAllowSleep(XmlReader.Element element) {
        if (element == null) return true;
        return Boolean.valueOf(element.getText().trim());
    }

    private boolean loadFixedRotation(XmlReader.Element element) {
        if (element == null) return false;
        return Boolean.valueOf(element.getText().trim());
    }

    private boolean loadBullet(XmlReader.Element element) {
        if (element == null) return false;
        return Boolean.valueOf(element.getText().trim());
    }

    private float loadGravityScale(XmlReader.Element element) {
        if (element == null) return 1;
        return Float.valueOf(element.getText().trim());
    }

    private float loadAngle(XmlReader.Element element) {
        if (element == null) return 0;
        return Float.valueOf(element.getText().trim());
    }

    private float loadLinearDamping(XmlReader.Element element) {
        if (element == null) return 0;
        return Float.valueOf(element.getText().trim());
    }

    private float loadAngularDamping(XmlReader.Element element) {
        if (element == null) return 0;
        return Float.valueOf(element.getText().trim());
    }

}
