package tanks.game.physics.filters;

public enum CategoryBit {

    SceneryCategory((short) 1),
    ObstacleCategory((short) 2),
    EnemyTankCategory((short) 4),
    EnemyBulletCategory((short) 8),
    PlayerTankCategory((short) 16),
    PlayerBulletCategory((short) 32),
    WaterCategory((short) 64);

    private final short bit;

    CategoryBit(short bit) {
        this.bit = bit;
    }

    public short getBit() {
        return bit;
    }

}
