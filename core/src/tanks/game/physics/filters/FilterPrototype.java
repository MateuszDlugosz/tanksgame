package tanks.game.physics.filters;

import com.google.common.base.MoreObjects;

import java.io.Serializable;

public class FilterPrototype implements Serializable {

    private CategoryBit categoryBit;
    private MaskBit maskBit;

    public CategoryBit getCategoryBit() {
        return categoryBit;
    }

    public void setCategoryBit(CategoryBit categoryBit) {
        this.categoryBit = categoryBit;
    }

    public MaskBit getMaskBit() {
        return maskBit;
    }

    public void setMaskBit(MaskBit maskBit) {
        this.maskBit = maskBit;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("categoryBit", categoryBit)
                .add("maskBit", maskBit)
                .toString();
    }

}
