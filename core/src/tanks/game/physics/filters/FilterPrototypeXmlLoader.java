package tanks.game.physics.filters;

import com.badlogic.gdx.utils.XmlReader;

public class FilterPrototypeXmlLoader {

    public static final String FILTER_ELEMENT = "filter";
    public static final String CATEGORY_BIT_ELEMENT = "categoryBit";
    public static final String MASK_BIT_ELEMENT = "maskBit";

    public FilterPrototype loadFilterPrototype(XmlReader.Element element) {
        FilterPrototype prototype = new FilterPrototype();
        prototype.setCategoryBit(loadBitCategory(element.getChildByName(CATEGORY_BIT_ELEMENT)));
        prototype.setMaskBit(loadBitMask(element.getChildByName(MASK_BIT_ELEMENT)));

        return prototype;
    }

    private CategoryBit loadBitCategory(XmlReader.Element element) {
        return CategoryBit.valueOf(element.getText().trim());
    }

    private MaskBit loadBitMask(XmlReader.Element element) {
        return MaskBit.valueOf(element.getText().trim());
    }

}
