package tanks.game.physics.filters;

public enum MaskBit {

    SceneryMask((short) -1),
    ObstacleMask((short) (CategoryBit.ObstacleCategory.getBit()
            | CategoryBit.EnemyTankCategory.getBit()
            | CategoryBit.EnemyBulletCategory.getBit()
            | CategoryBit.PlayerTankCategory.getBit()
            | CategoryBit.PlayerBulletCategory.getBit()
    )),
    EnemyTankMask((short) (CategoryBit.EnemyTankCategory.getBit()
            | CategoryBit.ObstacleCategory.getBit()
            | CategoryBit.PlayerTankCategory.getBit()
            | CategoryBit.WaterCategory.getBit()
    )),
    EnemyBulletMask((short) (CategoryBit.PlayerTankCategory.getBit()
            | CategoryBit.PlayerBulletCategory.getBit()
            | CategoryBit.ObstacleCategory.getBit()
    )),
    WaterMask((short) (CategoryBit.PlayerTankCategory.getBit()
            | CategoryBit.EnemyTankCategory.getBit()
    )),
    PlayerTankMask((short) (CategoryBit.EnemyTankCategory.getBit()
            | CategoryBit.EnemyBulletCategory.getBit()
            | CategoryBit.ObstacleCategory.getBit()
            | CategoryBit.WaterCategory.getBit()
    ));

    private final short bit;

    MaskBit(short bit) {
        this.bit = bit;
    }

    public short getBit() {
        return bit;
    }

}
