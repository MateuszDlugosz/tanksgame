package tanks.game.physics.fixtures;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import tanks.game.physics.shapes.ShapeFactory;
import tanks.game.utils.scales.Scale;

public class FixtureFactory {

    private final ShapeFactory shapeFactory;

    public FixtureFactory(ShapeFactory shapeFactory) {
        this.shapeFactory = shapeFactory;
    }

    public Fixture createFixture(FixturePrototype prototype, Body body, Scale scale) {
        return body.createFixture(createFixtureDef(prototype, scale));
    }

    public FixtureDef createFixtureDef(FixturePrototype prototype, Scale scale) {
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.isSensor = prototype.isSensor();
        fixtureDef.density = prototype.getDensity();
        fixtureDef.friction = prototype.getFriction();
        fixtureDef.restitution = prototype.getRestitution();
        fixtureDef.shape = shapeFactory.createShape(prototype.getShapePrototype(), scale);

        fixtureDef.filter.categoryBits = prototype.getFilterPrototype().getCategoryBit().getBit();
        fixtureDef.filter.maskBits = (short) prototype.getFilterPrototype().getMaskBit().getBit();

        return fixtureDef;
    }

}
