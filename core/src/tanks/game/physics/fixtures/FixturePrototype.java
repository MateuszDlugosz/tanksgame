package tanks.game.physics.fixtures;

import com.google.common.base.MoreObjects;
import tanks.game.physics.filters.FilterPrototype;
import tanks.game.physics.shapes.ShapePrototype;

import java.io.Serializable;

public class FixturePrototype implements Serializable {

    private String id;
    private FilterPrototype filterPrototype;
    private ShapePrototype shapePrototype;
    private float friction;
    private float restitution;
    private float density;
    private boolean isSensor;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public FilterPrototype getFilterPrototype() {
        return filterPrototype;
    }

    public void setFilterPrototype(FilterPrototype filterPrototype) {
        this.filterPrototype = filterPrototype;
    }

    public ShapePrototype getShapePrototype() {
        return shapePrototype;
    }

    public void setShapePrototype(ShapePrototype shapePrototype) {
        this.shapePrototype = shapePrototype;
    }

    public float getFriction() {
        return friction;
    }

    public void setFriction(float friction) {
        this.friction = friction;
    }

    public float getRestitution() {
        return restitution;
    }

    public void setRestitution(float restitution) {
        this.restitution = restitution;
    }

    public float getDensity() {
        return density;
    }

    public void setDensity(float density) {
        this.density = density;
    }

    public boolean isSensor() {
        return isSensor;
    }

    public void setIsSensor(boolean sensor) {
        isSensor = sensor;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(getClass())
                .add("id", id)
                .add("filterPrototype", filterPrototype)
                .add("shapePrototype", shapePrototype)
                .add("density", density)
                .add("friction", friction)
                .add("restitution", restitution)
                .add("isSensor", isSensor)
                .toString();
    }

}
