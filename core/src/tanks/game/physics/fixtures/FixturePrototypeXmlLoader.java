package tanks.game.physics.fixtures;

import com.badlogic.gdx.utils.XmlReader;
import tanks.game.physics.filters.FilterPrototypeXmlLoader;
import tanks.game.physics.shapes.ShapePrototypeXmlLoader;

public class FixturePrototypeXmlLoader {

    public static final String FIXTURES_ELEMENT = "fixtures";
    public static final String FIXTURE_ELEMENT = "fixture";
    public static final String ID_ATTRIBUTE = "id";
    public static final String FRICTION_ELEMENT = "friction";
    public static final String DENSITY_ELEMENT = "density";
    public static final String RESTITUTION_ELEMENT = "restitution";
    public static final String IS_SENSOR_ELEMENT = "isSensor";

    private final ShapePrototypeXmlLoader shapePrototypeXmlLoader;
    private final FilterPrototypeXmlLoader filterPrototypeXmlLoader;

    public FixturePrototypeXmlLoader(ShapePrototypeXmlLoader shapePrototypeXmlLoader,
                                     FilterPrototypeXmlLoader filterPrototypeXmlLoader)
    {
        this.shapePrototypeXmlLoader = shapePrototypeXmlLoader;
        this.filterPrototypeXmlLoader = filterPrototypeXmlLoader;
    }

    public FixturePrototype loadFixturePrototype(XmlReader.Element element) {
        FixturePrototype prototype = new FixturePrototype();
        prototype.setId(element.getAttribute(ID_ATTRIBUTE));
        prototype.setFilterPrototype(
                filterPrototypeXmlLoader.loadFilterPrototype(
                        element.getChildByName(FilterPrototypeXmlLoader.FILTER_ELEMENT)
                )
        );

        XmlReader.Element shapeElement = element.getChildByName(ShapePrototypeXmlLoader.SHAPE_ELEMENT);

        if (shapeElement != null) {
            prototype.setShapePrototype(shapePrototypeXmlLoader.loadShapePrototype(shapeElement));
        }

        prototype.setFriction(loadFriction(element.getChildByName(FRICTION_ELEMENT)));
        prototype.setRestitution(loadRestitution(element.getChildByName(RESTITUTION_ELEMENT)));
        prototype.setDensity(loadDensity(element.getChildByName(DENSITY_ELEMENT)));
        prototype.setIsSensor(loadIsSensor(element.getChildByName(IS_SENSOR_ELEMENT)));

        return prototype;
    }

    private float loadFriction(XmlReader.Element element) {
        if (element == null) return 0.2f;
        return Float.valueOf(element.getText().trim());
    }

    private float loadDensity(XmlReader.Element element) {
        if (element == null) return 0;
        return Float.valueOf(element.getText().trim());
    }

    private float loadRestitution(XmlReader.Element element) {
        if (element == null) return 0;
        return Float.valueOf(element.getText().trim());
    }

    private boolean loadIsSensor(XmlReader.Element element) {
        if (element == null) return false;
        return Boolean.valueOf(element.getText().trim());
    }

}
