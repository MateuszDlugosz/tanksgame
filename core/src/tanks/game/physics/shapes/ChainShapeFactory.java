package tanks.game.physics.shapes;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.ChainShape;
import tanks.game.utils.scales.Scale;

@RegistrableShapeFactory(registerAs = ChainShapePrototype.class)
public class ChainShapeFactory implements ConcreteShapeFactory<ChainShape, ChainShapePrototype> {

    @Override
    public ChainShape createShape(ChainShapePrototype prototype, Scale scale) {
        ChainShape shape = new ChainShape();
        Vector2[] vertices = new Vector2[prototype.getVertexPrototypes().size()];
        int number = 0;

        for (VertexPrototype vertex : prototype.getVertexPrototypes()) {
            Vector2 vertexAsVector = new Vector2();
            vertexAsVector.x = scale.scaleValue(vertex.getX() + prototype.getPositionPrototype().getX());
            vertexAsVector.y = scale.scaleValue(vertex.getY() + prototype.getPositionPrototype().getY());
            vertices[number] = vertexAsVector;
            number++;
        }

        shape.createChain(vertices);

        return shape;
    }

}
