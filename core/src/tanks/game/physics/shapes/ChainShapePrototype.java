package tanks.game.physics.shapes;

import com.google.common.base.MoreObjects;
import tanks.game.utils.positions.PositionPrototype;

import java.util.List;

public class ChainShapePrototype implements ShapePrototype {

    private PositionPrototype positionPrototype;
    private List<VertexPrototype> vertexPrototypes;

    @Override
    public void setPositionPrototype(PositionPrototype positionPrototype) {
        this.positionPrototype = positionPrototype;
    }

    @Override
    public PositionPrototype getPositionPrototype() {
        return positionPrototype;
    }

    public List<VertexPrototype> getVertexPrototypes() {
        return vertexPrototypes;
    }

    public void setVertexPrototypes(List<VertexPrototype> vertexPrototypes) {
        this.vertexPrototypes = vertexPrototypes;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("positionPrototype", positionPrototype)
                .add("vertexPrototypes", vertexPrototypes)
                .toString();
    }

}
