package tanks.game.physics.shapes;

import com.badlogic.gdx.maps.objects.PolylineMapObject;
import tanks.game.utils.positions.PositionPrototype;

@RegistrableShapePrototypeMapObjectLoader(registerAs = PolylineMapObject.class)
public class ChainShapePrototypeMapObjectLoader
        implements ConcreteShapePrototypeMapObjectLoader<ChainShapePrototype, PolylineMapObject>
{

    private final VertexPrototypeMapObjectLoader vertexPrototypeMapObjectLoader;

    public ChainShapePrototypeMapObjectLoader(VertexPrototypeMapObjectLoader vertexPrototypeMapObjectLoader) {
        this.vertexPrototypeMapObjectLoader = vertexPrototypeMapObjectLoader;
    }

    @Override
    public ChainShapePrototype loadShapePrototype(PolylineMapObject mapObject) {
        ChainShapePrototype prototype = new ChainShapePrototype();
        PositionPrototype posPrototype = new PositionPrototype();
        posPrototype.setX(0);
        posPrototype.setY(0);

        prototype.setPositionPrototype(posPrototype);
        prototype.setVertexPrototypes(
                vertexPrototypeMapObjectLoader.loadVerticesPrototypes(
                        mapObject.getPolyline().getTransformedVertices()
                )
        );

        return prototype;
    }

}
