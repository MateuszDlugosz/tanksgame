package tanks.game.physics.shapes;

import com.badlogic.gdx.utils.XmlReader;
import tanks.game.utils.positions.PositionPrototypeXmlLoader;

@RegistrableShapePrototypeXmlLoader(registerAs = "ChainShape")
public class ChainShapePrototypeXmlLoader implements ConcreteShapePrototypeXmlLoader<ChainShapePrototype> {

    private final PositionPrototypeXmlLoader positionPrototypeXmlLoader;
    private final VertexPrototypeXmlLoader vertexPrototypeXmlLoader;

    public ChainShapePrototypeXmlLoader(PositionPrototypeXmlLoader positionPrototypeXmlLoader,
                                        VertexPrototypeXmlLoader vertexPrototypeXmlLoader)
    {
        this.positionPrototypeXmlLoader = positionPrototypeXmlLoader;
        this.vertexPrototypeXmlLoader = vertexPrototypeXmlLoader;
    }

    @Override
    public ChainShapePrototype loadShapePrototype(XmlReader.Element element) {
        ChainShapePrototype prototype = new ChainShapePrototype();
        prototype.setPositionPrototype(
                positionPrototypeXmlLoader.loadPositionPrototype(
                        element.getChildByName(PositionPrototypeXmlLoader.POSITION_ELEMENT)
                )
        );
        prototype.setVertexPrototypes(
                vertexPrototypeXmlLoader.loadVerticesPrototypes(
                        element.getChildByName(VertexPrototypeXmlLoader.VERTICES_ELEMENT)
                                .getChildrenByName(VertexPrototypeXmlLoader.VERTEX_ELEMENT)
                )
        );

        return prototype;
    }

}
