package tanks.game.physics.shapes;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import tanks.game.utils.scales.Scale;

@RegistrableShapeFactory(registerAs = CircleShapePrototype.class)
public class CircleShapeFactory implements ConcreteShapeFactory<CircleShape, CircleShapePrototype> {

    @Override
    public CircleShape createShape(CircleShapePrototype prototype, Scale scale) {
        CircleShape shape = new CircleShape();

        shape.setRadius(scale.scaleValue(prototype.getRadius()));
        shape.setPosition(new Vector2(
                scale.scaleValue(prototype.getPositionPrototype().getX()),
                scale.scaleValue(prototype.getPositionPrototype().getY())
        ));

        return shape;
    }

}
