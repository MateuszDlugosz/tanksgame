package tanks.game.physics.shapes;

import com.google.common.base.MoreObjects;
import tanks.game.utils.positions.PositionPrototype;

public class CircleShapePrototype implements ShapePrototype {

    private PositionPrototype positionPrototype;
    private float radius;

    @Override
    public void setPositionPrototype(PositionPrototype positionPrototype) {
        this.positionPrototype = positionPrototype;
    }

    @Override
    public PositionPrototype getPositionPrototype() {
        return positionPrototype;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("positionPrototype", positionPrototype)
                .add("radius", radius)
                .toString();
    }

}
