package tanks.game.physics.shapes;

import com.badlogic.gdx.maps.objects.EllipseMapObject;
import tanks.game.utils.positions.PositionPrototype;

@RegistrableShapePrototypeMapObjectLoader(registerAs = EllipseMapObject.class)
public class CircleShapePrototypeMapObjectLoader
        implements ConcreteShapePrototypeMapObjectLoader<CircleShapePrototype, EllipseMapObject>
{

    @Override
    public CircleShapePrototype loadShapePrototype(EllipseMapObject mapObject) {
        CircleShapePrototype prototype = new CircleShapePrototype();
        prototype.setRadius(mapObject.getEllipse().width*0.5f);

        PositionPrototype posPrototype = new PositionPrototype();
        posPrototype.setX(0);
        posPrototype.setY(0);

        prototype.setPositionPrototype(posPrototype);

        return prototype;
    }

}
