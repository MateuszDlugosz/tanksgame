package tanks.game.physics.shapes;

import com.badlogic.gdx.utils.XmlReader;
import tanks.game.utils.positions.PositionPrototypeXmlLoader;

@RegistrableShapePrototypeXmlLoader(registerAs = "CircleShape")
public class CircleShapePrototypeXmlLoader implements ConcreteShapePrototypeXmlLoader<CircleShapePrototype> {

    public static final String RADIUS_ELEMENT = "radius";

    private final PositionPrototypeXmlLoader positionPrototypeXmlLoader;

    public CircleShapePrototypeXmlLoader(PositionPrototypeXmlLoader positionPrototypeXmlLoader) {
        this.positionPrototypeXmlLoader = positionPrototypeXmlLoader;
    }

    @Override
    public CircleShapePrototype loadShapePrototype(XmlReader.Element element) {
        CircleShapePrototype prototype = new CircleShapePrototype();
        prototype.setPositionPrototype(
                positionPrototypeXmlLoader.loadPositionPrototype(
                        element.getChildByName(PositionPrototypeXmlLoader.POSITION_ELEMENT)
                )
        );
        prototype.setRadius(Float.valueOf(element.getChildByName(RADIUS_ELEMENT).getText()));

        return prototype;
    }

}
