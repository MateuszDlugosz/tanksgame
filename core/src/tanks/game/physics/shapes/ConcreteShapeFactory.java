package tanks.game.physics.shapes;

import com.badlogic.gdx.physics.box2d.Shape;
import tanks.game.utils.scales.Scale;

public interface ConcreteShapeFactory<T extends Shape, U extends ShapePrototype> {

    T createShape(U prototype, Scale scale);

}
