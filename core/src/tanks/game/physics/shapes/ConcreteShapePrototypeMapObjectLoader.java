package tanks.game.physics.shapes;

import com.badlogic.gdx.maps.MapObject;

public interface ConcreteShapePrototypeMapObjectLoader<T extends ShapePrototype, U extends MapObject> {

    T loadShapePrototype(U mapObject);

}
