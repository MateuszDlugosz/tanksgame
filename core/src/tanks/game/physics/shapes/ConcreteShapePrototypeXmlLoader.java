package tanks.game.physics.shapes;

import com.badlogic.gdx.utils.XmlReader;

public interface ConcreteShapePrototypeXmlLoader<T extends ShapePrototype> {

    T loadShapePrototype(XmlReader.Element element);

}
