package tanks.game.physics.shapes;

import com.badlogic.gdx.maps.objects.PolygonMapObject;
import tanks.game.utils.positions.PositionPrototype;

@RegistrableShapePrototypeMapObjectLoader(registerAs = PolygonMapObject.class)
public class PolygonShapePrototypeMapObjectLoader
        implements ConcreteShapePrototypeMapObjectLoader<PolygonShapePrototype, PolygonMapObject>
{

    private final VertexPrototypeMapObjectLoader vertexPrototypeMapObjectLoader;

    public PolygonShapePrototypeMapObjectLoader(VertexPrototypeMapObjectLoader vertexPrototypeMapObjectLoader) {
        this.vertexPrototypeMapObjectLoader = vertexPrototypeMapObjectLoader;
    }

    @Override
    public PolygonShapePrototype loadShapePrototype(PolygonMapObject mapObject) {
        PositionPrototype posPrototype = new PositionPrototype();
        posPrototype.setX(0);
        posPrototype.setY(0);

        PolygonShapePrototype prototype = new PolygonShapePrototype();
        prototype.setPositionPrototype(posPrototype);
        prototype.setVertexPrototypes(
                vertexPrototypeMapObjectLoader.loadVerticesPrototypes(mapObject.getPolygon().getTransformedVertices())
        );

        return prototype;
    }

}
