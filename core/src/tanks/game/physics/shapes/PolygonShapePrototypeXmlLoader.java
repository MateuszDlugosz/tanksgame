package tanks.game.physics.shapes;

import com.badlogic.gdx.utils.XmlReader;
import tanks.game.utils.positions.PositionPrototypeXmlLoader;

@RegistrableShapePrototypeXmlLoader(registerAs = "PolygonShape")
public class PolygonShapePrototypeXmlLoader implements ConcreteShapePrototypeXmlLoader<PolygonShapePrototype> {

    private final PositionPrototypeXmlLoader positionPrototypeXmlLoader;
    private final VertexPrototypeXmlLoader vertexPrototypeXmlLoader;

    public PolygonShapePrototypeXmlLoader(PositionPrototypeXmlLoader positionPrototypeXmlLoader,
                                          VertexPrototypeXmlLoader vertexPrototypeXmlLoader)
    {
        this.positionPrototypeXmlLoader = positionPrototypeXmlLoader;
        this.vertexPrototypeXmlLoader = vertexPrototypeXmlLoader;
    }

    @Override
    public PolygonShapePrototype loadShapePrototype(XmlReader.Element element) {
        PolygonShapePrototype prototype = new PolygonShapePrototype();
        prototype.setPositionPrototype(
                positionPrototypeXmlLoader.loadPositionPrototype(
                        element.getChildByName(PositionPrototypeXmlLoader.POSITION_ELEMENT)
                )
        );
        prototype.setVertexPrototypes(
                vertexPrototypeXmlLoader.loadVerticesPrototypes(
                        element.getChildByName(VertexPrototypeXmlLoader.VERTICES_ELEMENT)
                                .getChildrenByName(VertexPrototypeXmlLoader.VERTEX_ELEMENT)
                )
        );

        return prototype;
    }

}
