package tanks.game.physics.shapes;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import tanks.game.utils.scales.Scale;

@RegistrableShapeFactory(registerAs = RectangleShapePrototype.class)
public class RectangleShapeFactory implements ConcreteShapeFactory<PolygonShape, RectangleShapePrototype> {

    @Override
    public PolygonShape createShape(RectangleShapePrototype prototype, Scale scale) {
        PolygonShape shape = new PolygonShape();

        Vector2 position = new Vector2(
                scale.scaleValue(prototype.getPositionPrototype().getX()),
                scale.scaleValue(prototype.getPositionPrototype().getY())
        );

        shape.setAsBox(
                scale.scaleValue(prototype.getWidth() * 0.5f),
                scale.scaleValue(prototype.getHeight() * 0.5f),
                position,
                0
        );

        return shape;
    }

}
