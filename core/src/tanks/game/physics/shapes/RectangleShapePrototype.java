package tanks.game.physics.shapes;

import com.google.common.base.MoreObjects;
import tanks.game.utils.positions.PositionPrototype;

public class RectangleShapePrototype implements ShapePrototype {

    private PositionPrototype positionPrototype;
    private float width;
    private float height;

    @Override
    public void setPositionPrototype(PositionPrototype positionPrototype) {
        this.positionPrototype = positionPrototype;
    }

    @Override
    public PositionPrototype getPositionPrototype() {
        return positionPrototype;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("positionPrototype", positionPrototype)
                .add("width", width)
                .add("height", height)
                .toString();
    }

}
