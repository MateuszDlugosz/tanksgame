package tanks.game.physics.shapes;

import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import tanks.game.utils.positions.PositionPrototype;

@RegistrableShapePrototypeMapObjectLoader(registerAs = RectangleMapObject.class)
public class RectangleShapePrototypeMapObjectLoader
        implements ConcreteShapePrototypeMapObjectLoader<RectangleShapePrototype, RectangleMapObject>
{

    @Override
    public RectangleShapePrototype loadShapePrototype(RectangleMapObject mapObject) {
        RectangleShapePrototype prototype = new RectangleShapePrototype();
        Rectangle rectangle = mapObject.getRectangle();
        prototype.setWidth(rectangle.getWidth());
        prototype.setHeight(rectangle.getHeight());

        PositionPrototype posPrototype = new PositionPrototype();
        posPrototype.setX(0);
        posPrototype.setY(0);

        prototype.setPositionPrototype(posPrototype);

        return prototype;
    }

}
