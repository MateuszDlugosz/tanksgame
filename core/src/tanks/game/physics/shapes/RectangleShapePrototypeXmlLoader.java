package tanks.game.physics.shapes;

import com.badlogic.gdx.utils.XmlReader;
import tanks.game.utils.positions.PositionPrototypeXmlLoader;

@RegistrableShapePrototypeXmlLoader(registerAs = "RectangleShape")
public class RectangleShapePrototypeXmlLoader implements ConcreteShapePrototypeXmlLoader<RectangleShapePrototype> {

    public static final String WIDTH_ELEMENT = "width";
    public static final String HEIGHT_ELEMENT = "height";

    private final PositionPrototypeXmlLoader positionPrototypeXmlLoader;

    public RectangleShapePrototypeXmlLoader(PositionPrototypeXmlLoader positionPrototypeXmlLoader) {
        this.positionPrototypeXmlLoader = positionPrototypeXmlLoader;
    }

    @Override
    public RectangleShapePrototype loadShapePrototype(XmlReader.Element element) {
        RectangleShapePrototype prototype = new RectangleShapePrototype();
        prototype.setPositionPrototype(
                positionPrototypeXmlLoader.loadPositionPrototype(
                        element.getChildByName(PositionPrototypeXmlLoader.POSITION_ELEMENT)
                )
        );
        prototype.setWidth(Float.valueOf(element.getChildByName(WIDTH_ELEMENT).getText()));
        prototype.setHeight(Float.valueOf(element.getChildByName(HEIGHT_ELEMENT).getText()));

        return prototype;
    }

}
