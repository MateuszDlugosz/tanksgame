package tanks.game.physics.shapes;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@interface RegistrableShapeFactory {

    Class<? extends ShapePrototype> registerAs();

}
