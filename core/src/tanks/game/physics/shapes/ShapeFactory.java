package tanks.game.physics.shapes;

import com.badlogic.gdx.physics.box2d.Shape;
import tanks.game.utils.scales.Scale;

import java.util.HashMap;
import java.util.Map;

public class ShapeFactory {

    private final Map<Class<? extends ShapePrototype>, ConcreteShapeFactory> innerFactories;

    public ShapeFactory(ConcreteShapeFactory[] factories) {
        innerFactories = new HashMap<Class<? extends ShapePrototype>, ConcreteShapeFactory>();

        for (ConcreteShapeFactory factory : factories) {
            RegistrableShapeFactory annotation = factory.getClass().getAnnotation(RegistrableShapeFactory.class);
            if (annotation == null) {
                throw new AnnotationNotFoundException(String.format("Annotation %s not found in class %s.",
                        RegistrableShapeFactory.class, factory.getClass()));
            }

            innerFactories.put(annotation.registerAs(), factory);
        }
    }

    public Shape createShape(ShapePrototype prototype, Scale scale) {
        Class<? extends ShapePrototype> type = prototype.getClass();

        if (innerFactories.containsKey(type)) {
            return innerFactories.get(type).createShape(prototype, scale);
        }
        else {
            throw new FactoryNotFoundException(String.format("Factory of type %s not found.", type));
        }
    }

}
