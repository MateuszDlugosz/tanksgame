package tanks.game.physics.shapes;

import tanks.game.utils.positions.PositionPrototype;

import java.io.Serializable;

public interface ShapePrototype extends Serializable {

    void setPositionPrototype(PositionPrototype positionPrototype);
    PositionPrototype getPositionPrototype();

}
