package tanks.game.physics.shapes;

import com.badlogic.gdx.maps.MapObject;

import java.util.HashMap;
import java.util.Map;

public class ShapePrototypeMapObjectLoader {

    private final Map<Class<? extends MapObject>, ConcreteShapePrototypeMapObjectLoader> innerLoaders;

    public ShapePrototypeMapObjectLoader(ConcreteShapePrototypeMapObjectLoader[] loaders) {
        innerLoaders = new HashMap<Class<? extends MapObject>, ConcreteShapePrototypeMapObjectLoader>();

        for (ConcreteShapePrototypeMapObjectLoader loader : loaders) {
            RegistrableShapePrototypeMapObjectLoader annotation
                    = loader.getClass().getAnnotation(RegistrableShapePrototypeMapObjectLoader.class);

            if (annotation == null) {
                throw new AnnotationNotFoundException(String.format("Annotation %s not found in class %s.",
                        RegistrableShapePrototypeMapObjectLoader.class, loader.getClass()));
            }

            innerLoaders.put(annotation.registerAs(), loader);
        }
    }

    public ShapePrototype loadShapePrototype(MapObject mapObject) {
        if (innerLoaders.containsKey(mapObject.getClass())) {
            return innerLoaders.get(mapObject.getClass()).loadShapePrototype(mapObject);
        }
        else {
            throw new LoaderNotFoundException(String.format("Prototype loader of type %s not found.", mapObject.getClass()));
        }
    }

}
