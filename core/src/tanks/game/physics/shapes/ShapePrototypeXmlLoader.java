package tanks.game.physics.shapes;

import com.badlogic.gdx.utils.XmlReader;

import java.util.HashMap;
import java.util.Map;

public class ShapePrototypeXmlLoader {

    public static final String SHAPE_ELEMENT = "shape";
    public static final String TYPE_ATTRIBUTE = "type";

    private final Map<String, ConcreteShapePrototypeXmlLoader> innerLoaders;

    public ShapePrototypeXmlLoader(ConcreteShapePrototypeXmlLoader[] loaders) {
        innerLoaders = new HashMap<String, ConcreteShapePrototypeXmlLoader>();

        for (ConcreteShapePrototypeXmlLoader loader : loaders) {
            RegistrableShapePrototypeXmlLoader annotation
                    = loader.getClass().getAnnotation(RegistrableShapePrototypeXmlLoader.class);

            if (annotation == null) {
                throw new AnnotationNotFoundException(String.format("Annotation %s not found in class %s.",
                        RegistrableShapePrototypeXmlLoader.class, loader.getClass()));
            }

            innerLoaders.put(annotation.registerAs(), loader);
        }
    }

    public ShapePrototype loadShapePrototype(XmlReader.Element element) {
        String type = element.getAttribute(TYPE_ATTRIBUTE);

        if (innerLoaders.containsKey(type)) {
            return innerLoaders.get(type).loadShapePrototype(element);
        }
        else {
            throw new LoaderNotFoundException(String.format("Prototype loader of type %s not found.", type));
        }
    }

}
