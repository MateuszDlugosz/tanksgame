package tanks.game.physics.shapes;

import java.util.ArrayList;
import java.util.List;

public class VertexPrototypeMapObjectLoader {

    public List<VertexPrototype> loadVerticesPrototypes(float[] vertices) {
        List<VertexPrototype> loadedVertices = new ArrayList<VertexPrototype>();

        for (int i = 0; i < vertices.length / 2; ++i) {
            VertexPrototype prototype = new VertexPrototype();
            prototype.setX(vertices[i * 2]);
            prototype.setY(vertices[i * 2 + 1]);
            loadedVertices.add(prototype);
        }

        return loadedVertices;
    }

}
