package tanks.game.physics.shapes;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

import java.util.ArrayList;
import java.util.List;

public class VertexPrototypeXmlLoader {

    public static final String VERTEX_ELEMENT = "vertex";
    public static final String VERTICES_ELEMENT = "vertices";
    public static final String X_ATTRIBUTE = "x";
    public static final String Y_ATTRIBUTE = "y";

    public VertexPrototype loadVertexPrototype(XmlReader.Element element) {
        VertexPrototype prototype = new VertexPrototype();
        prototype.setX(element.getFloat(X_ATTRIBUTE));
        prototype.setY(element.getFloat(Y_ATTRIBUTE));

        return prototype;
    }

    public List<VertexPrototype> loadVerticesPrototypes(Array<XmlReader.Element> elements) {
        List<VertexPrototype> prototypes = new ArrayList<VertexPrototype>();

        for (XmlReader.Element vertexElement : elements) {
            prototypes.add(loadVertexPrototype(vertexElement));
        }

        return prototypes;
    }

}
