package tanks.game.physics.worlds;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import tanks.game.utils.properties.Properties;

public class WorldFactory {

    public static final String GRAVITY_X_PROPERTY = "gravity_x";
    public static final String GRAVITY_Y_PROPERTY = "gravity_y";
    public static final String ALLOW_SLEEP_PROPERTY = "allow_sleep";

    public static final float DEFAULT_GRAVITY_X_VALUE = 0;
    public static final float DEFAULT_GRAVITY_Y_VALUE = 0;
    public static final boolean DEFAULT_ALLOW_SLEEP_VALUE = true;

    public World createWorld() {
        return createWorld(new Properties());
    }

    public World createWorld(Properties properties) {
        World world = new World(
                createWorldGravity(properties),
                properties.getBoolean(
                        ALLOW_SLEEP_PROPERTY,
                        DEFAULT_ALLOW_SLEEP_VALUE
                )
        );

        return world;
    }

    private Vector2 createWorldGravity(Properties properties) {
        return new Vector2(
                properties.getFloat(
                        GRAVITY_X_PROPERTY,
                        DEFAULT_GRAVITY_X_VALUE
                ),
                properties.getFloat(
                        GRAVITY_Y_PROPERTY,
                        DEFAULT_GRAVITY_Y_VALUE
                )
        );
    }

}
