package tanks.game.physics.worlds.renderers;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;

public class WorldRenderer {

    private final Box2DDebugRenderer box2DDebugRenderer;

    public WorldRenderer(Box2DDebugRenderer box2DDebugRenderer) {
        this.box2DDebugRenderer = box2DDebugRenderer;
    }

    public void render(World world, Camera camera) {
        box2DDebugRenderer.render(world, camera.combined);
    }

}
