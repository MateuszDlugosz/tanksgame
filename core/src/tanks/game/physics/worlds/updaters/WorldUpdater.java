package tanks.game.physics.worlds.updaters;

import com.badlogic.gdx.physics.box2d.World;

public class WorldUpdater {

    public void step(World world, float delta, float timeStep, int velocityIterations, int positionIterations) {
        world.step(timeStep, velocityIterations, positionIterations);
    }

}
