package tanks.game.providers;

import tanks.game.context.Context;
import tanks.game.context.ProviderClass;
import tanks.game.context.ProviderMethod;
import tanks.game.graphics.animations.*;
import tanks.game.graphics.cameras.CameraFactory;
import tanks.game.graphics.cameras.GameCameraFactory;
import tanks.game.graphics.cameras.OrthographicCameraFactory;
import tanks.game.graphics.colors.*;
import tanks.game.graphics.effects.*;
import tanks.game.graphics.particles.*;
import tanks.game.graphics.shaders.ShaderProgramFactory;
import tanks.game.graphics.shaders.ShaderProgramPrototypeXmlLoader;
import tanks.game.graphics.sprites.*;
import tanks.game.graphics.viewports.FitViewportFactory;
import tanks.game.graphics.viewports.ViewportFactory;
import tanks.game.graphics.views.*;
import tanks.game.graphics.views.renderers.*;
import tanks.game.graphics.views.updaters.ViewUpdater;
import tanks.game.utils.positions.PositionFactory;
import tanks.game.utils.positions.PositionPrototypeXmlLoader;

@ProviderClass
public class GraphicsProviders {

    @ProviderMethod
    public ShaderProgramFactory getShaderProgramFactory(Context context) {
        return new ShaderProgramFactory();
    }

    @ProviderMethod
    public ShaderProgramPrototypeXmlLoader getShaderProgramPrototypeXmlLoader(Context context) {
        return new ShaderProgramPrototypeXmlLoader();
    }

    @ProviderMethod
    public AnimationFactory getAnimationFactory(Context context) {
        return new AnimationFactory(
                new ConcreteAnimationFactory[] {
                        new FileAnimationFactory(),
                        new AtlasAnimationFactory()
                }
        );
    }

    @ProviderMethod
    public AnimationPrototypeXmlLoader getAnimationPrototypeXmlLoader(Context context) {
        return new AnimationPrototypeXmlLoader(
                new ConcreteAnimationPrototypeXmlLoader[] {
                        new FileAnimationPrototypeXmlLoader(),
                        new AtlasAnimationPrototypeXmlLoader()
                }
        );
    }

    @ProviderMethod
    public SpriteFactory getSpriteFactory(Context context) {
        return new SpriteFactory(
                new ConcreteSpriteFactory[] {
                        new FileSpriteFactory(),
                        new AtlasSpriteFactory()
                }
        );
    }

    @ProviderMethod
    public SpritePrototypeXmlLoader getSpritePrototypeXmlLoader(Context context) {
        return new SpritePrototypeXmlLoader(
                new ConcreteSpritePrototypeXmlLoader[] {
                        new FileSpritePrototypeXmlLoader(),
                        new AtlasSpritePrototypeXmlLoader()
                }
        );
    }

    @ProviderMethod
    public ColorFactory getColorFactory(Context context) {
        return new ColorFactory(
                new ConcreteColorFactory[] {
                        new RgbColorFactory(),
                        new HexColorFactory()
                }
        );
    }

    @ProviderMethod
    public ColorPrototypeXmlLoader getColorPrototypeXmlLoader(Context context) {
        return new ColorPrototypeXmlLoader(
                new ConcreteColorPrototypeXmlLoader[] {
                        new RgbColorPrototypeXmlLoader(),
                        new HexColorPrototypeXmlLoader()
                }
        );
    }

    @ProviderMethod
    public ParticleEffectFactory getParticleEffectFactory(Context context) {
        return new ParticleEffectFactory(
                new ConcreteParticleEffectFactory[] {
                        new FileParticleEffectFactory(),
                        new AtlasParticleEffectFactory()
                }
        );
    }

    @ProviderMethod
    public ParticleEffectPrototypeXmlLoader getParticleEffectPrototypeXmlLoader(Context context) {
        return new ParticleEffectPrototypeXmlLoader(
                new ConcreteParticleEffectPrototypeXmlLoader[] {
                        new FileParticleEffectPrototypeXmlLoader(),
                        new AtlasParticleEffectPrototypeXmlLoader()
                }
        );
    }

    @ProviderMethod
    public EffectPrototypeXmlLoader getEffectPrototypeXmlLoader(Context context) {
        return new EffectPrototypeXmlLoader(
                new ConcreteEffectPrototypeXmlLoader[] {
                        new AlphaEffectPrototypeXmlLoader(),
                        new TintEffectPrototypeXmlLoader(
                                context.getObject(ColorPrototypeXmlLoader.class)
                        ),
                        new ShaderEffectPrototypeXmlLoader(
                                context.getObject(ShaderProgramPrototypeXmlLoader.class)
                        )
                }
        );
    }

    @ProviderMethod
    public EffectFactory getEffectFactory(Context context) {
        return new EffectFactory(
                new ConcreteEffectFactory[] {
                        new AlphaEffectFactory(),
                        new TintEffectFactory(
                                context.getObject(ColorFactory.class)
                        ),
                        new ShaderEffectFactory(
                                context.getObject(ShaderProgramFactory.class)
                        )
                }
        );
    }

    @ProviderMethod
    public ViewPrototypeXmlLoader getViewPrototypeXmlLoader(Context context) {
        return new ViewPrototypeXmlLoader(
                new ConcreteViewPrototypeXmlLoader[] {
                        new SpriteViewPrototypeXmlLoader(
                                context.getObject(PositionPrototypeXmlLoader.class),
                                context.getObject(SpritePrototypeXmlLoader.class)
                        ),
                        new AnimationViewPrototypeXmlLoader(
                                context.getObject(PositionPrototypeXmlLoader.class),
                                context.getObject(AnimationPrototypeXmlLoader.class)
                        ),
                        new ParticleViewPrototypeXmlLoader(
                                context.getObject(PositionPrototypeXmlLoader.class),
                                context.getObject(ParticleEffectPrototypeXmlLoader.class)
                        )
                }
        );
    }

    @ProviderMethod
    public ViewFactory getViewFactory(Context context) {
        return new ViewFactory(
                new ConcreteViewFactory[] {
                        new SpriteViewFactory(
                                context.getObject(PositionFactory.class),
                                context.getObject(SpriteFactory.class)
                        ),
                        new AnimationViewFactory(
                                context.getObject(PositionFactory.class),
                                context.getObject(AnimationFactory.class)
                        ),
                        new ParticleViewFactory(
                                context.getObject(PositionFactory.class),
                                context.getObject(ParticleEffectFactory.class)
                        )
                }
        );
    }

    @ProviderMethod
    public ViewUpdater getViewUpdater(Context context) {
        return new ViewUpdater();
    }

    @ProviderMethod
    public ViewRenderer getViewRenderer(Context context) {
        return new ViewRenderer(
                new ConcreteViewRenderer[] {
                        new SpriteViewRenderer(),
                        new AnimationViewRenderer(),
                        new ParticleViewRenderer()
                }
        );
    }

    @ProviderMethod
    public ViewportFactory getViewportFactory(Context context) {
        return new FitViewportFactory();
    }

    @ProviderMethod
    public CameraFactory getCameraFactory(Context context) {
        return new OrthographicCameraFactory();
    }

    @ProviderMethod
    public GameCameraFactory getGameCameraFactory(Context context) {
        return new GameCameraFactory(
                context.getObject(CameraFactory.class),
                context.getObject(ViewportFactory.class)
        );
    }

}
