package tanks.game.providers;

import tanks.game.context.Context;
import tanks.game.context.ProviderClass;
import tanks.game.context.ProviderMethod;
import tanks.game.graphics.views.ViewFactory;
import tanks.game.graphics.views.ViewPrototypeXmlLoader;
import tanks.game.graphics.views.renderers.ViewRenderer;
import tanks.game.levels.LevelFactory;
import tanks.game.levels.LevelPrototypeXmlLoader;
import tanks.game.levels.entities.EntityFactory;
import tanks.game.levels.entities.EntityPrototypeRepositoryXmlLoader;
import tanks.game.levels.entities.EntityPrototypeXmlLoader;
import tanks.game.levels.entities.components.*;
import tanks.game.levels.entities.renderers.EntityRenderer;
import tanks.game.levels.renderers.LevelRenderer;
import tanks.game.levels.updaters.LevelUpdater;
import tanks.game.physics.bodies.BodyFactory;
import tanks.game.physics.bodies.BodyPrototypeXmlLoader;
import tanks.game.physics.fixtures.FixtureFactory;
import tanks.game.physics.fixtures.FixturePrototypeXmlLoader;
import tanks.game.physics.worlds.WorldFactory;
import tanks.game.physics.worlds.renderers.WorldRenderer;
import tanks.game.physics.worlds.updaters.WorldUpdater;
import tanks.game.utils.positions.PositionFactory;
import tanks.game.utils.positions.PositionPrototypeMapObjectLoader;
import tanks.game.utils.properties.MapPropertiesConverter;

@ProviderClass
public class LevelsProviders {

    @ProviderMethod
    public LevelFactory getLevelFactory(Context context) {
        return new LevelFactory(
                context.getObject(MapPropertiesConverter.class),
                context.getObject(WorldFactory.class),
                context.getObject(EntityFactory.class)
        );
    }

    @ProviderMethod
    public LevelPrototypeXmlLoader getLevelPrototypeXmlLoader(Context context) {
        return new LevelPrototypeXmlLoader();
    }

    @ProviderMethod
    public LevelRenderer getLevelRenderer(Context context) {
        return new LevelRenderer(
                context.getObject(WorldRenderer.class),
                context.getObject(EntityRenderer.class)
        );
    }

    @ProviderMethod
    public EntityPrototypeRepositoryXmlLoader getEntityPrototypeRepositoryXmlLoader(Context context) {
        return new EntityPrototypeRepositoryXmlLoader();
    }

    @ProviderMethod
    public EntityPrototypeXmlLoader getEntityPrototypeXmlLoader(Context context) {
        return new EntityPrototypeXmlLoader(
                context.getObject(ComponentPrototypeXmlLoader.class)
        );
    }

    @ProviderMethod
    public ComponentPrototypeXmlLoader getComponentPrototypeXmlLoader(Context context) {
        return new ComponentPrototypeXmlLoader(
                new ConcreteComponentPrototypeXmlLoader[] {
                        new PhysicsComponentPrototypeXmlLoader(
                                context.getObject(BodyPrototypeXmlLoader.class),
                                context.getObject(FixturePrototypeXmlLoader.class)
                        ),
                        new GraphicsComponentPrototypeXmlLoader(
                                context.getObject(ViewPrototypeXmlLoader.class)
                        ),
                        new ObstacleLayerComponentPrototypeXmlLoader(),
                        new GrassLayerComponentPrototypeXmlLoader(),
                        new GroundLayerComponentPrototypeXmlLoader(),
                        new TanksLayerComponentPrototypeXmlLoader(),
                        new BulletLayerComponentPrototypeXmlLoader(),
                        new AIComponentPrototypeXmlLoader(),
                        new SpeedComponentPrototypeXmlLoader(),
                        new DirectionComponentPrototypeXmlLoader(),
                        new HealthComponentPrototypeXmlLoader(),
                        new BulletComponentPrototypeXmlLoader(),
                        new ExplosiveComponentPrototypeXmlLoader(),
                        new LifetimeComponentPrototypeXmlLoader()
                }
        );
    }

    @ProviderMethod
    public ComponentFactory getComponentFactory(Context context) {
        return new ComponentFactory(
                new ConcreteComponentFactory[] {
                        new PhysicsComponentFactory(
                                context.getObject(BodyFactory.class),
                                context.getObject(FixtureFactory.class)
                        ),
                        new GraphicsComponentFactory(
                                context.getObject(ViewFactory.class)
                        ),
                        new ObstacleLayerComponentFactory(),
                        new GrassLayerComponentFactory(),
                        new GroundLayerComponentFactory(),
                        new TanksLayerComponentFactory(),
                        new BulletLayerComponentFactory(),
                        new AIComponentFactory(),
                        new SpeedComponentFactory(),
                        new DirectionComponentFactory(),
                        new HealthComponentFactory(),
                        new BulletComponentFactory(),
                        new ExplosiveComponentFactory(),
                        new LifetimeComponentFactory()
                }
        );
    }

    @ProviderMethod
    public EntityFactory getEntityFactory(Context context) {
        return new EntityFactory(
                context.getObject(ComponentFactory.class),
                context.getObject(PositionPrototypeMapObjectLoader.class),
                context.getObject(PositionFactory.class)
        );
    }

    @ProviderMethod
    public EntityRenderer getEntityRenderer(Context context) {
        return new EntityRenderer(
                context.getObject(ViewRenderer.class)
        );
    }

    @ProviderMethod
    public LevelUpdater getLevelUpdater(Context context) {
        return new LevelUpdater(
                context.getObject(WorldUpdater.class)
        );
    }

}
