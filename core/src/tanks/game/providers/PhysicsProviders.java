package tanks.game.providers;

import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import tanks.game.context.Context;
import tanks.game.context.ProviderClass;
import tanks.game.context.ProviderMethod;
import tanks.game.graphics.colors.ColorFactory;
import tanks.game.graphics.colors.ColorPrototypeXmlLoader;
import tanks.game.physics.bodies.BodyFactory;
import tanks.game.physics.bodies.BodyPrototypeXmlLoader;
import tanks.game.physics.filters.FilterPrototypeXmlLoader;
import tanks.game.physics.fixtures.FixtureFactory;
import tanks.game.physics.fixtures.FixturePrototypeXmlLoader;
import tanks.game.physics.shapes.*;
import tanks.game.physics.worlds.WorldFactory;
import tanks.game.physics.worlds.renderers.WorldRenderer;
import tanks.game.physics.worlds.updaters.WorldUpdater;
import tanks.game.utils.positions.PositionPrototypeXmlLoader;

@ProviderClass
public class PhysicsProviders {

    @ProviderMethod
    public BodyFactory getBodyFactory(Context context) {
        return new BodyFactory();
    }

    @ProviderMethod
    public BodyPrototypeXmlLoader getBodyPrototypeXmlLoader(Context context) {
        return new BodyPrototypeXmlLoader();
    }

    @ProviderMethod
    public FilterPrototypeXmlLoader getFilterPrototypeXmlLoader(Context context) {
        return new FilterPrototypeXmlLoader();
    }

    @ProviderMethod
    public ShapeFactory getShapeFactory(Context context) {
        return new ShapeFactory(
                new ConcreteShapeFactory[] {
                        new RectangleShapeFactory(),
                        new CircleShapeFactory(),
                        new PolygonShapeFactory(),
                        new ChainShapeFactory()
                }
        );
    }

    @ProviderMethod
    public VertexPrototypeXmlLoader getVertexPrototypeXmlLoader(Context context) {
        return new VertexPrototypeXmlLoader();
    }

    @ProviderMethod
    public ShapePrototypeXmlLoader getShapePrototypeXmlLoader(Context context) {
        return new ShapePrototypeXmlLoader(
                new ConcreteShapePrototypeXmlLoader[] {
                        new RectangleShapePrototypeXmlLoader(
                                context.getObject(PositionPrototypeXmlLoader.class)
                        ),
                        new CircleShapePrototypeXmlLoader(
                                context.getObject(PositionPrototypeXmlLoader.class)
                        ),
                        new PolygonShapePrototypeXmlLoader(
                                context.getObject(PositionPrototypeXmlLoader.class),
                                context.getObject(VertexPrototypeXmlLoader.class)
                        ),
                        new ChainShapePrototypeXmlLoader(
                                context.getObject(PositionPrototypeXmlLoader.class),
                                context.getObject(VertexPrototypeXmlLoader.class)
                        )
                }
         );
    }

    @ProviderMethod
    public VertexPrototypeMapObjectLoader getVertexPrototypeMapObjectLoader(Context context) {
        return new VertexPrototypeMapObjectLoader();
    }

    @ProviderMethod
    public ShapePrototypeMapObjectLoader getShapePrototypeMapObjectLoader(Context context) {
        return new ShapePrototypeMapObjectLoader(
                new ConcreteShapePrototypeMapObjectLoader[] {
                        new RectangleShapePrototypeMapObjectLoader(),
                        new CircleShapePrototypeMapObjectLoader(),
                        new PolygonShapePrototypeMapObjectLoader(
                                context.getObject(VertexPrototypeMapObjectLoader.class)
                        ),
                        new ChainShapePrototypeMapObjectLoader(
                                context.getObject(VertexPrototypeMapObjectLoader.class)
                        )
                }
        );
    }

    @ProviderMethod
    public FixturePrototypeXmlLoader getFixturePrototypeXmlLoader(Context context) {
        return new FixturePrototypeXmlLoader(
                context.getObject(ShapePrototypeXmlLoader.class),
                context.getObject(FilterPrototypeXmlLoader.class)
        );
    }

    @ProviderMethod
    public FixtureFactory getFixtureFactory(Context context) {
        return new FixtureFactory(
                context.getObject(ShapeFactory.class)
        );
    }

    @ProviderMethod
    public Box2DDebugRenderer getBox2DDebugRenderer(Context context) {
        return new Box2DDebugRenderer();
    }

    @ProviderMethod
    public WorldFactory getWorldFactory(Context context) {
        return new WorldFactory();
    }

    @ProviderMethod
    public WorldRenderer getWorldRenderer(Context context) {
        return new WorldRenderer(
                context.getObject(Box2DDebugRenderer.class)
        );
    }

    @ProviderMethod
    public WorldUpdater getWorldUpdater(Context context) {
        return new WorldUpdater();
    }

}
