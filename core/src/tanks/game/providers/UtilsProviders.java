package tanks.game.providers;

import tanks.game.context.Context;
import tanks.game.context.ProviderClass;
import tanks.game.context.ProviderMethod;
import tanks.game.utils.boundaries.BoundariesFactory;
import tanks.game.utils.boundaries.BoundariesPrototypeXmlLoader;
import tanks.game.utils.positions.PositionFactory;
import tanks.game.utils.positions.PositionPrototypeMapObjectLoader;
import tanks.game.utils.positions.PositionPrototypeXmlLoader;
import tanks.game.utils.properties.MapPropertiesConverter;
import tanks.game.utils.properties.PropertiesMapObjectLoader;
import tanks.game.utils.properties.PropertiesXmlLoader;

@ProviderClass
public class UtilsProviders {


    @ProviderMethod
    public BoundariesFactory getBoundariesFactory(Context context) {
        return new BoundariesFactory();
    }

    @ProviderMethod
    public BoundariesPrototypeXmlLoader getBoundariesPrototypeXmlLoader(Context context) {
        return new BoundariesPrototypeXmlLoader();
    }

    @ProviderMethod
    public PositionFactory getPositionFactory(Context context) {
        return new PositionFactory();
    }

    @ProviderMethod
    public PositionPrototypeXmlLoader getPositionPrototypeXmlLoader(Context context) {
        return new PositionPrototypeXmlLoader();
    }

    @ProviderMethod
    public PositionPrototypeMapObjectLoader getPositionPrototypeMapObjectLoader(Context context) {
        return new PositionPrototypeMapObjectLoader();
    }

    @ProviderMethod
    public PropertiesXmlLoader getPropertiesXmlLoader(Context context) {
        return new PropertiesXmlLoader();
    }

    @ProviderMethod
    public MapPropertiesConverter getMapPropertiesConverter(Context context) {
        return new MapPropertiesConverter();
    }

    @ProviderMethod
    public PropertiesMapObjectLoader getPropertiesMapObjectLoader(Context context) {
        return new PropertiesMapObjectLoader(
                context.getObject(MapPropertiesConverter.class)
        );
    }

}
