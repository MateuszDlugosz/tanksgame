package tanks.game.screens;

import com.badlogic.gdx.Screen;
import tanks.game.GameApplication;
import tanks.game.context.Context;

public abstract class GameScreen implements Screen {

    private final GameApplication gameApplication;
    private final Context context;

    public GameScreen(GameApplication gameApplication) {
        this.gameApplication = gameApplication;
        this.context = gameApplication.getContext();
    }

    public GameApplication getGameApplication() {
        return gameApplication;
    }

    public Context getContext() {
        return context;
    }

}
