package tanks.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.google.common.eventbus.EventBus;
import tanks.game.GameApplication;
import tanks.game.graphics.cameras.GameCamera;
import tanks.game.graphics.cameras.GameCameraFactory;
import tanks.game.levels.Level;
import tanks.game.levels.LevelController;
import tanks.game.levels.LevelFactory;
import tanks.game.levels.LevelPrototypeXmlLoader;
import tanks.game.levels.entities.EntityFactory;
import tanks.game.levels.entities.EntityPrototypeRepository;
import tanks.game.levels.entities.EntityPrototypeRepositoryXmlLoader;
import tanks.game.levels.entities.EntityPrototypeXmlLoader;
import tanks.game.levels.renderers.LevelRenderer;
import tanks.game.levels.updaters.LevelUpdater;

public class LevelScreen extends GameScreen {

    private float factor = 4f;
    private final LevelController levelController;
    private final LevelRenderer levelRenderer;
    private final LevelUpdater levelUpdater;
    private final GameCamera gameCamera;
    private final SpriteBatch spriteBatch;
    private boolean debug;

    public LevelScreen(GameApplication gameApplication, String levelFilename) {
        super(gameApplication);

        EntityPrototypeRepositoryXmlLoader repositoryXmlLoader = getContext().getObject(EntityPrototypeRepositoryXmlLoader.class);
        EntityPrototypeRepository repository = repositoryXmlLoader.loadEntityPrototypeRepository(
                "prototypes/entities/repository.xml",
                getContext().getObject(EntityPrototypeXmlLoader.class)
        );

        LevelPrototypeXmlLoader loader = getContext().getObject(LevelPrototypeXmlLoader.class);
        LevelFactory factory = getContext().getObject(LevelFactory.class);
        EntityFactory entityFactory = getContext().getObject(EntityFactory.class);
        spriteBatch = gameApplication.getSpriteBatch();
        EventBus eventBus = gameApplication.getEventBus();
        Level level = factory.createLevel(
                loader.loadLevelPrototype(levelFilename),
                gameApplication.getSpriteBatch(),
                repository,
                eventBus
        );
        levelRenderer = getContext().getObject(LevelRenderer.class);
        levelUpdater = getContext().getObject(LevelUpdater.class);
        gameCamera = getContext().getObject(GameCameraFactory.class).createGameCamera(960/factor, 960/factor, false, level.getScale());
        gameCamera.setPosition(
                level.getScale().scaleValue(960/factor/2),
                level.getScale().scaleValue(960/factor/2)
        );
        levelController = new LevelController(level, repository, entityFactory, levelUpdater, levelRenderer);
        gameApplication.getEventBus().register(levelController);
        debug = true;
    }

    @Override
    public void show() {

    }

    private void update(float delta) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.F1)) {
            debug = !debug;
        }

        levelController.update(delta);
        gameCamera.update(delta);
    }

    @Override
    public void render(float delta) {
        update(delta);
        levelController.render(spriteBatch, gameCamera.getCamera(), debug);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

}
