package tanks.game.utils.boundaries;

import tanks.game.utils.scales.Scale;

public class BoundariesFactory {

    public Boundaries createBoundaries(BoundariesPrototype prototype, Scale scale) {
        return new Boundaries(
                scale.scaleValue(prototype.getMinX()),
                scale.scaleValue(prototype.getMaxX()),
                scale.scaleValue(prototype.getMinY()),
                scale.scaleValue(prototype.getMaxY())
        );
    }

}
