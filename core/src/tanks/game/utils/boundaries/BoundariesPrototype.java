package tanks.game.utils.boundaries;

import com.google.common.base.MoreObjects;

import java.io.Serializable;

public class BoundariesPrototype implements Serializable {

    private float minX;
    private float maxX;
    private float minY;
    private float maxY;

    public float getMinX() {
        return minX;
    }

    public void setMinX(float minX) {
        this.minX = minX;
    }

    public float getMaxX() {
        return maxX;
    }

    public void setMaxX(float maxX) {
        this.maxX = maxX;
    }

    public float getMinY() {
        return minY;
    }

    public void setMinY(float minY) {
        this.minY = minY;
    }

    public float getMaxY() {
        return maxY;
    }

    public void setMaxY(float maxY) {
        this.maxY = maxY;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(getClass())
                .add("minX", minX)
                .add("maxX", maxX)
                .add("minY", minY)
                .add("maxY", maxY)
                .toString();
    }

}
