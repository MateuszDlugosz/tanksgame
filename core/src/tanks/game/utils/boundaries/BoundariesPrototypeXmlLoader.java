package tanks.game.utils.boundaries;

import com.badlogic.gdx.utils.XmlReader;

public class BoundariesPrototypeXmlLoader {

    public static final String BOUNDARIES_ELEMENT = "boundaries";
    public static final String MIN_X_ELEMENT = "minX";
    public static final String MAX_X_ELEMENT = "maxX";
    public static final String MIN_Y_ELEMENT = "minY";
    public static final String MAX_Y_ELEMENT = "maxY";

    public BoundariesPrototype loadBoundariesPrototype(XmlReader.Element element) {
        BoundariesPrototype prototype = new BoundariesPrototype();
        prototype.setMinX(loadBoundaryValue(element.getChildByName(MIN_X_ELEMENT)));
        prototype.setMaxX(loadBoundaryValue(element.getChildByName(MAX_X_ELEMENT)));
        prototype.setMinY(loadBoundaryValue(element.getChildByName(MIN_Y_ELEMENT)));
        prototype.setMaxY(loadBoundaryValue(element.getChildByName(MAX_Y_ELEMENT)));

        return prototype;
    }

    private float loadBoundaryValue(XmlReader.Element element) {
        return Float.valueOf(element.getText().trim());
    }

}
