package tanks.game.utils.positions;

import com.badlogic.gdx.math.Vector2;

public class Position {

    private float x;
    private float y;

    public Position() {
        this(0, 0);
    }

    public Position(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Position(Vector2 vector2) {
        this.x = vector2.x;
        this.y = vector2.y;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public Vector2 getAsVector2() {
        return new Vector2(x, y);
    }

}
