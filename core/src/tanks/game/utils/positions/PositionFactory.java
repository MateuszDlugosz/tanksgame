package tanks.game.utils.positions;

import tanks.game.utils.scales.Scale;

public class PositionFactory {

    public Position createPosition(PositionPrototype prototype, Scale scale) {
        return new Position(
                scale.scaleValue(prototype.getX()),
                scale.scaleValue(prototype.getY())
        );
    }

}
