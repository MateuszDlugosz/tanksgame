package tanks.game.utils.positions;

import com.google.common.base.MoreObjects;

import java.io.Serializable;

public class PositionPrototype implements Serializable {

    private float x;
    private float y;

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(getClass())
                .add("x", x)
                .add("y", y)
                .toString();
    }

}
