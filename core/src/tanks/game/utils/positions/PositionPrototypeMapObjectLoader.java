package tanks.game.utils.positions;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.EllipseMapObject;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.PolylineMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Ellipse;
import com.badlogic.gdx.math.Rectangle;

public class PositionPrototypeMapObjectLoader {

    public PositionPrototype loadPositionPrototype(MapObject mapObject) {
        PositionPrototype prototype = new PositionPrototype();

        if (mapObject instanceof PolygonMapObject || mapObject instanceof PolylineMapObject) {
            prototype.setX(0);
            prototype.setY(0);
        }

        if (mapObject instanceof RectangleMapObject) {
            Rectangle rectangle = ((RectangleMapObject) mapObject).getRectangle();
            prototype.setX(mapObject.getProperties().get("x", Float.class) + rectangle.getWidth() * 0.5f);
            prototype.setY(mapObject.getProperties().get("y", Float.class) + rectangle.getHeight() * 0.5f);
        }

        if (mapObject instanceof EllipseMapObject) {
            Ellipse ellipse = ((EllipseMapObject) mapObject).getEllipse();
            prototype.setX(mapObject.getProperties().get("x", Float.class) + ellipse.width * 0.5f);
            prototype.setY(mapObject.getProperties().get("y", Float.class) + ellipse.height * 0.5f);
        }

        return prototype;
    }

}
