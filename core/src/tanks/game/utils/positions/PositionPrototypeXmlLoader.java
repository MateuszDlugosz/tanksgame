package tanks.game.utils.positions;

import com.badlogic.gdx.utils.XmlReader;

public class PositionPrototypeXmlLoader {

    public static final String POSITION_ELEMENT = "position";
    public static final String X_ELEMENT = "x";
    public static final String Y_ELEMENT = "y";

    public PositionPrototype loadPositionPrototype(XmlReader.Element element) {
        PositionPrototype prototype = new PositionPrototype();
        prototype.setX(Float.valueOf(element.getChildByName(X_ELEMENT).getText().trim()));
        prototype.setY(Float.valueOf(element.getChildByName(Y_ELEMENT).getText().trim()));

        return prototype;
    }

}
