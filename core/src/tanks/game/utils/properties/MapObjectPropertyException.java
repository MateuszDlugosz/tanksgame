package tanks.game.utils.properties;

public class MapObjectPropertyException extends RuntimeException {

    public MapObjectPropertyException(String message) {
        super(message);
    }

}
