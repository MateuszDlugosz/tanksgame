package tanks.game.utils.properties;

import com.badlogic.gdx.maps.MapProperties;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MapPropertiesConverter {

    public Properties convert(MapProperties mapProperties) {
        Map<String, String> propertiesMap = new HashMap<String, String>();

        Iterator<String> iterator = mapProperties.getKeys();

        while (iterator.hasNext()) {
            String key = iterator.next();
            propertiesMap.put(
                    key,
                    mapProperties.get(key, String.class)
            );
        }

        return new Properties(propertiesMap);
    }

}
