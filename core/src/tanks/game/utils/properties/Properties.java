package tanks.game.utils.properties;

import com.google.common.base.MoreObjects;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Properties implements Serializable {

    private final Map<String, String> properties;

    public Properties() {
        this(new HashMap<String, String>());
    }

    public Properties(Map<String, String> properties) {
        this.properties = properties;
    }

    public void setProperty(String name, String value) {
        properties.put(name, value);
    }

    public void setProperty(String name, float value) {
        properties.put(name, String.valueOf(value));
    }

    public void setProperty(String name, int value) {
        properties.put(name, String.valueOf(value));
    }

    public void setProperty(String name, boolean value) {
        properties.put(name, String.valueOf(value));
    }

    public boolean hasProperty(String name) {
        return properties.containsKey(name);
    }

    public String getString(String name) {
        return properties.get(name);
    }

    public String getString(String name, String defaultValue) {
        return (properties.containsKey(name)) ? properties.get(name) : defaultValue;
    }

    public float getFloat(String name) {
        return Float.valueOf(properties.get(name));
    }

    public float getFloat(String name, float defaultValue) {
        return (properties.containsKey(name)) ? Float.valueOf(properties.get(name)) : defaultValue;
    }

    public int getInteger(String name) {
        return Integer.valueOf(properties.get(name));
    }

    public int getInteger(String name, int defaultValue) {
        return (properties.containsKey(name)) ? Integer.valueOf(properties.get(name)) : defaultValue;
    }

    public boolean getBoolean(String name) {
        return Boolean.valueOf(properties.get(name));
    }

    public boolean getBoolean(String name, boolean defaultValue) {
        return (properties.containsKey(name)) ? Boolean.valueOf(properties.get(name)) : defaultValue;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public Properties merge(Properties properties) {
        for (Map.Entry<String, String> entry : properties.getProperties().entrySet()) {
            this.properties.put(entry.getKey(), entry.getValue());
        }

        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(getClass())
                .add("properties", properties)
                .toString();
    }

}
