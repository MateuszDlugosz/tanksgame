package tanks.game.utils.properties;

import com.badlogic.gdx.maps.MapObject;

public class PropertiesMapObjectLoader {

    private final MapPropertiesConverter mapPropertiesConverter;

    public PropertiesMapObjectLoader(MapPropertiesConverter mapPropertiesConverter) {
        this.mapPropertiesConverter = mapPropertiesConverter;
    }

    public Properties loadProperties(MapObject mapObject) {
        return mapPropertiesConverter.convert(mapObject.getProperties());
    }

}
