package tanks.game.utils.properties;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

import java.util.HashMap;
import java.util.Map;

public class PropertiesXmlLoader {

    public static final String PROPERTIES_ELEMENT = "properties";
    public static final String PROPERTY_ELEMENT = "property";
    public static final String NAME_ATTRIBUTE = "name";

    public Properties loadProperties(XmlReader.Element element) {
        Map<String, String> propertiesMap = new HashMap<String, String>();
        Array<XmlReader.Element> propertyElements = element.getChildrenByName(PROPERTY_ELEMENT);

        for (XmlReader.Element propertyElement : propertyElements) {
            propertiesMap.put(
                    propertyElement.getAttribute(NAME_ATTRIBUTE),
                    propertyElement.getText()
            );
        }

        return new Properties(propertiesMap);
    }

}
