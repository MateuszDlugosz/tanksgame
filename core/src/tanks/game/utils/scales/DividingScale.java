package tanks.game.utils.scales;

public class DividingScale implements Scale {

    private final float scale;

    public DividingScale(float scale) {
        if (scale == 0) throw new ScaleValueException("Scale must be greater than zero.");
        this.scale = scale;
    }

    @Override
    public float scaleValue(float value) {
        return value / scale;
    }

    @Override
    public float getValue() {
        return scale;
    }

}
