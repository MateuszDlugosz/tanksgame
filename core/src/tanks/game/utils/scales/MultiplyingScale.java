package tanks.game.utils.scales;

public class MultiplyingScale implements Scale {

    private final float scale;

    public MultiplyingScale(float scale) {
        this.scale = scale;
    }

    @Override
    public float scaleValue(float value) {
        return value * scale;
    }

    @Override
    public float getValue() {
        return scale;
    }

}
