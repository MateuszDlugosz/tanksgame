package tanks.game.utils.scales;

public interface Scale {

    float scaleValue(float value);
    float getValue();

}
