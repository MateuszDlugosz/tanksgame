package tanks.game.utils.scales;

public class ScaleValueException extends RuntimeException {

    public ScaleValueException(String message) {
        super(message);
    }

}
