package tanks.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.google.common.collect.ImmutableMap;
import tanks.game.GameApplication;
import tanks.game.context.Context;
import tanks.game.context.ContextInitializer;

public class DesktopLauncher {

	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = 960;
		config.height = 960;
		config.resizable = false;
		config.fullscreen = false;
		config.forceExit = true;


		Context context = new ContextInitializer(GameApplication.class).initContext();
		GameApplication gameApplication = new GameApplication(context);

		new LwjglApplication(gameApplication, config);
	}

}
